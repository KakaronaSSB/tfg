<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Alineacions extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('alineacions_model');
        $this->load->library(array('form_validation'));
        $this->load->helper('url');
        $this->load->helper('common');
        $this->load->helper(array('form'));
        $this->load->database('default');
        $this->load->library('session');
    }

    private function jornadesAreConsistent($id_equip) {
    	$jornadesTotals = $this->alineacions_model->get_num_jornades($id_equip);
    	$ultimaJornada = $this->alineacions_model->get_num_jornada_ultima($id_equip);
    	return $jornadesTotals == $ultimaJornada;
    }

    private function find_jugador_array($jugador,$array) {
        $i = 0;
        while ($i < count($array)) {
            if($jugador['llicencia'] == $array[$i]['llicencia']) return $i;
            ++$i;
        }
        return -1;
    }
    
    //array1 es l'array de jugadors alineats
    private function diff_jugadors($array1,$array2) {
        foreach($array1 as $jugador) {
            $indx = $this->find_jugador_array($jugador,$array2);
            if($indx >= 0) array_splice($array2,$indx,1);
        }
        return $array2;
    }

    private function find_jugadorAlineat($jugador,$jornada) {
    	$i = 0;
		while ($i < count($jornada)) {
			$extraction = explode(',', $jornada[$i]);
			$llic_jugador = strtoupper(trim($extraction[1]));
			if($jugador['llic_jugador'] == $llic_jugador) return $i;
			++$i;
		}
		return -1;
    }

    //$jornada te la seguent pinta: [[idJornada,idJugador1],[idJornada,idJugador2],[idJornada,idJugador3]]
    private function esborrarBloquejatsDeJornada($jornada,$jugadorsBloquejats) {
    	foreach($jugadorsBloquejats as $jugador) {
    		$indx = $this->find_jugadorAlineat($jugador,$jornada);
    		if($indx >= 0) array_splice($jornada,$indx,1);
    	}
    	return $jornada;
    }

    private function getPromoError() {
    	$maxPromocions = array(
    		'Territorial' => array(
    			'PREB' => 5,
    			'BEN' => 5,
    			'ALE' => 5,
    			'INF' => 5,
    			'JUV' => 5,
    			'SUB23' => 1,
    			'SEN' => 1,
				'V40' => 1,
				'V50' => 1,
				'V60' => 1
    		),
    		'Veterans' => array(
    			'V40' => 2,
    			'V50' => 2,
    			'V60' => 2
    		)
    	);
    	$promocions = $this->alineacions_model->get_promocions_by_player();
    	$promoError = array();
    	foreach($promocions as $promocio) {
			if($promocio['numPromocions'] > $maxPromocions[$promocio['categoria']][$promocio['edat']]) {
				array_push($promoError,$promocio['llic_jugador']);
			}
    	}
    	return $promoError;
    }

    private function getErrorFaltenJugadors() {
    	$equipsAmbJornades = $this->alineacions_model->get_equips_with_jornades();
    	$equipsFaltenJugadors = array();
    	foreach($equipsAmbJornades as $equip) {
    		if($this->alineacions_model->get_num_jugadors($equip['id']) < 3) array_push($equipsFaltenJugadors,$equip['id']);
    	}
    	return $equipsFaltenJugadors;
    }

	public function territorial($id_equip=0,$num_jornada=-1) {
	    if($id_equip != 0) {
	        $data['jornades_totals'] = $this->alineacions_model->get_num_jornades($id_equip);
	        $data['consistencia'] = $this->jornadesAreConsistent($id_equip);
	    }
	    $temporadaActiva = $this->alineacions_model->get_temporada_activa();
	    $data['id_equip'] = $id_equip;
	    $data['num_jornada'] = $num_jornada;
	    $data['equips'] = $this->alineacions_model->get_equips_categoria(1,$temporadaActiva);
	    $data['categoria'] = 1;
	    $data['constraints'] = $this->session->flashdata('constraints');

	    $data['maxPromoError'] = $this->session->flashdata('maxPromoError');
	    $data['horitzontalPromoError'] = $this->session->flashdata('horitzontalPromoError');
	    $data['faltenJugadorsError'] = $this->session->flashdata('faltenJugadorsError');

	    $this->load->view('alineacions/alineacions',$data);
	}
	public function nacional($id_equip=0,$num_jornada=-1) {
	    if($id_equip != 0) {
	        $data['jornades_totals'] = $this->alineacions_model->get_num_jornades($id_equip);
	        $data['consistencia'] = $this->jornadesAreConsistent($id_equip);
	    }
	    $temporadaActiva = $this->alineacions_model->get_temporada_activa();
	    $data['id_equip'] = $id_equip;
	    $data['num_jornada'] = $num_jornada;
	    $data['equips'] = $this->alineacions_model->get_equips_categoria(2, $temporadaActiva);
	    $data['categoria'] = 2;
	    $data['constraints'] = $this->session->flashdata('constraints');

		$data['maxPromoError'] = $this->session->flashdata('maxPromoError');
		$data['horitzontalPromoError'] = $this->session->flashdata('horitzontalPromoError');
		$data['faltenJugadorsError'] = $this->session->flashdata('faltenJugadorsError');

	    $this->load->view('alineacions/alineacions',$data);
	}
	public function veterans($id_equip=0,$num_jornada=-1) {
	    if($id_equip != 0) {
	        $data['jornades_totals'] = $this->alineacions_model->get_num_jornades($id_equip);
	        $data['consistencia'] = $this->jornadesAreConsistent($id_equip);
	    }
	    $temporadaActiva = $this->alineacions_model->get_temporada_activa();
	    $data['id_equip'] = $id_equip;
	    $data['num_jornada'] = $num_jornada;
	    $data['equips'] = $this->alineacions_model->get_equips_categoria(3, $temporadaActiva);
	    $data['categoria'] = 3;
	    $data['constraints'] = $this->session->flashdata('constraints');

		$data['maxPromoError'] = $this->session->flashdata('maxPromoError');
		$data['horitzontalPromoError'] = $this->session->flashdata('horitzontalPromoError');
		$data['faltenJugadorsError'] = $this->session->flashdata('faltenJugadorsError');

	    $this->load->view('alineacions/alineacions',$data);
	}
	
	public function getJornada($numJornada,$idEquip) {
	    if($numJornada == -1) {
	    	$data['jornada'] = $this->alineacions_model->get_jornada_actual($idEquip);
	    	if(empty($data['jornada'])) $data['jornada'] = $this->alineacions_model->get_jornada_ultima($idEquip);
	    }
	    else $data['jornada'] = $this->alineacions_model->get_jornada_alineacio($numJornada,$idEquip);

	    $data['local'] = $this->alineacions_model->get_local_equip($idEquip);
	    $data['jornada']['validada'] = $this->alineacions_model->jornadaValidada($data['jornada']['id']);
	    $data['jugadors_alineats'] = $this->alineacions_model->get_jugadors_alineats($data['jornada']['id']);
	    $jugadors_habilitats = $this->alineacions_model->get_jugadors_habilitats($idEquip);
	    $jugadors_noDisponibilitat = $this->alineacions_model->get_jugadors_no_disponibles($idEquip,$data['jornada']['data']);
	    $jugadors_disponibles = $this->diff_jugadors($jugadors_noDisponibilitat,$jugadors_habilitats);
	    $data['jugadors'] = $this->diff_jugadors($data['jugadors_alineats'],$jugadors_disponibles);
	    
	    echo json_encode($data);
	}
	
	public function getJugadorsToBlock($idJornada) {
	    $jugadors = $this->input->post('jugadors');
	    $result = array();
	    foreach($jugadors as $jugador) {
	        $jugador2push = $this->alineacions_model->get_jugador_jornada($jugador,$idJornada);
	        if(empty($jugador2push)) {
	            $jugador2push = $this->alineacions_model->get_jugador($jugador);
	            $jugador2push['status'] = 0;
	        }
	        array_push($result,$jugador2push);
	    }
	    echo json_encode($result);
	}

	public function getJugadorsToSendEmail() {
		$jugadors = $this->input->post('jugadors');
		$result = array();
		foreach($jugadors as $jugador) {
			$jugador2push = $this->alineacions_model->get_jugador_email($jugador);
			array_push($result,$jugador2push);
		}
		echo json_encode($result);
	}

	public function getJugadorsPromocionables($idEquip,$idCategoria,$idJornada) {
		$jornada = $this->alineacions_model->get_jornada($idJornada);
		$promocionables = $this->alineacions_model->get_jugadors_promocionables($idEquip,$idCategoria);
		$promocionables_noDisponibilitat = $this->alineacions_model->get_jugadors_promocionables_no_disponibles($idEquip,$jornada['data']);
		$jugadors_alineats = $this->alineacions_model->get_jugadors_alineats($idJornada);


		$preJugadors = $this->input->post('jugadors');
		if ($preJugadors !== null) {
			$jugadors = array_chunk($this->input->post('jugadors'),1);
			$key = array('llicencia');
			$jugadorsChunked = array();
			for($i=0;$i<count($jugadors);++$i) array_push($jugadorsChunked,array_combine($key, $jugadors[$i]));
			$promocionablesNoAlineats = $this->diff_jugadors($jugadorsChunked,$promocionables);
		}
		else $promocionablesNoAlineats = $promocionables;
		$jugadorsAlineables = $this->diff_jugadors($jugadors_alineats,$promocionablesNoAlineats);
		echo json_encode($this->diff_jugadors($promocionables_noDisponibilitat,$jugadorsAlineables));
	}
	
	public function blockJornada() {
	    $info['id_jornada'] = $this->input->post('idJornadaBloquejar');
	    $idEquip = $this->alineacions_model->get_equip_from_jornada($info['id_jornada']);
	    if($this->input->post('alinear-J1') !== null) {
	        $info['llic_jugador'] = $this->input->post('alinear-J1');
	        $info['slot'] = 1;
	        if($this->input->post('check-J1')== 'on') $info['status'] = 2;
	        else $info['status'] = 1;
	        if($idEquip == $this->alineacions_model->get_equip_jugador($info['llic_jugador'])) $info['promociona'] = 0;
	        else $info['promociona'] = 1;
	        if($this->alineacions_model->jugadorEnSlot(1,$info['id_jornada'])) $this->alineacions_model->update_jugador_alineat($info);
	        else $this->alineacions_model->insert_jugador_alineat($info);
	    }
	    else $this->alineacions_model->deleteJugadorEnSlot(1,$info['id_jornada']);
	    if($this->input->post('alinear-J2') !== null) {
	        $info['llic_jugador'] = $this->input->post('alinear-J2');
	        $info['slot'] = 2;
	        if($this->input->post('check-J2')== 'on') $info['status'] = 2;
	        else $info['status'] = 1;
			if($idEquip == $this->alineacions_model->get_equip_jugador($info['llic_jugador'])) $info['promociona'] = 0;
			else $info['promociona'] = 1;
	        if($this->alineacions_model->jugadorEnSlot(2,$info['id_jornada'])) $this->alineacions_model->update_jugador_alineat($info);
	        else $this->alineacions_model->insert_jugador_alineat($info);
	    }
	    else $this->alineacions_model->deleteJugadorEnSlot(2,$info['id_jornada']);
	    if($this->input->post('alinear-J3') !== null) {
	        $info['llic_jugador'] = $this->input->post('alinear-J3');
	        $info['slot'] = 3;
	        if($this->input->post('check-J3')== 'on') $info['status'] = 2;
	        else $info['status'] = 1;
			if($idEquip == $this->alineacions_model->get_equip_jugador($info['llic_jugador'])) $info['promociona'] = 0;
			else $info['promociona'] = 1;
	        if($this->alineacions_model->jugadorEnSlot(3,$info['id_jornada'])) $this->alineacions_model->update_jugador_alineat($info);
	        else $this->alineacions_model->insert_jugador_alineat($info);
	    }
	    else $this->alineacions_model->deleteJugadorEnSlot(3,$info['id_jornada']);

		$nomCategoria = $this->alineacions_model->get_categoria($this->input->post('idCategoriaBloquejar'));
	    redirect('alineacions/'.$nomCategoria.'/'.$this->input->post('idEquipBloquejar').'/'.$this->input->post('numJornadaBloquejar'));
	}

	private function insertOrUpdateBeforeValidate() {
		$jornada = $this->input->post('idJornadaValidar');
		$idEquip = $this->alineacions_model->get_equip_from_jornada($jornada);
		$j1 = $this->input->post('validar-J1');
		$j2 = $this->input->post('validar-J2');
		$j3 = $this->input->post('validar-J3');
		if($idEquip == $this->alineacions_model->get_equip_jugador($j1)) $promoJ1 = 0;
		else $promoJ1 = 1;
		if($idEquip == $this->alineacions_model->get_equip_jugador($j2)) $promoJ2 = 0;
		else $promoJ2 = 1;
		if($idEquip == $this->alineacions_model->get_equip_jugador($j3)) $promoJ3 = 0;
		else $promoJ3 = 1;
		$jugador1 = array(
			'id_jornada' => $jornada,
			'llic_jugador' => $j1,
			'slot' => 1,
			'status' => 3,
			'promociona' => $promoJ1
		);
		if($this->alineacions_model->jugadorEnSlot(1,$jornada)) $this->alineacions_model->update_jugador_alineat($jugador1);
       	else $this->alineacions_model->insert_jugador_alineat($jugador1);
		$jugador2 = array(
			'id_jornada' => $jornada,
			'llic_jugador' => $j2,
			'slot' => 2,
			'status' => 3,
			'promociona' => $promoJ2
		);
		if($this->alineacions_model->jugadorEnSlot(2,$jornada)) $this->alineacions_model->update_jugador_alineat($jugador2);
        else $this->alineacions_model->insert_jugador_alineat($jugador2);
		$jugador3 = array(
			'id_jornada' => $jornada,
			'llic_jugador' => $j3,
			'slot' => 3,
			'status' => 3,
			'promociona' => $promoJ3
		);
		if($this->alineacions_model->jugadorEnSlot(3,$jornada)) $this->alineacions_model->update_jugador_alineat($jugador3);
        else $this->alineacions_model->insert_jugador_alineat($jugador3);
	}

	public function validateJornada() {
		$resultat = array(
			'resultat_equip' => $this->input->post('resultatEquip'),
			'resultat_contrincant' => $this->input->post('resultatContrincant')
		);
	    $this->insertOrUpdateBeforeValidate();
	    $this->alineacions_model->update_jornada($this->input->post('idJornadaValidar'),$resultat);

		$nomCategoria = $this->alineacions_model->get_categoria($this->input->post('idCategoriaValidar'));
		redirect('alineacions/'.$nomCategoria.'/'.$this->input->post('idEquipValidar').'/'.$this->input->post('numJornadaValidar'));
	}

	private function generarEntrada() {
		$jugadors = $this->alineacions_model->get_jugadors_for_prolog();
		$prolog_file ="%Dades d'entrada";
		$prolog_file = $prolog_file."\n"."%jugador(id,MinPartits,MaxPartits,Equip,Transport,Responsable,Promocionable,Edat,Fitxa).";
		foreach($jugadors as $jugador) {
			$prolog_file = $prolog_file."\n"."jugador(".strtolower($jugador['llicencia']).",".$jugador['min_partits'].",".$jugador['max_partits'].",".$jugador['id_equip'].",".$jugador['transport'].",".$jugador['responsable'].",".$jugador['promocionable'].",".strtolower($jugador['edat']).",".strtolower($jugador['fitxa']).").";
		}
		$equips = $this->alineacions_model->get_equips_for_prolog();
		$prolog_file = $prolog_file."\n"."%equip(id,idCategoria,jerarquia).";
		foreach($equips as $equip){
			$prolog_file = $prolog_file."\n"."equip(".$equip['id'].",".$equip['id_categoria'].",".$equip['jerarquia'].").";
		}
		$no_disponibilitats = $this->alineacions_model->get_dates_no_alineables_for_prolog();
		$prolog_file = $prolog_file."\n"."%noDisponibilitat(idJug,data).";
		foreach($no_disponibilitats as $no_disponible) {
			$prolog_file = $prolog_file."\n"."noDisponibilitat(".strtolower($no_disponible['llic_jugador']).",".date("d-m-Y",strtotime($no_disponible['data'])).").";
		}
		$prolog_file = $prolog_file."\n"."%jornada(id,idEquip,numJornada,data,local/visitant).";
		$jornades = $this->alineacions_model->get_jornades_for_prolog();
		foreach($jornades as $jornada) {
			$prolog_file = $prolog_file."\n"."jornada(".$jornada['id'].",".$jornada['id_equip'].",".$jornada['jornada'].",".date("d-m-Y",strtotime($jornada['data'])).",".$jornada['casa'].").";
		}
		$prolog_file = $prolog_file."\n"."%encadenats(idJugador1,idJugador2).";
		$encadenats = $this->alineacions_model->get_cadenes_for_prolog();
		foreach($encadenats as $cadena){
			$prolog_file = $prolog_file."\n"."encadenats(".strtolower($cadena['llicencia']).",".strtolower($cadena['llic_encadenat']).").";
		}
		$prolog_file = $prolog_file."\n"."%jugadorJornada(idJugador,idJornada).";
		$ja_alineats = $this->alineacions_model->get_ja_alineats_for_prolog();
		foreach($ja_alineats as $ja_alineat){
			$prolog_file = $prolog_file."\n"."jugadorJornada(".strtolower($ja_alineat['llic_jugador']).",".$ja_alineat['id_jornada'].").";
		}
		file_put_contents($_SERVER['DOCUMENT_ROOT'].'/'.TENANT.'/prolog/entrada.pl',$prolog_file);
	}

	private function llegirSolucio() {
		$solution = file($_SERVER['DOCUMENT_ROOT'].'/'.TENANT.'/prolog/solution');
		$groupedSolution = array_chunk($solution,3);
		foreach($groupedSolution as $jornada) {
			$idJornada = explode(',',$jornada[0])[0];
			if(!$this->alineacions_model->jornadaValidada($idJornada)) {
				$jugadorsBloquejats = $this->alineacions_model->getJugadorsBloquejats($idJornada);
				$slot = 1;
				if(count($jugadorsBloquejats) > 0){
					$jornada = $this->esborrarBloquejatsDeJornada($jornada,$jugadorsBloquejats);
					$this->alineacions_model->clean_jornades_blocked($idJornada);
					foreach($jugadorsBloquejats as $jugador) {
						$info = array(
							'llic_jugador' => $jugador['llic_jugador'],
							'id_jornada' => $idJornada,
							'status' => 2,
							'slot' => $slot,
							'promociona' => $jugador['promociona']
						);
						$this->alineacions_model->insert_jugador_alineat($info);
						$slot = $slot + 1;
					}
				}
				foreach($jornada as $jugadorAlineat) {
					$extraction = explode(',',$jugadorAlineat);
					$jugador = strtoupper(trim($extraction[1]));
					$jornada = $extraction[0];
					$idEquip = $this->alineacions_model->get_equip_from_jornada($jornada);
					if($idEquip == $this->alineacions_model->get_equip_jugador($jugador)) $promo = 0;
                    else $promo = 1;
					$info = array(
						'llic_jugador' => $jugador,
						'id_jornada' => $jornada,
						'status' => 1,
						'slot' => $slot,
						'promociona' => $promo
					);
					$this->alineacions_model->insert_jugador_alineat($info);
					$slot = $slot + 1;
				}
			}
		}
	}
	private function idToJugador($id) {
    	$jugador = $this->alineacions_model->get_nom_jugador($id);
    	return "<b>".$jugador['nom'].' '.$jugador['cognom']."</b>";
	}

	private function idToJornada($id) {
		$jornada = $this->alineacions_model->get_jornada_restriccio($id);
		return "<b> Jornada ".$jornada['jornada'].'</b> del equip <b>'.$jornada['equip'].' '.$jornada['categoria']."</b>";
	}

	private function llegirValidacio() {
		$validation = file($_SERVER['DOCUMENT_ROOT'].'/'.TENANT.'/prolog/validation');
		$constraints = array();
		foreach($validation as $constraint) {
			$constraint = explode(',',$constraint);
			$type = count($constraint);
			if($type == 2) {
				if($constraint[0] == '0') array_push($constraints,"El jugador ".$this->idToJugador(trim(strtoupper($constraint[1])))." no juga el seu màxim de partits establerts");
				if($constraint[0] == '1') array_push($constraints,"El jugador ".$this->idToJugador(trim(strtoupper($constraint[1])))." no juga el seu mínim de partits establerts");
				if($constraint[0] == '2') array_push($constraints,"La ".$this->idToJornada(trim(strtoupper($constraint[1])))." no te cap jugador responsable");
				if($constraint[0] == '3') array_push($constraints,"La ".$this->idToJornada(trim(strtoupper($constraint[1])))." no te cap jugador amb transport");
				if($constraint[0] == '4') array_push($constraints,"El jugador ".$this->idToJugador(trim(strtoupper($constraint[1])))." no juga el mateix nombre de partits com a local i visitant");
				if($constraint[0] == '6') array_push($constraints,"El jugador ".$this->idToJugador(trim(strtoupper($constraint[1])))." no juga de forma ben espaiada");
			}
			else if($type == 3) {
				array_push($constraints,"El jugador ".$this->idToJugador(trim(strtoupper($constraint[1])))." promociona durant la ".$this->idToJornada(trim(strtoupper($constraint[2]))));
			}
			else if($type == 4) {
				array_push($constraints,"Els jugadors ".$this->idToJugador(trim(strtoupper($constraint[1])))." i ".$this->idToJugador(trim(strtoupper($constraint[2])))." no juguen junts a la ".$this->idToJornada(trim(strtoupper($constraint[3]))));
			}
		}
		$this->session->set_flashdata('constraints', $constraints);
	}

	public function generar() {
		$maxPromoError = $this->getPromoError();
		$horitzontalPromoError = $this->alineacions_model->get_promocions_horitzontals();
		$faltenJugadorsError = $this->getErrorFaltenJugadors();
		if(count($faltenJugadorsError) > 0)  $this->session->set_flashdata('faltenJugadorsError',$faltenJugadorsError);
		if(count($maxPromoError) > 0) $this->session->set_flashdata('maxPromoError',$maxPromoError);
		if(count($horitzontalPromoError) > 0) $this->session->set_flashdata('horitzontalPromoError',$horitzontalPromoError);
		if(count($maxPromoError) == 0 && count($horitzontalPromoError) == 0 && count($faltenJugadorsError) == 0) {
			$this->alineacions_model->esborrarSugerimentAntic();
			$this->generarEntrada();
			shell_exec('bash '.$_SERVER['DOCUMENT_ROOT'].'/'.TENANT.'/prolog/run.sh');
			$this->llegirSolucio();
			$this->alineacions_model->backup_suggestion();
			$this->llegirValidacio();
			shell_exec('bash '.$_SERVER['DOCUMENT_ROOT'].'/'.TENANT.'/prolog/clean.sh');
		}
		redirect('alineacions/territorial');
	}

	public function validar() {
		$maxPromoError = $this->getPromoError();
		$horitzontalPromoError = $this->alineacions_model->get_promocions_horitzontals();
		$faltenJugadorsError = $this->getErrorFaltenJugadors();
		if(count($faltenJugadorsError) > 0)  $this->session->set_flashdata('faltenJugadorsError',$faltenJugadorsError);
		if(count($maxPromoError) > 0) $this->session->set_flashdata('maxPromoError',$maxPromoError);
		if(count($horitzontalPromoError) > 0) $this->session->set_flashdata('horitzontalPromoError',$horitzontalPromoError);
		if(count($maxPromoError) == 0 && count($horitzontalPromoError) == 0 && count($faltenJugadorsError) == 0) {
			$this->generarEntrada();
			shell_exec('bash '.$_SERVER['DOCUMENT_ROOT'].'/'.TENANT.'/prolog/run.sh');
			$this->llegirValidacio();
			shell_exec('bash '.$_SERVER['DOCUMENT_ROOT'].'/'.TENANT.'/prolog/clean.sh');
		}
		redirect('alineacions/territorial');
	}

	public function rolback() {
		$this->alineacions_model->restore_suggestion();
		redirect('alineacions/territorial');
	}

	public function sendEmails() {
		setlocale(LC_TIME, 'ca_ES', 'Catalan_Spain', 'Catalan');

    	$toEmail = EMAIL;
		if($this->input->post('send-J1')== 'on') $toEmail = $toEmail.",".$this->input->post('email-J1');
		if($this->input->post('send-J2')== 'on') $toEmail = $toEmail.",".$this->input->post('email-J2');
		if($this->input->post('send-J3')== 'on') $toEmail = $toEmail.",".$this->input->post('email-J3');

		$idJornada = $this->input->post("idJornadaEmail");

		$infoJornada = $this->alineacions_model->get_info_jornada_email($idJornada);

		$data = array(
			'equip' => $infoJornada['equip'],
			'categoria' => $infoJornada['categoria'],
			'diaSetmana' => strftime('%A',strtotime($infoJornada['data'])),
			'ciutat' => $infoJornada['ciutat'],
			'data' => strftime('%e %b%y',strtotime($infoJornada['data'])),
			'hora' => date('H:i',strtotime($infoJornada['data'])),
			'categoriaEquip' => $infoJornada['categoria'][0].$infoJornada['equip'],
			'contrincant' => $infoJornada['contrincant'],
			'direccio' => $infoJornada['carrer'],
			'local' => $infoJornada['local'],
			'localEquip' => $infoJornada['localEquip'],
			'cp' => $infoJornada['codi_postal'],
			'jugador1' => $this->input->post('nom-J1'),
			'jugador2' => $this->input->post('nom-J2'),
			'jugador3' => $this->input->post('nom-J3')
		);

		$this->load->library('email');
		$this->email->from(EMAIL, EMAIL);
		$this->email->to($toEmail);
		$this->email->subject('CONVOCATÒRIA');

		if($infoJornada['casa'] == 1) $this->email->message($this->load->view('templates/convocatoria_local',$data,true));
		else $this->email->message($this->load->view('templates/convocatoria_visitant',$data,true));

		$this->email->set_mailtype('html');
		$this->email->send();

		$nomCategoria = $this->alineacions_model->get_categoria($this->input->post('idCategoriaEmails'));
		redirect('alineacions/'.$nomCategoria.'/'.$this->input->post('idEquipEmails').'/'.$this->input->post('numJornadaEmails'));
	}
}
