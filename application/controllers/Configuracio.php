<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Configuracio extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('alineacions_model');
        $this->load->library(array('form_validation'));
        $this->load->helper('url');
        $this->load->helper('common');
        $this->load->helper(array('form'));
        $this->load->database('default');
        $this->load->library('session');
    }
    
    //TEMPORADES
    
	public function index(){
	    $data['temporades'] = $this->alineacions_model->get_temporades();
	    $i = 0;
	    while($i < count($data['temporades'])) {
	        $data['temporades'][$i]['numEquips'] = $this->alineacions_model->get_num_equips($data['temporades'][$i]['id']);
	        ++$i;
	    }
	    $data['errorDelete'] = $this->session->flashdata('errorDelete');
		$this->load->view('alineacions/temporades',$data);
	}
	
	public function addSeason() {
	    if($this->input->post('activa') == 'on') {
	        $activa = 1;
	        $this->alineacions_model->desmarcar_actives();
	    }
	    else $activa = 0;
	    $info = array(
	        'temporada' => $this->input->post('nomTemporada'),
	        'activa' => $activa
	    );
	    $this->alineacions_model->insert_temporada($info);
	    redirect('');
	}
	
	public function deleteSeason() {
	    $id = $this->input->post('idTemporada');
	   	if(!$this->alineacions_model->isActiva($id)) $this->alineacions_model->delete_temporada($id);
	   	else $this->session->set_flashdata('errorDelete','activa');
	   	redirect('');
	}
	
	public function getSeasonToEdit($idSeason) {
	    echo json_encode($this->alineacions_model->get_temporada($idSeason));
	}
	
	public function editSeason() {
	    if($this->input->post('activa') == 'on') {
	        $activa = 1;
	        $this->alineacions_model->desmarcar_actives();
	    }
	    else $activa = 0;
	    $info = array(
	        'temporada' => $this->input->post('nomTemporada'),
	        'activa' => $activa
	    );
	    $this->alineacions_model->update_temporada($this->input->post('idTemporada'),$info);
	    redirect('');
	}
	
	//LOCALS
	
	public function locals() {
		$data['locals'] = $this->alineacions_model->get_locals();
		$data['errorDelete'] = $this->session->flashdata('errorDelete');
	    $this->load->view('alineacions/locals',$data);
	}

	public function addLocal() {
		$info = array(
			'ciutat' => $this->input->post('ciutat'),
			'codi_postal' => $this->input->post('codiPostal'),
			'local' => $this->input->post('local'),
			'carrer' => $this->input->post('carrer'),
			'pilota' => $this->input->post('pilota'),
			'taula' => $this->input->post('taula'),
			'equipacio' => $this->input->post('equipacio')
		);
		$this->alineacions_model->insert_local($info);
		redirect('configuracio/locals');
	}

	public function editLocal() {
		$info = array(
			'ciutat' => $this->input->post('eciutat'),
			'codi_postal' => $this->input->post('ecp'),
			'local' => $this->input->post('elocal'),
			'carrer' => $this->input->post('ecarrer'),
			'pilota' => $this->input->post('epilota'),
			'taula' => $this->input->post('etaula'),
			'equipacio' => $this->input->post('eequipacio')
		);
		$this->alineacions_model->update_local($this->input->post('idLocal'),$info);
		redirect('configuracio/locals');
	}

	public function getLocalToEdit($idLocal) {
		echo json_encode($this->alineacions_model->get_local($idLocal));
	}

	public function deleteLocal() {
		$idLocal = $this->input->post('idLocal');
		$equipsLocal = $this->alineacions_model->get_equips_from_local($idLocal);
		if(count($equipsLocal) > 0) $this->session->set_flashdata('errorDelete',$equipsLocal);
		else $this->alineacions_model->delete_local($idLocal);
		redirect('configuracio/locals');
	}

	//EQUIPS
	
	public function equips() {
	    $data['temporades'] = $this->alineacions_model->get_temporades();
	    $data['categories'] = $this->alineacions_model->get_categories();
	    $data['locals'] = $this->alineacions_model->get_locals();
	    $data['equips'] = $this->alineacions_model->get_equips_vista();
	    $data['errorDelete'] = $this->session->flashdata('errorDelete');
	    $i = 0;
	    while($i < count($data['equips'])) {
	        $data['equips'][$i]['numJugadors'] = $this->alineacions_model->get_num_jugadors( $data['equips'][$i]['id']);
	        $data['equips'][$i]['numJornades'] = $this->alineacions_model->get_num_jornades( $data['equips'][$i]['id']);
	        ++$i;
	    }
	    $this->load->view('alineacions/equips',$data);
	}
	
	public function addTeam() {
	    $info = array(
	        'equip' => $this->input->post('nomEquip'),
	        'id_local' => $this->input->post('local'),
	        'id_temporada' => $this->input->post('temporada'),
	        'id_categoria' => $this->input->post('categoria'),
	        'jerarquia' => 0
	    );

	    if (null !== $this->input->post('jerarquia')) $info['jerarquia'] = $this->alineacions_model->update_jerarquia($this->input->post('categoria'),$this->input->post('equip-referencia'),$this->input->post('jerarquia'));;
	    $this->alineacions_model->insert_equip($info);
	    redirect('configuracio/equips');
	}

	public function editTeam() {
		$info = array(
			'equip' => $this->input->post('eequip'),
			'id_local' => $this->input->post('elocal'),
			'id_temporada' => $this->input->post('etemporada'),
			'id_categoria' => $this->input->post('ecategoria'),
			'jerarquia' => 0
		);
		if (null !== $this->input->post('edit-jerarquia')) $info['jerarquia'] = $this->alineacions_model->update_jerarquia($this->input->post('ecategoria'),$this->input->post('edit-equip-referencia'),$this->input->post('edit-jerarquia'));
		$this->alineacions_model->update_equip($this->input->post('idEquip'),$info);
		redirect('configuracio/equips');
	}

	public function getTeamToEdit($idEquip) {
		echo json_encode($this->alineacions_model->get_equip($idEquip));
	}

	public function getTeamsFromCategoria($idCategoria,$idTemporada) {
		echo json_encode($this->alineacions_model->get_equips_categoria($idCategoria,$idTemporada));
	}

	public function deleteTeam() {
		$idEquip = $this->input->post('idEquip');
		$numJugadors = $this->alineacions_model->get_num_jugadors($idEquip);
		if($numJugadors > 0) $this->session->set_flashdata('errorDelete',$numJugadors);
		else {
			$equip = $this->alineacions_model->get_equip($idEquip);
			$this->alineacions_model->update_jerarquia_delete($equip['id_categoria'],$equip['jerarquia']);
			$this->alineacions_model->delete_team($idEquip);
		}
		redirect('configuracio/equips');
	}
	
	//CONTRINCANTS
	
	public function contrincants() {
	    if ($this->input->post('submit') == "addContrincants") {
	        $i = 0;
	        while($i < $this->input->post('numContrincants')) {
	            $info = array(
    	            'contrincant' => $this->input->post('equip'.$i),
    	            'ciutat' => $this->input->post('ciutat'.$i),
    	            'codi_postal' => $this->input->post('codiPostal'.$i),
    	            'local' => $this->input->post('local'.$i),
    	            'carrer' => $this->input->post('carrer'.$i),
	                'pilota' => $this->input->post('pilota'.$i),
	                'taula' => $this->input->post('taula'.$i),
	                'equipacio' => $this->input->post('equipacio'.$i)
	            );
	            ++$i;
	            $this->alineacions_model->insert_contrincant($info);
	        }
	        redirect('configuracio/contrincants');
	    }
	    $data['errorDelete'] = $this->session->flashdata('errorDelete');
	    $data['contrincants'] = $this->alineacions_model->get_contrincants();
	    $this->load->view('alineacions/contrincants',$data);
	}
	
	public function addContrincant() {
	    $data['numContrincants'] = $this->input->post('numContrincants');
	    $this->load->view('alineacions/afegir-contrincants',$data);
	}

	public function editContrincant() {
		$info = array(
			'contrincant' => $this->input->post('econtrincant'),
			'ciutat' => $this->input->post('eciutat'),
			'codi_postal' => $this->input->post('ecp'),
			'local' => $this->input->post('elocal'),
			'carrer' => $this->input->post('ecarrer'),
			'pilota' => $this->input->post('epilota'),
			'taula' => $this->input->post('etaula'),
			'equipacio' => $this->input->post('eequipacio')
		);
		$this->alineacions_model->update_contrincant($this->input->post('idContrincant'),$info);
    	redirect('configuracio/contrincants');
    }

	public function getContrincantToEdit($idContrincant) {
		echo json_encode($this->alineacions_model->get_contrincant($idContrincant));
	}

	public function deleteContrincant() {
		$idContrincant = $this->input->post('idContrincant');
		$jornadesContrincant = $this->alineacions_model->get_jornades_from_contrincant($idContrincant);
		if(count($jornadesContrincant) > 0) $this->session->set_flashdata('errorDelete',$jornadesContrincant);
		else $this->alineacions_model->delete_contrincant($idContrincant);
		redirect('configuracio/contrincants');
	}
	
	//JORNADES
	
	public function jornades() {
		if ($this->input->post('submit') == "addJornades") {
		    $i = 0;
		    while($i < $this->input->post('numJornades')) {
		    	if($this->input->post('casa'.$i) == 'on') $casa = 1;
				else $casa = 0;
    		    $info = array(
    		        'jornada' => $this->input->post('numero'.$i),
    		        'data' => date('Y-m-d H:i:s',strtotime($this->input->post($i.'datetimepicker'))),
    		        'id_contrincant' => $this->input->post('contrincant'.$i),
    		        'id_equip' => $this->input->post('equip'),
    		        'casa' => $casa
    		    );
    		    ++$i;
    		    $this->alineacions_model->insert_jornada($info);
		    }
		    redirect('configuracio/jornades');
		}
		$data['jornades'] = $this->alineacions_model->get_jornades_vista();
		$data['contrincants'] = $this->alineacions_model->get_contrincants();
		$data['equips'] = $this->alineacions_model->get_equips_select();
		$data['errorDelete'] = $this->session->flashdata('errorDelete');
	    $this->load->view('alineacions/jornades',$data);
	}
	
	public function addJornades() {
	    $data['numJornades'] = $this->input->post('numJornades');
	    $data['equip'] = $this->input->post('equip');
	    $data['contrincants'] = $this->alineacions_model->get_contrincants();
	    $this->load->view('alineacions/afegir-jornades',$data);
	}

	public function editJornada() {
		if($this->input->post('ecasa') == 'on') $casa = 1;
    	else $casa = 0;
		$info = array(
			'jornada' => $this->input->post('ejornada'),
			'data' => date('Y-m-d H:i:s',strtotime($this->input->post('edatepicker'))),
			'id_contrincant' => $this->input->post('econtrincant'),
			'casa' => $casa
		);
		$this->alineacions_model->update_jornada($this->input->post('idJornada'),$info);
		redirect('configuracio/jornades');
	}

	public function deleteJornada() {
		$idJornada = $this->input->post('idJornada');
		$linedUp = $this->alineacions_model->jornada_is_lined_up($idJornada);
		if($linedUp) $this->session->set_flashdata('errorDelete','linedUp');
		else {
			$this->alineacions_model->delete_jornada($idJornada);
			$this->alineacions_model->backup_suggestion();
		}
		redirect('configuracio/jornades');
	}

	public function getJornadaToEdit($idJornada) {
		echo json_encode($this->alineacions_model->get_jornada($idJornada));
	}
	
	//JUGADORS
	
	public function jugadors() {
	    if ($this->input->post('submit') == "addPlayers") {
	        $i = 0;
	        while($i < $this->input->post('numJugadors')) {
	            if($this->input->post('transport'.$i) == 'on') $transport = 1;
	            else $transport = 0;
	            if($this->input->post('responsable'.$i) == 'on') $responsable = 1;
	            else $responsable = 0;
	            if($this->input->post('promocionable'.$i) == 'on') $promocionable = 1;
				else $promocionable = 0;
	            $info = array(
	                'llicencia' => $this->input->post('llicencia'.$i),
	                'nom' => $this->input->post('nom'.$i),
	                'cognom' => $this->input->post('cognom'.$i),
	                'email' => $this->input->post('email'.$i),
	                'id_fitxa' => $this->input->post('fitxa'.$i),
	                'id_edat' => $this->input->post('edat'.$i),
	                'id_equip' => $this->input->post('equip'.$i),
	                'min_partits' => $this->input->post('minGames'.$i),
	                'max_partits' => $this->input->post('maxGames'.$i),
	                'transport' => $transport,
	                'responsable' => $responsable,
	                'promocionable' => $promocionable
	            );
	            ++$i;
	            $this->alineacions_model->insert_jugador($info);
	        }
	        redirect('configuracio/jugadors');
	    }
	    $data['equips'] = $this->alineacions_model->get_equips_select();
        $data['fitxes'] = $this->alineacions_model->get_fitxes();
        $data['edats'] = $this->alineacions_model->get_edats();
	    $data['error'] = $this->session->flashdata('error');
	    $data['errorDelete'] = $this->session->flashdata('errorDelete');
		$data['jugadors'] = $this->alineacions_model->get_jugadors_vista();

		$i = 0;
		while($i < count($data['jugadors'])){
			$data['jugadors'][$i]['partits'] = $this->alineacions_model->get_num_matches_player($data['jugadors'][$i]['llicencia']);
			++$i;
		}

		$data['jugadors_nocadena'] =$this->alineacions_model->get_jugadors_nocadena();
	    $this->load->view('alineacions/jugadors',$data);
	}
	
	public function addPlayers() {
	    $data['equips'] = $this->alineacions_model->get_equips_select();
	    $data['fitxes'] = $this->alineacions_model->get_fitxes();
	    $data['edats'] = $this->alineacions_model->get_edats();
	    $data['numJugadors'] = $this->input->post('numJugadors');
	    $data['temporada'] = $this->input->post('temporada');
	    $this->load->view('alineacions/afegir-jugadors',$data);
	}
	
	public function linkPlayers() {
	    $jugadorA = $this->input->post('jugador1');
	    $jugadorB = $this->input->post('jugador2');
	    if($jugadorA == $jugadorB) $this->session->set_flashdata('error', 'mateixJugador');
	    else if($this->alineacions_model->teCadena($jugadorA) or $this->alineacions_model->teCadena($jugadorB)) {
	        $this->session->set_flashdata('error', 'teCadena');
	    }
	    else if($this->alineacions_model->get_equip_jugador($jugadorA) != $this->alineacions_model->get_equip_jugador($jugadorB)){
	        $this->session->set_flashdata('error', 'diferentEquip');
	    }       
	    else {
    	    $this->alineacions_model->encadenar_jugadors($jugadorA,$jugadorB);
	    }
	    redirect('configuracio/jugadors');
	}
	
	public function editPlayer() {
	   if($this->input->post('etransport') == 'on') $transport = 1;
	   else $transport = 0;
	   if($this->input->post('eresponsable') == 'on') $responsable = 1;
	   else $responsable = 0;
	   if($this->input->post('epromocionable') == 'on') $promocionable = 1;
	   else $promocionable = 0;
	   $info = array(
	   	'llicencia' => $this->input->post('ellicencia'),
	   	'nom' => $this->input->post('enom'),
	   	'cognom' => $this->input->post('ecognom'),
	   	'id_fitxa' => $this->input->post('efitxa'),
	   	'id_edat' => $this->input->post('eedat'),
	   	'id_equip' => $this->input->post('eequip'),
	   	'email' => $this->input->post('eemail'),
	   	'transport' => $transport,
		'responsable' => $responsable,
		'promocionable' => $promocionable,
		'min_partits' => $this->input->post('eminPartits'),
		'max_partits' => $this->input->post('emaxPartits'),
	   );
	   $this->alineacions_model->update_jugador($this->input->post('idJugador'),$info);
	   redirect('configuracio/jugadors');
	}
	
	public function disablePlayer() {
	    $this->alineacions_model->canvi_estat_jugador($this->input->post('idJugador'),$this->input->post('habilitat'));
	    redirect('configuracio/jugadors');
	}
	
	public function deletePlayer() {
	    $idJugador = $this->input->post('idJugador');
	    $partitsAlineats = $this->alineacions_model->get_partits_alineats($idJugador);
	    if(count($partitsAlineats) > 0) $this->session->set_flashdata('errorDelete',$partitsAlineats);
	    else {
			$this->alineacions_model->backup_suggestion();
			$this->alineacions_model->delete_jugador($idJugador);
	    }
	    redirect('configuracio/jugadors');
	}
	
	public function disableJornada() {
		if($this->input->post('select-motiu') == 1) {
	    	$dates = explode(", ", $this->input->post('datepicker'));
	    	foreach($dates as $data) {
				 $info = array(
					'llic_jugador' => $this->input->post('idJugador'),
					'motiu' => 1,
					'id_jornada' => -1,
					'data' => $data
				);
				$this->alineacions_model->deshabilitar_data($info);
	    	}
		}
		else if($this->input->post('select-motiu') == 2) {
			 $info = array(
				'llic_jugador' => $this->input->post('idJugador'),
				'motiu' => 2,
				'id_jornada' => $this->input->post('select-jornades'),
				'data' => $this->input->post('datepicker')
			);
			$this->alineacions_model->deshabilitar_data($info);
		}
        redirect('configuracio/jugadors');
	}

	public function getPlayerToEdit($idPlayer) {
	    echo json_encode($this->alineacions_model->get_jugador($idPlayer));
	}

	public function getJornadesFromData($data) {
		echo json_encode($this->alineacions_model->get_jornades_from_data($data));
	}

}
