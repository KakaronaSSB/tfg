<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Consultes extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('alineacions_model');
        $this->load->library(array('form_validation'));
        $this->load->helper('url');
        $this->load->helper('common');
        $this->load->helper(array('form'));
        $this->load->database('default');
        $this->load->library('session');
    }

    private function group_by_key($array,$key) {
    	$result = array();
    	foreach($array as $element) {
    		$result[$element[$key]][] = $element;
    	}
    	return $result;
    }

    public function index() {
    	$data['jugadors'] = array();
    	$this->load->view('alineacions/consultar-jugadors',$data);
    }

    public function jornades($idEquip=-1) {
    	$temporadaActiva = $this->alineacions_model->get_temporada_activa();
   		$data['equips_1'] = $this->alineacions_model->get_equips_categoria(1,$temporadaActiva);
   		$data['equips_2'] = $this->alineacions_model->get_equips_categoria(2,$temporadaActiva);
   		$data['equips_3'] = $this->alineacions_model->get_equips_categoria(3,$temporadaActiva);
   		if($idEquip != -1) {
   			$jornadesAsRows = $this->alineacions_model->get_jornades_alineades($idEquip);
   			$jornadesGroupedByNum = $this->group_by_key($jornadesAsRows,'jornada');
   			$data['jornades'] = array_chunk($jornadesGroupedByNum,3);
   			$data['id_equip'] = $idEquip;
   			$data['categoria'] = $this->alineacions_model->get_categoria_equip($idEquip);
   		}
		$this->load->view('alineacions/consultar-jornades',$data);
	}

	public function buscarJugador($query) {
		echo json_encode($this->alineacions_model->buscar_jugador($query));
	}

	public function consultarJugador($llicencia) {
		$data['jugador'] = $this->alineacions_model->get_jugador($llicencia);
		$data['partits_jugats'] = $this->alineacions_model->get_partits_jugats($llicencia);
		$data['partits_alineats'] = $this->alineacions_model->get_partits_alineats($llicencia);
		$data['dates_noalineables'] = $this->alineacions_model->get_dates_no_alineables($llicencia);
		$data['partits_declinats'] = $this->alineacions_model->get_partits_declinats($llicencia);
		$data['encadenat'] = $this->alineacions_model->get_jugador_encadenat($llicencia);
		$this->load->view('alineacions/consultar-jugadors',$data);
	}

	public function unlinkPlayers() {
		$this->alineacions_model->unlink_players($this->input->post('idJugador'));
		redirect('consultes/jugador/'.$this->input->post('idJugador'));
	}

	public function deleteData() {
		$this->alineacions_model->delete_data($this->input->post('idData'));
		redirect('consultes/jugador/'.$this->input->post('idJugador'));
	}

}
