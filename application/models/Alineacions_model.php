<?php

class Alineacions_model extends CI_Model {
    public function __construct() {
        $this->load->database();
    }
    //Funcions generals
    public function get_categories() {
        $query = $this->db->get('categories');
        return $query->result_array();
    }

    public function get_categoria($id) {
    	$this->db->where('id',$id);
    	$query = $this->db->get('categories');
    	$categoria = $query->row_array()['categoria'];
    	return strtolower($categoria);
    }
    
    public function get_fitxes() {
        $query = $this->db->get('fitxes');
        return $query->result_array();
    }
    
    public function get_edats() {
        $query = $this->db->get('edats');
        return $query->result_array();
    }
    
    //Funcions temporades
    public function get_temporades() {
        $query = $this->db->get('temporades');
        return $query->result_array();
    }
    
    public function get_num_equips($id_temporada) {
        $query = $this->db->query('select count(*) as numEquips
                 from equips as e
                 where e.id_temporada= '.$id_temporada);
        return $query->row_array()['numEquips'];
    }
    
    public function get_temporada($id) {
        $this->db->where('id',$id);
        $query = $this->db->get('temporades');
        return $query->row_array();
    }
    
    public function insert_temporada($info) {
        $this->db->insert('temporades', $info);
    }
    
    public function desmarcar_actives() {
        $this->db->where('activa',1);
        $this->db->set('activa',0);
        $this->db->update('temporades');
    }
    
    public function update_temporada($id,$info) {
        $this->db->where('id',$id);
        $this->db->update('temporades',$info);
    }

    public function isActiva($id) {
    	$this->db->where('id',$id);
    	$query = $this->db->get('temporades');
    	return $query->row_array()['activa'];
    }

    public function delete_temporada($id) {
    	$this->db->where('id',$id);
    	$this->db->delete('temporades');
    }

    public function get_temporada_activa() {
    	$this->db->where('activa',1);
        $query = $this->db->get('temporades');
        return $query->row_array()['id'];
    }


    //Funcions locals

     public function insert_local($info) {
	 	$this->db->insert('locals', $info);
	 }
	 public function get_locals() {
		 $query = $this->db->get('locals');
		 return $query->result_array();
	 }
	 public function get_local($id) {
		 $this->db->where('id',$id);
		 $query = $this->db->get('locals');
		 return $query->row_array();
	 }

	 public function update_local($id,$info) {
		 $this->db->where('id',$id);
		 $this->db->update('locals',$info);
	 }

	public function get_local_equip($id) {
		$query = $this->db->query('select l.*
		 		 from locals as l join equips as e on l.id = e.id_local
		 		 where e.id ='.$id);
		return $query->row_array();
	}

	public function get_equips_from_local($id) {
		$query = $this->db->query('select e.equip, c.categoria, t.temporada
				 from equips as e join locals as l on e.id_local = l.id
				 join categories as c on e.id_categoria = c.id
				 join temporades as t on e.id_temporada = t.id
				 where l.id = '.$id);
		return $query->result_array();
	}

	public function delete_local($id) {
		$this->db->where('id', $id);
		$this->db->delete('locals');
	}
    
    //Funcions equips
    public function get_equips() {
        $query = $this->db->get('equips');
        return $query->result_array();
    }
    
    public function insert_equip($info) {
        $this->db->insert('equips', $info);
    }
    
    public function get_equips_select() {
        $query = $this->db->query('select e.id,e.equip,c.categoria
                 from equips as e join categories as c on e.id_categoria = c.id
                 join temporades as t on e.id_temporada = t.id
                 where t.activa = 1');
        return $query->result_array();
    }
    
    public function get_equips_vista() {
        $query = $this->db->query('select e.id,e.equip, e.jerarquia, e.id_categoria, l.local, t.temporada, c.categoria
                 from equips as e join categories as c on e.id_categoria = c.id
                 join locals as l on e.id_local = l.id
                 join temporades as t on e.id_temporada = t.id
                 where t.activa = 1
                 order by e.id_categoria,e.jerarquia desc');
        return $query->result_array();
    }
    public function get_equips_categoria($categoria,$temporada) {
        $query = $this->db->query('select e.id,e.equip,e.jerarquia,c.categoria
                 from equips as e join categories as c on e.id_categoria = c.id
                 join temporades as t on e.id_temporada = t.id
                 where t.id = '.$temporada.' and c.id='.$categoria);
        return $query->result_array();
    }
    
    public function get_num_jugadors($id_equip) {
        $query = $this->db->query('select count(*) as numJugadors
                 from jugadors as j
                 where j.id_equip= '.$id_equip);
        return $query->row_array()['numJugadors'];
    }

	public function get_equip($id) {
		$this->db->where('id',$id);
		$query = $this->db->get('equips');
		return $query->row_array();
	}

	public function update_equip($id,$info) {
		$this->db->where('id',$id);
		$this->db->update('equips',$info);
	}

	public function update_jerarquia($categoria,$equipRef,$ordre) {
		if($ordre == -1) $this->db->query('update equips as e join temporades as t on e.id_temporada = t.id
						 set e.jerarquia = e.jerarquia + '.$ordre.'
						 where t.activa = 1 and e.jerarquia < '.$equipRef.' and e.id_categoria = '.$categoria);
		else if($ordre == 1) $this->db->query('update equips as e join temporades as t on e.id_temporada = t.id
							 set e.jerarquia = e.jerarquia + '.$ordre.'
							 where t.activa = 1 and e.jerarquia > '.$equipRef.' and e.id_categoria = '.$categoria);
		return $equipRef + $ordre;
	}

	public function update_jerarquia_delete($categoria,$equipRef) {
		$this->db->query('update equips as e join temporades as t on e.id_temporada = t.id
				 set e.jerarquia = e.jerarquia + 1
				 where t.activa = 1 and e.jerarquia < '.$equipRef.' and e.id_categoria = '.$categoria);
	}

	public function delete_team($id) {
		$this->db->where('id',$id);
		$this->db->delete('equips');
	}

	public function get_categoria_equip($id) {
		$query = $this->db->query("select c.categoria
			from equips as e join categories as c on e.id_categoria
			where e.id=".$id);
		$categoria = $query->row_array()['categoria'];
		return strtolower($categoria);
	}

	public function get_equips_with_jornades() {
		$query = $this->db->query("select e.id, count(j.id) as numJornades
			   from equips as e join jornades as j on e.id = j.id_equip
			   join temporades as t on e.id_temporada = t.id
			   where t.activa = 1
			   group by e.id
			   having numJornades > 0");
		return $query->result_array();
	}

	private function get_jerarquia($id_equip) {
		$query = $this->db->query("select jerarquia from equips where id = ".$id_equip);
		return $query->row_array()['jerarquia'];
	}

    //Funcions contrincants
    public function get_contrincants() {
        $query = $this->db->get('contrincants');
        return $query->result_array();
    }
    
    public function insert_contrincant($info) {
        $this->db->insert('contrincants', $info);
    }

	public function get_contrincant($id) {
		$this->db->where('id',$id);
		$query = $this->db->get('contrincants');
		return $query->row_array();
	}

	public function update_contrincant($id,$info) {
		$this->db->where('id',$id);
		$this->db->update('contrincants',$info);
    }

    public function get_jornades_from_contrincant($id) {
    	$query = $this->db->query('select j.data
    			 from jornades as j join contrincants as c on j.id_contrincant = c.id
    			 where c.id = '.$id);
    	return $query->result_array();
    }

    public function delete_contrincant($id) {
    	$this->db->where('id',$id);
    	$this->db->delete('contrincants');
    }

    //Funcions jornades
    public function get_jornades_vista() {
        $query = $this->db->query('select j.id, j.jornada, e.equip, t.temporada, cg.categoria, j.data, c.contrincant, j.casa
                 from jornades as j join contrincants as c on j.id_contrincant = c.id
                 join equips as e on j.id_equip = e.id
                 join categories as cg on e.id_categoria = cg.id
                 join temporades as t on e.id_temporada = t.id
                 where t.activa = 1
                 ');
        return $query->result_array();
    }
    
    public function insert_jornada($info) {
        $this->db->insert('jornades', $info);
    }
    
    public function get_jornada_actual($id_equip) {
        $query = $this->db->query('select j.id, j.data,j.jornada, j.casa,j.resultat_equip,j.resultat_contrincant, c.contrincant,c.ciutat,c.codi_postal,c.local,c.carrer,c.pilota,c.taula,c.equipacio
                 from jornades as j join contrincants as c on j.id_contrincant = c.id
                 where j.data > NOW() and j.id_equip ='.$id_equip.' order by j.data asc limit 1');
        return $query->row_array();
    }

    public function get_jornada_ultima($id_equip) {
		$query = $this->db->query('select j.id, j.data,j.jornada, j.casa, j.resultat_equip,j.resultat_contrincant, c.contrincant,c.ciutat,c.codi_postal,c.local,c.carrer,c.pilota,c.taula,c.equipacio
                 from jornades as j join contrincants as c on j.id_contrincant = c.id
                 where j.id_equip ='.$id_equip.' order by j.data desc limit 1');
                return $query->row_array();
    }
    
    public function get_jornada_alineacio($jornada,$id_equip) {
        $query = $this->db->query('select j.id, j.data,j.jornada, j.casa, j.resultat_equip,j.resultat_contrincant, c.contrincant,c.ciutat,c.codi_postal,c.local,c.carrer,c.pilota,c.taula,c.equipacio
                 from jornades as j join contrincants as c on j.id_contrincant = c.id
                 where j.jornada ='.$jornada.' and j.id_equip ='.$id_equip);
        return $query->row_array();
    }
    
    public function get_num_jornades($id_equip) {
        $query = $this->db->query('select count(*) as numJornades
                 from jornades as j
                 where j.id_equip= '.$id_equip);
        return $query->row_array()['numJornades'];
    }

	public function get_num_jornada_ultima($id_equip) {
		$query = $this->db->query('select j.jornada
				 from jornades as j
				 where j.id_equip ='.$id_equip.' order by j.data desc limit 1');
				return $query->row_array()['jornada'];
	}

	public function get_jornada($id) {
		$this->db->where('id',$id);
		$query = $this->db->get('jornades');
		return $query->row_array();
	}

	public function update_jornada($id,$info) {
		$this->db->where('id',$id);
		$this->db->update('jornades',$info);
	}

	public function get_jornades_from_data($data) {
		$query = $this->db->query("select j.id, j.jornada, e.equip, cg.categoria, c.contrincant
				 from jornades as j join contrincants as c on j.id_contrincant = c.id
				 join equips as e on j.id_equip = e.id
				 join categories as cg on e.id_categoria = cg.id
			     where DATE(j.data) = '".$data."'");
		return $query->result_array();
	}

	public function get_equip_from_jornada($id) {
		$query = $this->db->query("select id_equip from jornades where id=".$id);
		return $query->row_array()['id_equip'];
	}

	public function delete_jornada($id) {
		$this->db->where('id',$id);
		$this->db->delete('jornades');
	}

	public function get_info_jornada_email($id) {
		$query = $this->db->query("select j.casa, j.data,
				c.contrincant, c.ciutat, c.codi_postal, c.local, c.carrer,
				e.equip,
				l.local as localEquip,
				ca.categoria
				from jornades as j join equips as e on j.id_equip = e.id
				join locals as l on e.id_local = l.id
				join contrincants as c on j.id_contrincant = c.id
				join categories as ca on e.id_categoria = ca.id
				where j.id = ".$id);
		return $query->row_array();
	}

	public function get_jornada_restriccio($id) {
    	$query = $this->db->query("select j.jornada, e.equip, c.categoria
    	from jornades as j join equips as e on j.id_equip = e.id
    	join categories as c on e.id_categoria = c.id
    	where j.id = ".$id);
    	return $query->row_array();
	}
    
    //Funcions jugadors
    public function insert_jugador($info) {
        $this->db->insert('jugadors', $info);
    }
	
	public function get_jugadors_vista() {
		$query = $this->db->query('select j.*,f.fitxa,c.categoria,e.equip,t.temporada,ed.edat
				from jugadors as j join equips as e on j.id_equip = e.id
				join fitxes as f on j.id_fitxa = f.id
				join categories as c on e.id_categoria = c.id
				join temporades as t on e.id_temporada = t.id
                join edats as ed on j.id_edat = ed.id
                where t.activa = 1');
		return $query->result_array();
	}
	
	public function get_jugadors_nocadena() {
	    $query = $this->db->query('select llicencia,nom,cognom
                 from jugadors
                 where llic_encadenat is null');
	    return $query->result_array();
	}
	
	public function teCadena($jugador) {
	    $this->db->where('llicencia',$jugador);
	    $query = $this->db->get('jugadors');
	    if($query->row_array()['llic_encadenat'] == "") return false;
	    else return true;
	}
	
	public function get_equip_jugador($jugador) {
	    $this->db->where('llicencia',$jugador);
	    $query = $this->db->get('jugadors');
	    return $query->row_array()['id_equip'];
	}
	
	public function encadenar_jugadors($jugadorA,$jugadorB) {
	    $this->db->where('llicencia',$jugadorA);
	    $this->db->set('llic_encadenat',$jugadorB);
	    $this->db->update('jugadors');
	    $this->db->where('llicencia',$jugadorB);
	    $this->db->set('llic_encadenat',$jugadorA);
	    $this->db->update('jugadors');
	}

	public function get_jugador($id) {
		$this->db->where('llicencia',$id);
		$query = $this->db->get('jugadors');
		return $query->row_array();
	}

	public function get_nom_jugador($id) {
    	$query = $this->db->query("select nom,cognom from jugadors where llicencia='".$id."'");
    	return $query->row_array();

	}
	
	public function canvi_estat_jugador($id,$estat) {
	    $this->db->where('llicencia',$id);
	    if($estat) $this->db->set('habilitat',0);
	    else $this->db->set('habilitat',1);
	    $this->db->update('jugadors');
	}

	public function update_jugador($id,$info) {
		$this->db->where('llicencia',$id);
        $this->db->update('jugadors',$info);
	}
	
	public function deshabilitar_data($info) {
	    $this->db->insert('vacances', $info);
	}

	public function delete_data($id) {
		$this->db->where('id',$id);
		$this->db->delete('vacances');
	}

	public function get_dates_no_alineables($llicencia) {
		$this->db->where('llic_jugador',$llicencia);
		$this->db->where('motiu',1);
    	$query = $this->db->get('vacances');
    	return $query->result_array();
	}

	public function get_partits_declinats($llicencia) {
		$query = $this->db->query("select c.contrincant, j.casa, j.data
				 from vacances as v join jornades as j on v.id_jornada = j.id
				 join contrincants as c on j.id_contrincant = c.id
				 where motiu = 2 and llic_jugador='".$llicencia."'");
		return $query->result_array();
	}

	public function buscar_jugador($nom) {
		$query = $this->db->query("select *
				 from jugadors
				 where nom like '%".$nom."%' or cognom like '%".$nom."%'");
		return $query->result_array();
	}

	public function get_jugadors_promocionables($id_equip,$id_categoria) {
		$jerarquia = $this->get_jerarquia($id_equip);
		$query = $this->db->query('select j.*
				 from jugadors as j join equips as e on j.id_equip = e.id
				 where j.promocionable = 1 and e.id_categoria = '.$id_categoria.' and e.jerarquia + 1 = '.$jerarquia);
		return $query->result_array();
	}

	public function delete_jugador($id) {
		$this->db->where('llicencia',$id);
		$this->db->delete('jugadors');
	}

	public function get_jugador_encadenat($id) {
		$query = $this->db->query("select j2.llicencia, j2.nom, j2.cognom from jugadors as j1 join jugadors as j2 on j1.llic_encadenat = j2.llicencia
				 where j1.llicencia = '".$id."'");
		return $query->row_array();
	}

	public function unlink_players($id) {
		$this->db->trans_start();
		$this->db->set('llic_encadenat', null);
		$this->db->where('llicencia', $id);
		$this->db->update('jugadors');

		$this->db->set('llic_encadenat', null);
		$this->db->where('llic_encadenat', $id);
		$this->db->update('jugadors');
		$this->db->trans_complete();
	}

	public function get_jugador_email($id) {
    	$query = $this->db->query("select llicencia,email,nom,cognom from jugadors where llicencia = '".$id."'");
    	return $query->row_array();
	}

	//Funcions jugadors_alineats
	public function insert_jugador_alineat($info) {
	    $this->db->insert('jugadors_alineats', $info);
	}
	
	public function update_jugador_alineat($info) {
	    $where = array(
	        'slot' => $info['slot'],
	        'id_jornada' => $info['id_jornada']
	    );
	    $this->db->where($where);
	    $this->db->update('jugadors_alineats', $info);
	}
	
	public function get_jugadors_habilitats($id_equip) {
	    $query = $this->db->query('select j.* 
        from jugadors as j
        where j.habilitat and id_equip='.$id_equip);
	    return $query->result_array();
	}

	public function get_jugadors_no_disponibles($idEquip,$data) {
		$query = $this->db->query('select j.*
		from jugadors as j join vacances as v on j.llicencia = v.llic_jugador
		where j.id_equip = '.$idEquip.' and v.data = DATE("'.$data.'")');
		return $query->result_array();
	}

	public function get_jugadors_promocionables_no_disponibles($idEquip,$data) {
		$equip = $this->get_equip($idEquip);
		$query = $this->db->query('select j.*
		from jugadors as j join vacances as v on j.llicencia = v.llic_jugador
		join equips as e on j.id_equip = e.id
		where j.promocionable = 1 and v.data = DATE("'.$data.'") and e.jerarquia + 1 = '.$equip['jerarquia'].'
		and e.id_categoria = '.$equip['id_categoria']);
		return $query->result_array();
	}
	
	public function get_jugadors_alineats($id_jornada) {
	    $query = $this->db->query('select j.*, ja.status,ja.promociona
        from jugadors as j join jugadors_alineats as ja on j.llicencia = ja.llic_jugador
        where ja.id_jornada = '.$id_jornada);
	    return $query->result_array();
	}
	
	public function get_jugador_jornada($id,$id_jornada) {
        $query = $this->db->query('select j.*, ja.status
        from jugadors as j join jugadors_alineats as ja on j.llicencia = ja.llic_jugador
        where j.llicencia = "'.$id.'" and ja.id_jornada = '.$id_jornada);
	    return $query->row_array();
	}
	
	public function jugadorEnSlot($slot,$id_jornada) {
	    $query = $this->db->query('select 1
        from jugadors_alineats 
        where slot = '.$slot.' and id_jornada = '.$id_jornada);
	    if(empty($query->result_array())) return false;
	    else return true;
	}

	public function jornada_is_lined_up($id) {
		$this->db->where('id_jornada',$id);
		$query = $this->db->get('jugadors_alineats');
		if(count($query->result_array()) > 0) return true;
		else return false;
	}
	
	public function deleteJugadorEnSlot($slot,$id_jornada) {
	    $this->db->where('slot', $slot);
	    $this->db->where('id_jornada', $id_jornada);
	    $this->db->delete('jugadors_alineats');
	}

	public function esborrarSugerimentAntic() {
		$this->db->where('status', 1);
    	$this->db->delete('jugadors_alineats');
	}
	
	public function jornadaValidada($id_jornada) {
	    $query = $this->db->query('select ja.status
        from jugadors as j join jugadors_alineats as ja on j.llicencia = ja.llic_jugador
        where ja.id_jornada = '.$id_jornada.' limit 1');
	    if(empty($query->row_array()) or $query->row_array()['status'] != 3) return false;
	    else return true;
	}
	
	public function validar_jornada($id_jornada) {
	    $this->db->where('id_jornada',$id_jornada);
	    $this->db->update('jugadors_alineats', array('status' => 3));
	}

	public function getJugadorsBloquejats($id_jornada) {
		$this->db->where('id_jornada',$id_jornada);
		$this->db->where('status', 2);
		$query = $this->db->get('jugadors_alineats');
		return $query->result_array();
	}

	public function get_num_matches_player($llic_jugador) {
    	$query = $this->db->query("select count(*) as partitsJugats from jugadors_alineats
    			 where status = 3 and llic_jugador='".$llic_jugador."'");
    	return $query->row_array()['partitsJugats'];
    }

    public function get_partits_jugats($llicencia) {
    	$query = $this->db->query("select j.resultat_equip, j.resultat_contrincant,j.data,j.casa,c.contrincant
    			 from jugadors_alineats as ja
    			 join jornades as j on ja.id_jornada = j.id
    			 join contrincants as c on j.id_contrincant = c.id
				 where status = 3 and llic_jugador='".$llicencia."'
				 order by j.data asc");
		return $query->result_array();
    }

    public function get_partits_alineats($llicencia) {
    	$query = $this->db->query("select j.data,j.casa,c.contrincant
				 from jugadors_alineats as ja
				 join jornades as j on ja.id_jornada = j.id
				 join contrincants as c on j.id_contrincant = c.id
    		  	 where (status = 1 or status = 2) and llic_jugador='".$llicencia."'
    		  	 order by j.data asc");
    	return $query->result_array();
    }

	public function get_promocions_by_player() {
		$query = $this->db->query("select llic_jugador, edat, categoria, count(*) as numPromocions
			   from `jugadors_alineats` as ja join jugadors as j on ja.llic_jugador = j.llicencia
			   join edats as e on j.id_edat = e.id
			   join equips as eq on j.id_equip = eq.id
			   join categories as c on eq.id_categoria = c.id
			   where promociona = 1 and ja.status > 1
			   group by llic_jugador");
		return $query->result_array();
	}

	public function get_promocions_horitzontals() {
		$query = $this->db->query("select ja.llic_jugador, count(distinct j.id_equip) as differentTeams
					from jugadors_alineats as ja join jornades as j on ja.id_jornada = j.id
					where ja.promociona and ja.status > 1
					group by ja.llic_jugador
					having differentTeams > 1");
		return $query->result_array();
	}

	public function get_jornades_alineades($equip) {
		$query = $this->db->query("select j.nom, j.cognom, jo.jornada
		from jugadors_alineats as ja join jugadors as j on ja.llic_jugador = j.llicencia
		join jornades as jo on ja.id_jornada = jo.id
		where jo.id_equip = ".$equip."
		order by jo.jornada");
		return $query->result_array();
	}

	public function clean_jornades_blocked($id_jornada) {
		$this->db->where('id_jornada',$id_jornada);
		$this->db->delete('jugadors_alineats');
	}

    //PROLOG
    public function get_jugadors_for_prolog() {
    	$query = $this->db->query("select j.llicencia,j.min_partits,j.max_partits,j.responsable,j.transport,j.id_equip,j.promocionable,f.fitxa,ed.edat
				from jugadors as j
				join fitxes as f on j.id_fitxa = f.id
				join edats as ed on j.id_edat = ed.id
				join equips as e on j.id_equip = e.id
				join temporades as t on e.id_temporada = t.id
				where j.habilitat = 1 and t.activa = 1
    	");
    	return $query->result_array();
    }

    public function get_equips_for_prolog() {
    	$query = $this->db->query("select e.id,e.id_categoria,e.jerarquia
    			 from equips as e join temporades as t on e.id_temporada = t.id
    			 where t.activa = 1");
    	return $query->result_array();
    }

    public function get_dates_no_alineables_for_prolog() {
    	$query = $this->db->query("select v.llic_jugador,v. data
    	from vacances as v join jugadors as j on v.llic_jugador = j.llicencia
    	join equips as e on j.id_equip = e.id
    	join temporades as t on e.id_temporada = t.id
    	where t.activa = 1");
    	return $query->result_array();
    }

    public function get_jornades_for_prolog() {
    	$query = $this->db->query("select j.id, j.jornada, j.id_equip, j.data, j.casa
    	from jornades as j join equips as e on j.id_equip = e.id
    	join temporades as t on e.id_temporada = t.id
    	where t.activa = 1");
    	return $query->result_array();
    }

    public function get_cadenes_for_prolog() {
    	$query = $this->db->query("select llicencia, llic_encadenat from jugadors where llic_encadenat not like '' and llicencia > llic_encadenat");
    	return $query->result_array();
    }

    public function get_ja_alineats_for_prolog() {
    	$query = $this->db->query("select ja.llic_jugador, ja.id_jornada
    	from jugadors_alineats as ja join jornades as j on ja.id_jornada = j.id
    	join equips as e on j.id_equip = e.id
    	join temporades as t on e.id_temporada = t.id
    	where (ja.status = 2 or ja.status = 3) and t.activa = 1");
    	return $query->result_array();
    }

    public function backup_suggestion() {
    	$this->db->trans_start();
    	$this->db->empty_table('jugadors_alineats_backup');
    	$query = $this->db->query("insert into jugadors_alineats_backup select * from jugadors_alineats");
    	$this->db->trans_complete();
    }

   public function restore_suggestion() {
   		$this->db->trans_start();
   		$this->db->empty_table('jugadors_alineats');
   		$query = $this->db->query("insert into jugadors_alineats select * from jugadors_alineats_backup");
   		$this->db->trans_complete();
   }
}
