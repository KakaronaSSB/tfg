<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>

    <link href="<?= base_url("assets/css/datatables.min.css"); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/gijgo.min.css'); ?>" rel="stylesheet" />
	<script src="<?= base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
</head>
<body>


<main>

    <div class="container">
        <div class="btn-group">
        	<?php $hidden = array('numContrincants' => $numContrincants); ?>
            <?=form_open(site_url('configuracio/contrincants'),"",$hidden)?>
            <button class="btn btn-outline-primary" type="submit" style="border-radius: 0 !important;" formnovalidate><i class="fa fa-arrow-circle-left"></i> Tornar enrere </a>
			<button class="btn btn-outline-primary" type="submit" name="submit" value="addContrincants" style="border-radius: 0 !important;"><i class="fa fa-check"></i> Validar </a>
        </div>
        <br><br>
        <div class="alert alert-primary">
        	<p>No es necessari omplir els camps <b>Taula</b> i <b>Equipació</b> en cas de desconeixement.</p>
        </div>
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Equip</th>
                        <th>Ciutat</th>
                        <th>Codi Postal</th>
                        <th>Local</th>
                        <th>Carrer</th>
                        <th>Pilota</th>
                        <th>Taula</th>
                        <th>Equipació</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0 ?>
                        <?php while ($i < $numContrincants): ?>
                            <tr>
                            	<td><input class="form-control" id='equip<?=$i;?>' name='equip<?=$i;?>' type='text' maxlength="25" size="25" required/> </td>
                            	<td><input class="form-control" id='ciutat<?=$i;?>' name='ciutat<?=$i;?>' type='text' maxlength="35" size="35"required /> </td>
                                <td><input class="form-control" id='codiPostal<?=$i;?>' name='codiPostal<?=$i;?>' type='text' minlength="5" maxlength="5" size="5" required/> </td>
                                <td><input class="form-control" id='local<?=$i;?>' name='local<?=$i;?>' type='text' maxlength="40" size="40" required/> </td>
                                <td><input class="form-control" id='carrer<?=$i;?>' name='carrer<?=$i;?>' type='text' maxlength="30" size="30" required/> </td>
                                <td><input class="form-control" id='pilota<?=$i;?>' name='pilota<?=$i;?>' type='text' maxlength="15" size="15" required/> </td>
                                <td><input class="form-control" id='taula<?=$i;?>' name='taula<?=$i;?>' type='text' maxlength="15" size="15"/> </td>
                                <td><input class="form-control" id='equipacio<?=$i;?>' name='equipacio<?=$i;?>' type='text' maxlength="10" size="10"/> </td>
                            </tr>
                            <?php ++$i; ?>
                        <?php endwhile; ?>
                </tbody>
            </table>
        </div>
        <?=form_close()?>
        <br>
    </div>
	<br>
</main>

<script>
$(document).ready(function () {	

 });
</script>
</body>


</html>
