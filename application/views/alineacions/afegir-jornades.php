<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>

    <link href="<?= base_url("assets/css/datatables.min.css"); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/datepicker.min.css'); ?>" rel="stylesheet" />
	<script src="<?= base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/datepicker.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/i18n/datepicker.es.js'); ?>"></script>

</head>
<body>


<main>

    <div class="container">
        <div class="btn-group">
        	<?php $hidden = array('numJornades' => $numJornades,'equip' => $equip); ?>
            <?=form_open(site_url('configuracio/jornades'),"",$hidden)?>
            <button class="btn btn-outline-primary" type="submit" style="border-radius: 0 !important;"><i class="fa fa-arrow-circle-left"></i> Tornar enrere </a>
			<button class="btn btn-outline-primary" type="submit" name="submit" value="addJornades" style="border-radius: 0 !important;"><i class="fa fa-check"></i> Validar </a>
        </div>
        <br><br>
		<div class="alert alert-primary">
            <p>Marca la casella <i>Casa</i> si el partit es juga com a <i>Local</i></p>
        </div>
        <div class="table-responsive"> <!-- Sense el responsive no surt l'scroll -->
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Número Jornada</th>
                        <th>Data</th>
                        <th>Contrincant</th>
                        <th>Casa</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0 ?>
                        <?php while ($i < $numJornades): ?>
                            <tr>
                            	<td><input class="form-control" id='numero<?=$i;?>' name='numero<?=$i;?>' type='number' min='1' size='2'/> </td>
                            	<td><input data-timepicker="true" data-language='es'class="datepicker-here form-control" id="<?=$i?>datetimepicker" name="<?=$i?>datetimepicker"/></td>
                                <td>
                                	<select class="form-control" id='contrincant<?=$i;?>' name='contrincant<?=$i;?>'> <!-- Recordar afegir class="form-control" i un form a partir del botò!-->
                                        <?php
                                            foreach ($contrincants as $contrincant):	
                                                echo '<option value="'.$contrincant['id'].'">'.$contrincant['contrincant'].'</option>';
                                            endforeach;
                                        ?>
                                    </select></td>
                                <td style="text-align: center; vertical-align: middle;"><input class="form-control" id='casa<?=$i;?>' name='casa<?=$i;?>'type='checkbox'/></td>
                            </tr>
                            <?php ++$i; ?>
                        <?php endwhile; ?>
                </tbody>
            </table>
        </div>
        <?=form_close()?>
        <br>
    </div>
	<br>
</main>

<script>
$(document).ready(function () {	
	 <?php for($j=0; $j < $numJornades; ++$j) { ?>
	 $('#<?=$j?>datetimepicker').datepicker({
		 		language: 'es',
                timeFormat: 'hh:ii',
                dateFormat: 'yyyy-mm-dd',
     });
	 $('#<?=$j?>datetimepicker').keydown(function() {
		return false;
	 });
	 <?php } ?>	
 
 });
</script>
</body>


</html>
