<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>

    <link href="<?= base_url("assets/css/datatables.min.css"); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet" />
	<script src="<?= base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
	
	<style>

	</style>

</head>
<body>


<main>

    <div class="container">
        <div class="btn-group">
        	<?php $hidden = array('numJugadors' => $numJugadors); ?>
            <?=form_open(site_url('configuracio/jugadors'),"",$hidden)?>
            <button class="btn btn-outline-primary" type="submit" style="border-radius: 0 !important;" formnovalidate><i class="fa fa-arrow-circle-left"></i> Tornar enrere </a>
			<button class="btn btn-outline-primary" type="submit" name="submit" value="addPlayers" style="border-radius: 0 !important;"><i class="fa fa-check"></i> Validar </a>
        </div>
        <br><br>
        <div class="alert alert-primary">
        	<p>A <i>Partits Esperats</i> introdueix el <b>mínim</b> de partits esperats a l'esquerra i el <b>màxim</b> a la dreta. La llicencia ha d'estar precedida per la inicial de la categoria.</p>
        </div>
        <div class="table-responsive "> <!-- Sense el responsive no surt l'scroll -->
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th>Llicència</th>
                        <th>Nom</th>
                        <th>Cognom</th>
                        <th>Email</th>
                        <th>Fitxa</th>
                        <th>Edat</th>
                        <th>Equip</th>
                        <th>Partits Esperats </th>
                        <th>Transport</th>
                        <th>Responsable</th>
                        <th>Promocionable</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0 ?>
                        <?php while ($i < $numJugadors): ?>
                            <tr>
                            	<td><input class="form-control" id='llicencia<?=$i;?>' name='llicencia<?=$i;?>' type='text' maxlength="6" size="6" required/> </td>
                            	<td><input class="form-control" id='nom<?=$i;?>' name='nom<?=$i;?>' type='text' maxlength="15" size="15" required/> </td>
                                <td><input class="form-control" id='cognom<?=$i;?>' name='cognom<?=$i;?>' type='text' maxlength="15" size="15" required/> </td>
                                <td><input class="form-control" id='email<?=$i;?>' name='email<?=$i;?>' type='email' maxlength="50" size="15" required/> </td>
                                <td>
                                	<select class="form-control" id='fitxa<?=$i;?>' name='fitxa<?=$i;?>' required> <!-- Recordar afegir class="form-control" i un form a partir del botò!-->
                                        <?php
                                            foreach ($fitxes as $fitxa):	
                                                echo '<option value="'.$fitxa['id'].'">'.$fitxa['fitxa'].'</option>';
                                            endforeach;
                                        ?>
                                    </select>
								</td>
								<td>
									<select class="form-control" id='edat<?=$i;?>' name='edat<?=$i;?>' required>
                                        <?php
                                            foreach ($edats as $edat):	
                                                echo '<option value="'.$edat['id'].'">'.$edat['edat'].'</option>';
                                            endforeach;
                                        ?>
                                    </select>
                                </td>
                                <td>
									<select class="form-control" id='equip<?=$i;?>' name='equip<?=$i;?>' required>
                                        <?php
                                            foreach ($equips as $equip):	
                                                echo '<option value="'.$equip['id'].'">'.$equip['equip'].' '.substr($equip['categoria'],0,3).'</option>';
                                            endforeach;
                                        ?>
                                    </select>
                                </td>
                                <td style='width:14%'>
                                	<div style='width:100%'>
                                		<input class="half-td form-control" id='minGames<?=$i;?>' name='minGames<?=$i;?>' type='number' min='0' placeholder='Min' required/>
                                		<input class="half-td form-control" id='maxGames<?=$i;?>' name='maxGames<?=$i;?>' type='number' min='0' placeholder='Max' required/>
                                	</div>
                                </td>
                                <td style="text-align: center; vertical-align: middle;"><input class="form-control" id='transport<?=$i;?>' name='transport<?=$i;?>'type='checkbox'/></td>
                                <td style="text-align: center; vertical-align: middle;"><input class="form-control" id='responsable<?=$i;?>' name='responsable<?=$i;?>'type='checkbox'/></td>
                                <td style="text-align: center; vertical-align: middle;"><input class="form-control" id='promocionable<?=$i;?>' name='promocionable<?=$i;?>'type='checkbox'/></td>
                            </tr>
                            <?php ++$i; ?>
                        <?php endwhile; ?>
                </tbody>
            </table>
        </div>
        <?=form_close()?>
        <br>
    </div>
	<br>
</main>

<script>
$(document).ready(function () {	

 });
</script>
</body>


</html>
