<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>
	<meta name="viewport" content="width=device-width" />

    <link href="<?= base_url("assets/css/datatables.min.css"); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/css/rpg-awesome.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet" />
	<link href="https://fonts.googleapis.com/css?family=Ubuntu&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Orbitron&display=swap" rel="stylesheet">
	<script src="<?= base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/popper.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/jquery-ui.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/jquery-dateformat.min.js'); ?>"></script>
	
</head>
<body>


<main>
	<div class="hidden-values-jornada-fix">
		<input id='idEquip' name='idEquip' value='<?=$id_equip?>' hidden/>
        <input id='idCategoria' name='idCategoria' value='<?=$categoria?>' hidden/>
	</div>
	<div class="hidden-values-jornada"></div>
    <div class="container">
		<div class="btn-group">
            <a href="<?= site_url('configuracio') ?>" class="btn btn-outline-primary"><i class="fa fa-cogs"></i> Configuració </a>
			<a href="<?= site_url('alineacions/territorial') ?>" class="btn btn-primary"><i class="fa fa-th"></i> Alineacions </a>
			<a href="<?= site_url('consultes') ?>" class="btn btn-outline-primary"><i class="fa fa-search"></i> Consultes </a>
        </div>
        <br><br>
        <div class="btn-group">
            <a href="<?= site_url('alineacions/territorial') ?>" class="btn <?php if($categoria==1): echo "btn-primary"; else: echo "btn-outline-primary"; endif;?>"><i class="ra ra-trophy"></i> Territorial </a>
            <a href="<?= site_url('alineacions/nacional') ?>" class="btn <?php if($categoria==2): echo "btn-primary"; else: echo "btn-outline-primary"; endif;?>"><i class="ra ra-trophy"></i> Nacional </a>
            <a href="<?= site_url('alineacions/veterans') ?>" class="last-btn btn <?php if($categoria==3): echo "btn-primary"; else: echo "btn-outline-primary"; endif;?>"><i class='ra ra-trophy'></i> Veterans </a>
			<div class="btn-group dropdown">
              <button type="button" class="btn dropdown-toggle btn-generador btn-outline-dark" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class='fa fa-bars'></i> Accions </button>
              <div class="dropdown-menu">
              	<a onclick="loadSpinner()" href="<?= site_url('alineacions/generar') ?>" class="dropdown-item btn "><i class='fa fa-cog'></i> Generar sugeriment </a>
              	<a onclick="loadSpinner()" href="<?= site_url('alineacions/validar') ?>" class="dropdown-item btn "><i class='fa fa-check'></i> Validar estat actual </a>
              	<a href="<?= site_url('alineacions/rolback') ?>" class="dropdown-item btn "><i class='fa fa-backward'></i> Tornar a estat anterior </a>
              </div>
            </div>
        </div>
        <br><br>
        <div class="btn-group">
        	<?php foreach($equips as $equip):?>
        		<?php if($equip['id']==$id_equip):$class = "btn btn-primary"; else: $class="btn btn-outline-primary";endif;?>
            	<?php if($categoria==1): echo '<a href="'.site_url('alineacions/territorial/'.$equip['id']).'"'."class='$class'><i class='ra ra-ping-pong'></i>"." ".$equip['equip']."</a>" ?>
            	<?php elseif($categoria==2): echo '<a href="'.site_url('alineacions/nacional/'.$equip['id']).'"'."class='$class'><i class='ra ra-ping-pong'></i>"." ".$equip['equip']."</a>" ?>
            	<?php elseif($categoria==3): echo '<a href="'.site_url('alineacions/veterans/'.$equip['id']).'"'."class='$class'><i class='ra ra-ping-pong'></i>"." ".$equip['equip']."</a>" ?>
            	<?php endif;?>
            <?php endforeach;?>
        </div>
        <br><br>
		<?php if(isset($maxPromoError)):?>
			<?php foreach($maxPromoError as $jugador):?>
				<?php  error("Error: El jugador ".$jugador." està bloquejat o validat en masses partits promocionant"); ?>
			<?php endforeach; ?>
		<?php endif; ?>
		<?php if(isset($horitzontalPromoError)):?>
			<?php foreach($horitzontalPromoError as $jugador):?>
				<?php  error("Error: El jugador ".$jugador['llic_jugador']." està promocionant horitzontalment"); ?>
			<?php endforeach; ?>
		<?php endif; ?>
		<?php if(isset($faltenJugadorsError)):?>
			<?php foreach($faltenJugadorsError as $equip):?>
				<?php  error("Error: L'equip ".$equip." no arriba al mínim de 3 jugadors"); ?>
			<?php endforeach; ?>
		<?php endif; ?>
        <?php if(isset($jornades_totals)): ?>
            <?php if($jornades_totals > 0 and $consistencia):?>
                <div class='alineacio-container'> 
                	<div class='alineacio-info'>
            		</div>
                	<div class='alineacio-equip'>
                		<div class='jornada'> </div>
                    	<div class='equip'> </div>
                    	<div class='botons-container'>
                    		<div class='boto-alineacio'><i onclick="addPromocionables(<?=$id_equip?>,<?=$categoria?>)" id="butProm" class='fa fa-user-plus fa-3x boto-ali'></i></div>
                    		<div class='boto-alineacio'><i onclick='genBlockJornada()' class='fa fa-lock fa-3x boto-ali' id="butBlock" data-toggle="modal" data-target="#blockJornada"></i></div>
                    		<div class='boto-alineacio'><i onclick='genSendEmails()' class='fa fa-envelope fa-3x boto-ali' id="butMail" data-toggle="modal" data-target="#sendEmails"></i></div>
                    		<div class='boto-alineacio'><i onclick='genValidateJornada()' class='fa fa-check fa-3x boto-ali' id="butVal" data-toggle="modal" data-target="#validateJornada"'></i></div>
                    	</div>
                    	<div class='fletxes-jornada'>
                    		<div class='fletxa-esquerra'></div>
                    		<div class='fletxa-dreta'></div>
                    	</div>
                	</div>
                	<div class='alineacio-jugadors'>
                		<div class='alineacio-text'> <p>Jugadors Disponibles</p> </div>
                		<div class='jugadors-container'></div>
                	</div>
                	<div class='alineacio-resultat'>
						<div class='alineacio-text'> <p>Resultat</p> </div>
						<div class='resultat-container'></div>
					</div>
                </div>
        	<?php elseif($jornades_totals == 0):?>
                <div class="alert alert-primary">
                	<p>No hi ha jornades disponibles per a aquest equip</p>
                </div>
            <?php elseif(!$consistencia):?>
             <div class="alert alert-warning">
				<p>Hi ha alguna inconsistència amb els nombres de jornades d'aquest equip</p>
			 </div>
            <?php endif;?>
		<?php endif;?>

        </div>


</main>

<form action="<?= site_url("alineacions/blockJornada") ?>" method="post">
    <div class="modal fade" id="blockJornada" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Bloquejar Jornada</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                	<div class="hidden-values-bloquejar"></div>
                	<div class="row">
                		<div class="form-group llic-jugador1 col-sm-12 col-md-4"></div>
                		<div class="form-group block-jugador1 col-sm-12 col-md-4"></div>
                		<div class="form-group col-sm-8 col-md-4">
                			<label class="switch switch-jugador1"></label>
                		</div>
                	</div>
                	<div class="row">
                		<div class="form-group llic-jugador2 col-sm-12 col-md-4"></div>
                		<div class="form-group block-jugador2 col-sm-12 col-md-4"></div>
                		<div class="form-group col-sm-12 col-md-4">
                			<label class="switch switch-jugador2"></label>
                		</div>
                	</div>
                	<div class="row">
                		<div class="form-group llic-jugador3 col-sm-12 col-md-4"></div>
                		<div class="form-group block-jugador3 col-sm-12 col-md-4"></div>
                		<div class="form-group col-sm-12 col-md-4">
                			<label class="switch switch-jugador3"></label>
                		</div>
                	</div>
                	<div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Guardar</button>
                	</div>
           		 </div>
        	</div>
    	</div>
    </div>
</form>

<form action="<?= site_url("alineacions/sendEmails") ?>" method="post">
	<div class="modal fade" id="sendEmails" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Enviar Emails</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="alert alert-primary email-alert"></div>
					<div class="hidden-values-emails"></div>
					<div class="row">
						<div class="form-group email-nom-jugador1"></div>
						<div class="form-group email-llic-jugador1 col-sm-12 col-md-3"></div>
						<div class="form-group email-jugador1 col-sm-12 col-md-7"></div>
						<div class="form-group col-sm-8 col-md-2">
							<label class="switch email-switch-jugador1"></label>
						</div>
					</div>
					<div class="row">
						<div class="form-group email-nom-jugador2"></div>
						<div class="form-group email-llic-jugador2 col-sm-12 col-md-3"></div>
						<div class="form-group email-jugador2 col-sm-12 col-md-7"></div>
						<div class="form-group col-sm-12 col-md-2">
							<label class="switch email-switch-jugador2"></label>
						</div>
					</div>
					<div class="row">
						<div class="form-group email-nom-jugador3"></div>
						<div class="form-group email-llic-jugador3 col-sm-12 col-md-3"></div>
						<div class="form-group email-jugador3 col-sm-12 col-md-7"></div>
						<div class="form-group col-sm-12 col-md-2">
							<label class="switch email-switch-jugador3"></label>
						</div>
					</div>
					<div class="modal-footer email-footer"></div>
				</div>
			</div>
		</div>
	</div>
</form>

<form action="<?= site_url("alineacions/validateJornada") ?>" method="post">
    <div class="modal fade" id="validateJornada" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Validar Jornada</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                	<div class="alert alert-primary val-alert"></div>
                	<div class="hidden-values-validar"></div>
                	<div class="row">
                		<div class="form-group col-sm-12 col-md-6 val-resultat-equip"></div>
                		<div class="form-group col-sm-12 col-md-6 val-resultat-contrincant"></div>
                	</div>
                	<div class="modal-footer"></div>
           		 </div>
        	</div>
    	</div>
    </div>
</form>

<div class="modal fade" id="constraints" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Restriccions no sastisfetes</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    	<span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
					<ul class="list-group list-group-flush">
					<?php foreach($constraints as $constraint): ?>
					  <li class="list-group-item"><?= $constraint ?></li>
					<?php endforeach; ?>
					</ul>
                <div class="modal-footer"></div>
           		 </div>
        	</div>
    	</div>
    </div>

    <div class="modal fade bd-example-modal-lg" id="spinner" data-backdrop="static" data-keyboard="false" tabindex="-1">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="spinner-border"></div>
            </div>
        </div>
    </div>


<script>

	function loadSpinner() {
		$('#spinner').modal();
	}

<?php if(isset($jornades_totals) and $jornades_totals > 0): ?>
	function loadJornada(numJornada,idEquip) {
		$.ajax({url: "<?= site_url("alineacions/getJornada/") ?>"+numJornada+"/"+idEquip, success: function(result){

			var data = JSON.parse(result);
			var jornada = data.jornada;
			var jugadors = data.jugadors;
			var jugadors_alineats = data.jugadors_alineats;
			var local = data.local;


			if(jornada.validada) {
				$(".botons-container").hide();
				$(".alineacio-jugadors").hide();
				$(".alineacio-resultat").show();
			}
			else {
				$(".botons-container").show();
				$(".alineacio-jugadors").show();
				$(".alineacio-resultat").hide();
			}
			
        	if(jornada.jornada > 1){
            	$('.fletxa-esquerra').show();
            	if(screen.width <= 900) $('.fletxa-dreta').css('margin-left', '70.5%');
            	else $('.fletxa-dreta').css('margin-left', '78.5%');
        	}
        	
            else {
                $('.fletxa-esquerra').hide();
                if(screen.width <= 900) $('.fletxa-dreta').css('margin-left', '+=16.50%');
                else $('.fletxa-dreta').css('margin-left', '+=11.25%');
            }

            if(jornada.jornada == <?=$jornades_totals?> || <?=$jornades_totals?> == 1) {
            	$('.fletxa-dreta').hide();
            }
            else {
                $('.fletxa-dreta').show();
            }

			$(".hidden-values-jornada").empty();
			$(".hidden-values-jornada").append("<input id='numJornada' name='numJornada' value='"+jornada.jornada+"' hidden/>");
			$(".hidden-values-jornada").append("<input id='idJornada' name='idJornada' value='"+jornada.id+"' hidden/>");

			var data = 	$.format.date(jornada.data, "dd-MM-yyyy hh:mm");

			$(".jornada").empty();
        	$(".jornada").append("<div class='alineacio-text'> <p> <b>Jornada "+jornada.jornada+"</b> "+data+"</p> </div>");

        	$(".alineacio-info").empty();
        	$(".alineacio-info").append("<div class='alineacio-text'> <p> <b>"+jornada.contrincant+"</b></div>");

        	if(jornada.casa == 0) {
				if(jornada.ciutat.length < 20) $(".alineacio-info").append("<div class='alineacio-text'> <p>"+jornada.ciutat+" ("+jornada.codi_postal+")"+"</div>");
				else  $(".alineacio-info").append("<div class='alineacio-text'> <p>"+jornada.ciutat+"</div>");
				$(".alineacio-info").append("<div class='alineacio-text'> <p>"+jornada.local+"</div>");
				$(".alineacio-info").append("<div class='alineacio-text'> <p>"+jornada.carrer+"</div>");
				$(".alineacio-info").append("<div class='alineacio-text'> <p> <b>Pilota:</b> "+jornada.pilota+"</div>");
				if(jornada.taula !== "" && jornada.taula !== null) $(".alineacio-info").append("<div class='alineacio-text'> <p> <b>Taula:</b> "+jornada.taula+"</div>");
				if(jornada.equipacio !== "" && jornada.equipacio !== null) $(".alineacio-info").append("<div class='alineacio-text'> <p> <b>Equipació:</b> "+jornada.equipacio+"</div>");
			}
			else {
				$(".alineacio-info").append("<div class='alineacio-text'> <p>"+local.ciutat+" ("+jornada.codi_postal+")"+"</div>");
				$(".alineacio-info").append("<div class='alineacio-text'> <p>"+local.local+"</div>");
				$(".alineacio-info").append("<div class='alineacio-text'> <p>"+local.carrer+"</div>");
				$(".alineacio-info").append("<div class='alineacio-text'> <p> <b>Pilota </b>"+local.pilota+" </div>");
				if(local.taula !== "" && local.taula !== null) $(".alineacio-info").append("<div class='alineacio-text'> <p> <b> Taula: </b> "+local.taula+"</div>");
				if(local.equipacio !== "" && local.equipacio !== null) $(".alineacio-info").append("<div class='alineacio-text'> <p> <b>Equipació:</b> "+local.equipacio+"</div>");
			}

			$(".equip").empty();
			for(var i=0;i<jugadors_alineats.length;i++){
                var transport = "";
                var responsable = "";
                var status = "";
                var promociona="";
                if(jugadors_alineats[i].status == 2) status ="blocked locked";
                else if(jugadors_alineats[i].status == 3) status ="validated locked";
                if(jugadors_alineats[i].promociona == 1) promociona="-promocionable";
            	if(jugadors_alineats[i].transport == 1) transport ="<i class='fa fa-car'> </i>";
            	if(jugadors_alineats[i].responsable == 1) responsable ="<i class='ra ra-aware'> </i>";
				$(".equip").append("<div class='"+status+" jugador"+promociona+"' id='"+jugadors_alineats[i].llicencia+"'> <div class='jugador-responsable'>"+responsable+"</div> <div class='jugador-transport'>"+transport+"</div> <div class='jugador-nom'> <p>"+jugadors_alineats[i].nom+" "+jugadors_alineats[i].cognom+"</p> </div> </div>");
            }
			
        	$(".jugadors-container").empty();
            for(var i=0;i<jugadors.length;i++){
                var transport = "";
                var responsable = "";
            	if(jugadors[i].transport == 1) transport ="<i class='fa fa-car'> </i>";
            	if(jugadors[i].responsable == 1) responsable ="<i class='ra ra-aware'> </i>";
				$(".jugadors-container").append("<div class='jugador' id='"+jugadors[i].llicencia+"'> <div class='jugador-responsable'>"+responsable+"</div> <div class='jugador-transport'>"+transport+"</div> <div class='jugador-nom'> <p>"+jugadors[i].nom+" "+jugadors[i].cognom+"</p> </div> </div>");
            }

            $(".resultat-container").empty();
            if(jornada.casa == 1) $(".resultat-container").append("<p>"+jornada.resultat_equip+" - "+jornada.resultat_contrincant+"</p>");
            else $(".resultat-container").append("<p>"+jornada.resultat_contrincant+" - "+jornada.resultat_equip+"</p>");
		}})
	}
	<?php endif; ?>

	function addPromocionables(idEquip,idCategoria) {
		var idJornada = $("#idJornada").val();
		var jugadors = [];
		$('.equip').children().each(function () { jugadors.push(this.id);});

		$.ajax({type: "POST", data: {jugadors:jugadors},url: "<?= site_url("alineacions/getJugadorsPromocionables/") ?>"+idEquip+"/"+idCategoria+"/"+idJornada, success: function(result){

			var jugadors = JSON.parse(result);

            $(".jugadors-container").find(".jugador-promocionable").remove();
            
			for(var i=0;i<jugadors.length;i++){
				var transport = "";
				var responsable = "";
				if(jugadors[i].transport == 1) transport ="<i class='fa fa-car'> </i>";
				if(jugadors[i].responsable == 1) responsable ="<i class='ra ra-aware'> </i>";
				$(".jugadors-container").append("<div class='jugador-promocionable' id='"+jugadors[i].llicencia+"'> <div class='jugador-responsable'>"+responsable+"</div> <div class='jugador-transport'>"+transport+"</div> <div class='jugador-nom'> <p>"+jugadors[i].nom+" "+jugadors[i].cognom+"</p> </div> </div>");
			}

            }})

	}

	function genValidateJornada() {

		var idJornada = $("#idJornada").val();

		var numJornada = $("#numJornada").val();
		var idEquip = $("#idEquip").val();
		var idCategoria = $("#idCategoria").val();

		$(".hidden-values-validar").empty();
		$(".hidden-values-validar").append("<input id='idJornadaValidar' name='idJornadaValidar' value='"+idJornada+"' hidden/>");

		$(".hidden-values-validar").append("<input id='numJornadaValidar' name='numJornadaValidar' value='"+numJornada+"' hidden/>");
		$(".hidden-values-validar").append("<input id='idEquipValidar' name='idEquipValidar' value='"+idEquip+"' hidden/>");
		$(".hidden-values-validar").append("<input id='idCategoriaValidar' name='idCategoriaValidar' value='"+idCategoria+"' hidden/>");


		$(".modal-footer").empty();
		$(".val-alert").empty();

		$(".val-resultat-equip").empty();
		$(".val-resultat-contrincant").empty();

		$(".modal-footer").append("<button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancelar</button>");

		if($('.equip').children().length == 3) {

			var i = 1;
			$('.equip').children().each(function () {
				$(".hidden-values-validar").append("<input class='text' id='validar-J"+i+"' name='validar-J"+i+"' value="+this.id+" hidden/>");
				i = i + 1;
			});
			$(".modal-footer").append("<button type='submit' class='btn btn-success'>Validar</button>");
			$(".val-alert").append("<p>Estàs segur de que desitges validar aquesta jornada? No es podrà editar més tard.</p>");
			$(".val-resultat-equip").append("<label for='resultatEquip'>Resultat de l'Equip</label>");
			$(".val-resultat-equip").append("<input type='number' class='form-control' id='resultatEquip' name='resultatEquip' min=0 max=6 required>");
            $(".val-resultat-contrincant").append("<label for='resultatContrincant'>Resultat del Contrincant</label>");
            $(".val-resultat-contrincant").append("<input type='number' class='form-control' id='resultatContrincant' name='resultatContrincant' min=0 max=6 required>");
		}
		else $(".val-alert").append("<p>No es pot validar una jornada si no està composta per 3 jugadors.</p>");

	}

	function genBlockJornada() {

		var idJornada = $("#idJornada").val();
		var numJornada = $("#numJornada").val();
		var idEquip = $("#idEquip").val();
		var idCategoria = $("#idCategoria").val();

		$(".hidden-values-bloquejar").empty();
        $(".hidden-values-bloquejar").append("<input id='idJornadaBloqujear' name='idJornadaBloquejar' value='"+idJornada+"' hidden/>");

		$(".hidden-values-bloquejar").append("<input id='numJornadaBloquejar' name='numJornadaBloquejar' value='"+numJornada+"' hidden/>");
		$(".hidden-values-bloquejar").append("<input id='idEquipBloquejar' name='idEquipBloquejar' value='"+idEquip+"' hidden/>");
		$(".hidden-values-bloquejar").append("<input id='idCategoriaBloquejar' name='idCategoriaBloquejar' value='"+idCategoria+"' hidden/>");

		$(".llic-jugador1").empty();
		$(".block-jugador1").empty();
		$(".switch-jugador1").empty();
		
		$(".llic-jugador2").empty();
		$(".block-jugador2").empty();
		$(".switch-jugador2").empty();

		$(".llic-jugador3").empty();
		$(".block-jugador3").empty();
		$(".switch-jugador3").empty();


        var jugadors = [];
		
		$('.equip').children().each(function () { jugadors.push(this.id);});

		$.ajax({type: "POST", data: {jugadors:jugadors},url: "<?= site_url("alineacions/getJugadorsToBlock/") ?>"+idJornada,success: function(result){
			var data = JSON.parse(result);
			var checked;
			for(var i=1;i<=data.length;++i){
				if(data[i-1].status == 2) checked = "checked";
				else checked = "";
				$(".llic-jugador"+i).append("<input id='alinear-J"+i+"' name='alinear-J"+i+"' class='form-control' type='text' value='"+data[i-1].llicencia+"' readonly />");
				$(".block-jugador"+i).append("<input class='form-control' type='text' value='"+data[i-1].nom+" "+data[i-1].cognom+"' readonly />");
				$(".switch-jugador"+i).append("<input id='check-J"+i+"' name='check-J"+i+"' class='form-control custom-control-input' type='checkbox' "+checked+" />");
				$(".switch-jugador"+i).append("<span class='slider round'></span>");
			}
		}})
    }

    function genSendEmails() {

        var idJornada = $("#idJornada").val();
		var numJornada = $("#numJornada").val();
		var idEquip = $("#idEquip").val();
		var idCategoria = $("#idCategoria").val();


        $(".hidden-values-emails").empty();
        $(".hidden-values-emails").append("<input id='idJornadaEmail' name='idJornadaEmail' value='" + idJornada + "' hidden/>");

		$(".hidden-values-emails").append("<input id='numJornadaEmails' name='numJornadaEmails' value='"+numJornada+"' hidden/>");
		$(".hidden-values-emails").append("<input id='idEquipEmails' name='idEquipEmails' value='"+idEquip+"' hidden/>");
		$(".hidden-values-emails").append("<input id='idCategoriaEmails' name='idCategoriaEmails' value='"+idCategoria+"' hidden/>");

        $(".email-nom-jugador1").empty();
        $(".email-llic-jugador1").empty();
        $(".email-jugador1").empty();
        $(".email-switch-jugador1").empty();

		$(".email-nom-jugador1").empty();
        $(".email-llic-jugador2").empty();
        $(".email-jugador2").empty();
        $(".email-switch-jugador2").empty();

		$(".email-nom-jugador1").empty();
        $(".email-llic-jugador3").empty();
        $(".email-jugador3").empty();
        $(".email-switch-jugador3").empty();


        $(".email-footer").empty();
        $(".email-alert").empty();

        $(".email-footer").append("<button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancelar</button>");
        if ($('.equip').children().length == 3) {
            var jugadors = [];
            $('.equip').children().each(function () {
                jugadors.push(this.id);
            });

            $.ajax({
                type: "POST",
                data: {jugadors: jugadors},
                url: "<?= site_url("alineacions/getJugadorsToSendEmail") ?>",
                success: function (result) {
                    var data = JSON.parse(result);
                    for (var i = 1; i <= 3; ++i) {
                        $(".email-nom-jugador" + i).append("<input id='nom-J" + i + "' name='nom-J" + i + "' class='form-control' type='text' value='" + data[i - 1].nom+ " "+data[i - 1].cognom + "' hidden />");
                        $(".email-llic-jugador" + i).append("<input id='email-llic-J" + i + "' name='email-llic-J" + i + "' class='form-control' type='text' value='" + data[i - 1].llicencia + "' readonly />");
                        $(".email-jugador" + i).append("<input class='form-control' type='text' id='email-J"+ i+ "' name='email-J"+i+"' value='" + data[i - 1].email + "' readonly />");
                        $(".email-switch-jugador" + i).append("<input id='send-J" + i + "' name='send-J" + i + "' class='form-control custom-control-input' type='checkbox'/>");
                        $(".email-switch-jugador" + i).append("<span class='slider round'></span>");
                    }
					$(".email-alert").append("<p>Selecciona els jugadors als que vols enviar la convocatoria.</p>");
                    $(".email-footer").append("<button type='submit' class='btn btn-success'>Enviar</button>");
                }
            })
        }
        else $(".email-alert").append("<p>No es poden enviar els correus d'una jornada si aquesta no està composta per 3 jugadors.</p>");
    }

	$(document).ready(function(){
		<?php if(isset($jornades_totals) and $jornades_totals > 0): ?> loadJornada(<?=$num_jornada?>,<?=$id_equip?>); <?php endif; ?>
    	$('.jugadors-container').sortable({
    		connectWith: ".equip",
			start: function(e, ui){
				$(".equip").css('border', '2px dashed black');
			},
			stop: function(e, ui){
				$(".equip").css('border', '0px');
			}
    	});
    	
    	$(".fletxa-esquerra").on("click", function(){
            loadJornada(parseInt($('#numJornada').val())-1,<?=$id_equip?>);
        })
        
        $(".fletxa-dreta").on("click", function(){
            loadJornada(parseInt($('#numJornada').val())+1,<?=$id_equip?>);
   		})
    	$('.equip').sortable({
    		connectWith: ".jugadors-container",
    		items: '> div:not(.locked)',
			start: function(e, ui){
				$(".equip").css('border', '2px dashed black');
			},
			stop: function(e, ui){
				$(".equip").css('border', '0px');
			},
    		receive: function(event, ui) {
                if ($(this).children().length > 3) $(ui.sender).sortable('cancel');
            }
        }).disableSelection();
        <?php if(isset($constraints)): ?>
        	$('#constraints').modal();
        <?php endif; ?>

		if(screen.width <= 900) {
			$("#butBlock").attr('class', 'fa fa-lock fa-2x boto-ali');
			$("#butMail").attr('class', 'fa fa-envelope fa-2x boto-ali');
			$("#butProm").attr('class', 'fa fa-user-plus fa-2x boto-ali');
			$("#butVal").attr('class', 'fa fa-check fa-2x boto-ali');
		}
	})
</script>

</body>
</html>
