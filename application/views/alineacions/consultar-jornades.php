<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>

    <link href="<?= base_url("assets/css/datatables.min.css"); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/css/rpg-awesome.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet" />
	<script src="<?= base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/popper.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
	
</head>
<body>


<main>
    <div class="container">
		<div class="btn-group">
            <a href="<?= site_url('configuracio') ?>" class="btn btn-outline-primary"><i class="fa fa-cogs"></i> Configuració </a>
			<a href="<?= site_url('alineacions/territorial') ?>" class="btn btn-outline-primary"><i class="fa fa-th"></i> Alineacions </a>
			<a href="<?= site_url('consultes') ?>" class="btn btn-primary"><i class="fa fa-search"></i> Consultes </a>
        </div>
        <br><br>
        <div class="btn-group">
            <a href="<?= site_url('consultes/') ?>" class="btn btn-outline-primary"><i class="fa fa-user"></i> Consultar Jugadors </a>
            <a href="<?= site_url('consultes/jornades') ?>" class="btn btn-primary"><i class="fa fa-calendar"></i> Consultar Jornades </a>
        </div>
        <br><br>
		<div class="btn-group">
			<div class="btn-group dropdown">
			  <button type="button" class="btn dropdown-toggle btn-outline-primary" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class='fa fa-bars'></i> Equips </button>
			  <div class="dropdown-menu">
				<?php if(isset($equips_1)): ?>
					  <h6 class="dropdown-header">Territorial</h6>
					  <?php foreach($equips_1 as $equip): ?>
					  	<a href="<?= site_url('consultes/jornades/'.$equip['id']) ?>" class="dropdown-item btn "><?=$equip['equip']?> </a>
					  <?php endforeach; ?>
				<?php endif; ?>
				<?php if(isset($equips_2)): ?>
					<h6 class="dropdown-header">Nacional</h6>
			    	<?php foreach($equips_2 as $equip): ?>
						<a href="<?= site_url('consultes/jornades/'.$equip['id']) ?>" class="dropdown-item btn "><?=$equip['equip']?> </a>
			    	<?php endforeach; ?>
				<?php endif; ?>
				<?php if(isset($equips_3)): ?>
					<h6 class="dropdown-header">Veterans</h6>
					<?php foreach($equips_3 as $equip): ?>
						<a href="<?= site_url('consultes/jornades/'.$equip['id']) ?>" class="dropdown-item btn "><?=$equip['equip']?> </a>
					<?php endforeach; ?>
				<?php endif; ?>
			  </div>
			</div>
		</div>
		<br><br>
		<?php if(isset($jornades)): ?>
			<?php foreach($jornades as $jornades3): ?>
				<div class="row">
            		<?php foreach($jornades3 as $jornada): ?>
            			<div class="col-4 consulta-jornada">
            				<div class="col-border">
							  <a href="<?= site_url('alineacions/'.$categoria.'/'.$id_equip.'/'.$jornada[0]['jornada']) ?>" class='text-grid'> <b> Jornada <?=$jornada[0]['jornada']?> </b> </a>
							  <hr class='jornada'>
							  <?php foreach($jornada as $jugador): ?>
								  <p class='text-grid'> <?=$jugador['nom'].' '.$jugador['cognom']?> </p>
							  <?php endforeach; ?>
							  </div>
						</div>
            		<?php endforeach; ?>
        		</div>
        	<?php endforeach; ?>
        <?php endif; ?>
    </div>
</main>

<script>

$(document).ready(function () {	

 });
</script>
</body>


</html>
