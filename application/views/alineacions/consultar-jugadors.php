<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>

    <link href="<?= base_url("assets/css/datatables.min.css"); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/css/rpg-awesome.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet" />
	<script src="<?= base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
	
</head>
<body>


<main>
    <div class="container">
		<div class="btn-group">
            <a href="<?= site_url('configuracio') ?>" class="btn btn-outline-primary"><i class="fa fa-cogs"></i> Configuració </a>
			<a href="<?= site_url('alineacions/territorial') ?>" class="btn btn-outline-primary"><i class="fa fa-th"></i> Alineacions </a>
			<a href="<?= site_url('consultes') ?>" class="btn btn-primary"><i class="fa fa-search"></i> Consultes </a>
        </div>
        <br><br>
        <div class="btn-group">
            <a href="<?= site_url('consultes/') ?>" class="btn btn-primary"><i class="fa fa-user"></i> Consultar Jugadors </a>
            <a href="<?= site_url('consultes/jornades') ?>" class="btn btn-outline-primary"><i class="fa fa-calendar"></i> Consultar Jornades </a>
        </div>
        <input id='siteUrl' type='text' value= "<?= site_url('consultes/jugador/') ?>" hidden />
        <br><br>
        <input class="form-control form-control-lg mr-sm-2" style="width:50%;" type="text" placeholder="Nom del jugador a consultar..." aria-label="Search" id="buscar-jugador" name="player">
        <br>
        <div class="buscar-jugador-resultat list-group"></div>
        <br>
        <?php if(isset($jugador)): ?>
        <div class="jugador-info">
        	<h3> <?= $jugador['nom']." ".$jugador['cognom']." (".$jugador['llicencia'].")" ?> </h3>
        	<?php if(isset($encadenat)): ?>
        	<div class="cadena-info">
        		<h5 class= "consulta-cadena"> Encadenat amb: <?= $encadenat['nom']." ".$encadenat['cognom']." (".$encadenat['llicencia'].")" ?> </h5>
        		<i onclick="genUnlinkModal('<?=$jugador['llicencia']?>')" class="fa fa-unlink consulta-cadena icon-cadena action-icon" data-toggle="modal" data-target="#unlink"></i>
        	</div>
        	<br>
        	<?php endif; ?>
        	<hr>
        		<?php if(count($partits_jugats) > 0): ?>
        	    <h5>Partits Jugats</h5>
        	    <table class="table">
					<tbody>
					<?php foreach($partits_jugats as $partit_jugat): ?>
						<tr class="border">
							<td> <b> <?= $partit_jugat['contrincant'] ?> </b> </td>
							<td> <?= date("d-m-Y H:i",strtotime($partit_jugat['data'])) ?> </td>
							<td>
								<?php if($partit_jugat['resultat_equip'] > $partit_jugat['resultat_contrincant']): ?> <span class="badge badge-success badge-pill">Victoria</span>
								<?php else: ?> <span class="badge badge-danger badge-pill">Derrota</span>
								<?php endif; ?>
							</td>
						</tr>
					 <?php endforeach; ?>
					</tbody>
            	</table>
			<br>
			<?php endif; ?>
			<?php if(count($partits_alineats) > 0): ?>
				<h5>Próxims Partits</h5>
				<table class="table">
                	<tbody>
					<?php foreach($partits_alineats as $partit_alineat): ?>
						<tr class="border">
							<td> <b> <?= $partit_alineat['contrincant'] ?> </b> </td>
							<td> <?= date("d-m-Y H:i",strtotime($partit_alineat['data'])) ?> </td>
							<td>
								<?php if($partit_alineat['casa']): ?> <span class="badge badge-primary badge-pill">Local</span>
								<?php else: ?> <span class="badge badge-primary badge-pill">Visitant</span>
								<?php endif; ?>
							</td>
						</tr>
					 <?php endforeach; ?>
					</tbody>
				</table>
			<br>
			<?php endif; ?>
			<?php if(count($dates_noalineables) > 0): ?>
        	<h5> Dates on no pot competir </h5>
        	<table class="table">
				<tbody>
				<?php foreach($dates_noalineables as $data_noalineable): ?>
					<tr class="border">
						<td> <b> <?= date("d-m-Y",strtotime($data_noalineable['data'])) ?> </b> </td>
						<td> <i onclick="genDeleteModal('<?=$data_noalineable['id']?>','<?=$jugador['llicencia']?>')" class='action-icon fa fa-trash' data-toggle="modal" data-target="#deleteData" ></i> </td>
					</tr>
				 <?php endforeach; ?>
				</tbody>
			</table>
			<br>
			<?php endif; ?>
			<?php if(count($partits_declinats) > 0): ?>
			<h5> Dates on ha declinat la convocatoria </h5>
				<table class="table">
					<tbody>
					<?php foreach($partits_declinats as $partit_declinat): ?>
						<tr class="border">
							<td> <b> <?= $partit_declinat['contrincant'] ?> </b> </td>
							<td> <?= date("d-m-Y",strtotime($partit_declinat['data'])) ?> </td>
							<td>
								<?php if($partit_declinat['casa']): ?> <span class="badge badge-primary badge-pill">Local</span>
								<?php else: ?> <span class="badge badge-primary badge-pill">Visitant</span>
								<?php endif; ?>
							</td>
							<td> <i onclick="genDeleteModal('<?=$partit_declinat['id']?>','<?=$jugador['llicencia']?>')" class='action-icon fa fa-trash' data-toggle="modal" data-target="#deleteData" ></i> </td>
						</tr>
					 <?php endforeach; ?>
					</tbody>
				</table>
			<?php endif; ?>
				</div>
        	</div>
        <?php endif; ?>
</main>

<form action="<?= site_url("consultes/unlinkPlayers") ?>" method="post">
    <div class="modal fade" id="unlink" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Desencadenar Jugadors</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                	<div class="hidden-values"></div>
                    <div class="alert alert-info">
                        <p>Estàs segur que desitges desencadenar els jugadors? </p>
                    </div>
                	<div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Desencadenar</button>
                	</div>
           		 </div>
        	</div>
    	</div>
    </div>
</form>

<form action="<?= site_url("consultes/deleteData") ?>" method="post">
    <div class="modal fade" id="deleteData" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                	<div class="hidden-values"></div>
                    <div class="alert alert-danger">
                        <p>Estàs segur que desitges esborrar la data? </p>
                    </div>
                	<div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Eliminar</button>
                	</div>
           		 </div>
        	</div>
    	</div>
    </div>
</form>


<script>

	function genDeleteModal(idData,idJugador) {
		$(".hidden-values").empty();
		$(".hidden-values").append("<input id='idData' name='idData' value='"+idData+"' hidden />");
		$(".hidden-values").append("<input id='idJugador' name='idJugador' value='"+idJugador+"' hidden />");
	}

	function genUnlinkModal(idJugador) {
		$(".hidden-values").empty();
		$(".hidden-values").append("<input id='idJugador' name='idJugador' value='"+idJugador+"' hidden />");
	}

$(document).ready(function () {	
	$("#buscar-jugador").on("keyup", function(e){
		$(".jugador-info").empty();
		$(".buscar-jugador-resultat").empty();
		var q = $("#buscar-jugador").val();
		if(q != "") {
			$.ajax({url: "<?= site_url("consultes/buscarJugador/") ?>"+q, success: function(result){

				var siteUrl = $('#siteUrl').val();
				var data = JSON.parse(result);

				for(var i=0;i<data.length;++i) {
					$(".buscar-jugador-resultat").append("<div class='list-group-item'><a href='"+siteUrl+data[i].llicencia+"'>"+data[i].nom+" "+data[i].cognom+"</a></div>");
				}
        	}})
		}
	})

 });
</script>
</body>


</html>
