<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>

    <link href="<?= base_url("assets/css/datatables.min.css"); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/css/rpg-awesome.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet" />
	<script src="<?= base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
</head>
<body>


<main>

    <div class="container">
		<div class="btn-group">
            <a href="<?= site_url('configuracio') ?>" class="btn btn-primary"><i class="fa fa-cogs"></i> Configuració </a>
			<a href="<?= site_url('alineacions/territorial') ?>" class="btn btn-outline-primary"><i class="fa fa-th"></i> Alineacions </a>
			<a href="<?= site_url('consultes') ?>" class="btn btn-outline-primary"><i class="fa fa-search"></i> Consultes </a>
        </div>
        <br><br>
        <div class="btn-group">
            <a href="<?= site_url('configuracio') ?>" class="btn btn-outline-primary"><i class="fa fa-hourglass"></i> Temporades </a>
            <a href="<?= site_url('configuracio/locals') ?>" class="btn btn-outline-primary"><i class="fa fa-building"></i> Locals </a>
            <a href="<?= site_url('configuracio/equips') ?>" class="btn btn-outline-primary"><i class="fa fa-users"></i> Equips </a>
			<a href="<?= site_url('configuracio/contrincants') ?>" class="btn btn-primary"><i class='ra ra-crossed-swords'></i>Contrincants </a>
			<a href="<?= site_url('configuracio/jornades') ?>"class="btn btn-outline-primary"><i class="fa fa-calendar"></i> Jornades </a>
            <a href="<?= site_url('configuracio/jugadors') ?>" class="btn btn-outline-primary"><i class="fa fa-address-card"></i> Jugadors </a>
        </div>
        <br><br>
        <div class="btn-group">
            <button class="btn btn-outline-primary" data-toggle="modal" data-target="#addContrincant"><i class="fa fa-plus-circle"></i> Afegir Contrincants </button>
        </div>
        <br><br>
		<?php if(isset($errorDelete)):?>
			<?php foreach($errorDelete as $partit):?>
				<?php  error("Error al esborrar: Hi ha una jornada contra aquest contrincant el dia ".date('d-m-Y H:i',strtotime($partit['data']))); ?>
			<?php endforeach; ?>
		<?php endif; ?>
        <div class="table-responsive">
            <table class="table table-striped table-bordered datatable">
                <thead>
                    <tr>
                        <th>Equip</th>
                        <th>Ciutat </th>
                        <th>Local </th>
                        <th>Carrer</th>
                        <th>Pilota</th>
                        <th>Taula</th>
                        <th>Equipació</th>
                        <th>Accions</th>
                    </tr>
                </thead>
                <tbody>
                 <?php foreach ($contrincants as $contrincant): ?>
                    <tr>
                    	<td> <?= $contrincant['contrincant']?> </td>
                    	<td> <?= $contrincant['ciutat'].' ('.$contrincant['codi_postal'].')'?> </td>
                    	<td> <?= $contrincant['local']?> </td>
                    	<td> <?= $contrincant['carrer']?> </td>
                    	<td> <?= $contrincant['pilota']?> </td>
                    	<td> <?= $contrincant['taula']?> </td>
                    	<td> <?= $contrincant['equipacio']?> </td>
                    	<td>
							<i onclick="genEditModal('<?=$contrincant['id']?>')" class='action-icon fa fa-pencil' data-toggle="modal" data-target="#editContrincant" ></i>
							<i onclick="genDeleteModal('<?=$contrincant['id']?>')"class='action-icon fa fa-trash' data-toggle="modal" data-target="#deleteContrincant" ></i>
						</td>
                    </tr>
                 <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <br>
    </div>
	<br>
</main>

<form action="<?= site_url("configuracio/addContrincant") ?>" method="post">
    <div class="modal fade" id="addContrincant" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Afegir Contrincant</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-primary">
                        <p>Selecciona el nombre de contrincants a afegir</p>
                    </div>
					<div class="row">
						<div class="form-group col-sm-12 col-md-12">
							<label for="numJugadors">Número de contrincants</label>
							<input type="number" class="form-control " id="numContrincants" name="numContrincants" min=1 required>
						</div>
					</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success">Afegir</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form action="<?= site_url("configuracio/editContrincant") ?>" method="post">
    <div class="modal fade" id="editContrincant" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Editar Contrincant</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                	<div class="hidden-values"></div>
                	<div class="row">
                		<div class="form-group col-sm-12 col-md-4 edit-contrincant"></div>
                		<div class="form-group col-sm-12 col-md-4 edit-ciutat"></div>
                		<div class="form-group col-sm-12 col-md-4 edit-cp"></div>
                	</div>
						<div class="row">
							<div class="form-group col-sm-12 col-md-6 edit-local"></div>
							<div class="form-group col-sm-12 col-md-6 edit-carrer"></div>
						</div>
                	<div class="row">
						<div class="form-group col-sm-12 col-md-4 edit-pilota"></div>
						<div class="form-group col-sm-12 col-md-4 edit-taula"></div>
						<div class="form-group col-sm-12 col-md-4 edit-equipacio"></div>
					</div>
                	<div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Editar</button>
                	</div>
           		 </div>
        	</div>
    	</div>
    </div>
</form>

<form action="<?= site_url("configuracio/deleteContrincant") ?>" method="post">
    <div class="modal fade" id="deleteContrincant" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar Contrincant</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                	<div class="hidden-values"></div>
                    <div class="alert alert-danger">
                        <p>Estàs segur que desitges esborrar el contrincant? </p> <p> Per a asegurar el comportament de l'aplicació, no es permetra esborrar contrincants asignats a alguna jornada</p>
                    </div>
                	<div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Eliminar</button>
                	</div>
           		 </div>
        	</div>
    	</div>
    </div>
</form>

<script src="<?php echo base_url("assets/js/datatables.min.js"); ?>"></script>

<script>

function genEditModal(idContrincant) {
		$.ajax({url: "<?= site_url("configuracio/getContrincantToEdit/") ?>"+idContrincant, success: function(result){

			var data = JSON.parse(result);

			var taula = "";
			var equipacio = "";

			if(data.taula !== null) taula = data.taula;
			if(data.equipacio !== null) equipacio = data.equipacio;

			$(".hidden-values").empty();
			$(".hidden-values").append("<input id='idContrincant' name='idContrincant' value='"+idContrincant+"' hidden />");

			$(".edit-contrincant").empty();
			$(".edit-contrincant").append("<label for='econtrincant'>Contrincant</label>");
			$(".edit-contrincant").append("<input type='text' class='form-control' id='econtrincant' name='econtrincant' value='"+data.contrincant+"' maxlength='25' required>");

			$(".edit-ciutat").empty();
			$(".edit-ciutat").append("<label for='eciutat'>Ciutat</label>");
			$(".edit-ciutat").append("<input type='text' class='form-control' id='eciutat' name='eciutat' value='"+data.ciutat+"' maxlength='35' required>");

			$(".edit-cp").empty();
			$(".edit-cp").append("<label for='ecp'>Codi Postal</label>");
			$(".edit-cp").append("<input type='text' class='form-control' id='ecp' name='ecp' value='"+data.codi_postal+"' maxlength='5' required>");

			$(".edit-local").empty();
			$(".edit-local").append("<label for='elocal'>Local</label>");
			$(".edit-local").append("<input type='text' class='form-control' id='elocal' name='elocal' value='"+data.local+"' maxlength='40' required>");

			$(".edit-carrer").empty();
			$(".edit-carrer").append("<label for='ecarrer'>Carrer</label>");
			$(".edit-carrer").append("<input type='text' class='form-control' id='ecarrer' name='ecarrer' value='"+data.carrer+"' maxlength='30' required>");

			$(".edit-pilota").empty();
			$(".edit-pilota").append("<label for='epilota'>Pilota</label>");
			$(".edit-pilota").append("<input type='text' class='form-control' id='epilota' name='epilota' value='"+data.pilota+"' maxlength='15' required>");

			$(".edit-taula").empty();
			$(".edit-taula").append("<label for='etaula'>Taula</label>");
			$(".edit-taula").append("<input type='text' class='form-control' id='etaula' name='etaula' value='"+taula+"' maxlength='15' >");

			$(".edit-equipacio").empty();
			$(".edit-equipacio").append("<label for='eequipacio'>Equipació</label>");
			$(".edit-equipacio").append("<input type='text' class='form-control' id='eequipacio' name='eequipacio' value='"+equipacio+"' maxlength='10' >");

		}})
	}

	function genDeleteModal(idContrincant) {
    	$(".hidden-values").empty();
    	$(".hidden-values").append("<input id='idContrincant' name='idContrincant' value='"+idContrincant+"' hidden />");
    }

$(document).ready(function () {	
        table = $('.datatable').DataTable({
            order: [[ 0, "desc" ]],
            language: {
                "sProcessing":     "Processant...",
                "sLengthMenu":     "Mostrar _MENU_ contrincants",
                "sZeroRecords":    "No s'han trobat contrincants",
                "sEmptyTable":     "Cap contrincant disponible",
                "sInfo":           "Mostrant de _START_ a _END_ de _TOTAL_ contrincants",
                "sInfoEmpty":      "Mostrant del 0 a 0 de 0 contrincants",
                "sInfoFiltered":   "(filtrades d'un total de _MAX_ contrincants)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Carregant...",
                "oPaginate": {
                    "sFirst":    "«",
                    "sLast":     "»",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "oAria": {
                    "sSortAscending":  ": Activar per a ordenar la columna de manera ascendent",
                    "sSortDescending": ": Activar per a ordenar la columna de manera descendent"
                },
			}
        })
 });
</script>
</body>


</html>
