<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>

    <link href="<?= base_url("assets/css/datatables.min.css"); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/rpg-awesome.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet" />
	<script src="<?= base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
</head>
<body>


<main>

    <div class="container">
		<div class="btn-group">
            <a href="<?= site_url('configuracio') ?>" class="btn btn-primary"><i class="fa fa-cogs"></i> Configuració </a>
			<a href="<?= site_url('alineacions/territorial') ?>" class="btn btn-outline-primary"><i class="fa fa-th"></i> Alineacions </a>
			<a href="<?= site_url('consultes') ?>" class="btn btn-outline-primary"><i class="fa fa-search"></i> Consultes </a>
        </div>
        <br><br>
        <div class="btn-group">
            <a href="<?= site_url('configuracio') ?>" class="btn btn-outline-primary"><i class="fa fa-hourglass"></i> Temporades </a>
            <a href="<?= site_url('configuracio/locals') ?>" class="btn btn-outline-primary"><i class="fa fa-building"></i> Locals </a>
            <a href="<?= site_url('configuracio/equips') ?>" class="btn btn-primary"><i class="fa fa-users"></i> Equips </a>
			<a href="<?= site_url('configuracio/contrincants') ?>" class="btn btn-outline-primary"><i class='ra ra-crossed-swords'></i>Contrincants </a> 
			<a href="<?= site_url('configuracio/jornades') ?>"class="btn btn-outline-primary"><i class="fa fa-calendar"></i> Jornades </a>
            <a href="<?= site_url('configuracio/jugadors') ?>" class="btn btn-outline-primary"><i class="fa fa-address-card"></i> Jugadors </a>
        </div>
        <br><br>
        <div class="btn-group">
            <button onclick="initJerarquia(1)" class="btn btn-outline-primary" data-toggle="modal" data-target="#addTeam"><i class="fa fa-plus-circle"></i> Afegir Equip </button>
            <button class="btn btn-outline-primary" data-toggle="modal" data-target="#showOrdre"><i class="fa fa-sort-numeric-asc"></i> Consultar Ordre </button>
        </div>
        <br><br>
        <?php if(isset($errorDelete)): error("Error al esborrar: ".$errorDelete." jugadors formen part de l'equip");?>
        <?php endif;?>
        <div class="table-responsive">
            <table class="table table-striped table-bordered datatable">
                <thead>
                    <tr>
                        <th>Equip</th>
                        <th>Categoria </th>
                        <th>Local </th>
                        <th>Temporada </th>
                        <th>Nombre Jornades</th>
                        <th>Nombre Jugadors</th>
                        <th>Accions</th>
                    </tr>
                </thead>
                <tbody>
                 <?php foreach ($equips as $equip): ?>
                    <tr>
                    	<td> <?= $equip['equip']?> </td>
                    	<td> <?= $equip['categoria']?> </td>
                    	<td> <?= $equip['local']?> </td>
                    	<td> <?= $equip['temporada']?> </td>
                    	<td> <?= $equip['numJornades']?> </td>
                    	<td> <?= $equip['numJugadors']?> </td>
                    	<td>
                    		<i onclick="genEditModal('<?=$equip['id']?>')" class='action-icon fa fa-pencil' data-toggle="modal" data-target="#editTeam" ></i>
                            <i onclick="genDeleteModal('<?=$equip['id']?>')" class='action-icon fa fa-trash' data-toggle="modal" data-target="#deletePlayer" ></i>
                        </td>
                    </tr>
                 <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <br>
    </div>
	<br>
</main>

<form action="<?= site_url("configuracio/addTeam") ?>" method="post">
    <div class="modal fade" id="addTeam" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Afegir Equip</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-primary">
                        <p>Escriu el nom de l'equip (pot ser la lliga on juga) i escolleix la temporada i categoria adient</p>
                    </div>
					<div class="row">
						<div class="form-group col-sm-12 col-md-12">
							<label for="nomEquip">Nom Equip</label>
							<input type="text" class="form-control " id="nomEquip" name="nomEquip" required>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-sm-12 col-md-6">
							<label for="temporada">Temporada</label>
							<select class="form-control " id="temporada" name="temporada" required>
								<?php
								foreach ($temporades as $temporada):
									echo '<option value="' . $temporada['id'] . '">' . $temporada['temporada'] . '</option>';
								endforeach;
								?>
							</select>
						</div>
						<div class="form-group col-sm-12 col-md-6">
							<label for="categoria">Categoria</label>
							<select class="form-control " id="categoria" name="categoria" required>
								<?php
								foreach ($categories as $categoria):
									echo '<option value="' . $categoria['id'] . '">' . $categoria['categoria'] . '</option>';
								endforeach;
								?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-sm-12 col-md-12">
							<label for="categoria">Local</label>
							<select class="form-control " id="local" name="local" required>
								<?php
								foreach ($locals as $local):
									echo '<option value="' . $local['id'] . '">' . $local['local'] . '</option>';
								endforeach;
								?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-sm-12 col-md-6 ordre-jerarquia"></div>
						<div class="form-group col-sm-12 col-md-6 equip-jerarquia"></div>
					</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success">Afegir</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form action="<?= site_url("configuracio/editTeam") ?>" method="post">
    <div class="modal fade" id="editTeam" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Editar Equip</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                	<div class="hidden-values"></div>
                	<div class="row">
                		<div class="form-group col-sm-12 col-md-4 edit-equip"></div>
						<div class="form-group col-sm-12 col-md-4">
							<label for="temporada">Temporada</label>
							<select class="form-control setemporada" id="etemporada" name="etemporada" required>
								<?php
									foreach ($temporades as $temporada):
										echo '<option value="' . $temporada['id'] . '">' . $temporada['temporada'] . '</option>';
									endforeach;
								?>
							</select>
						</div>
						<div class="form-group col-sm-12 col-md-4">
							<label for="categoria">Categoria</label>
							<select class="form-control secategoria" id="ecategoria" name="ecategoria" required>
								<?php
									foreach ($categories as $categoria):
										echo '<option value="' . $categoria['id'] . '">' . $categoria['categoria'] . '</option>';
									endforeach;
								?>
							</select>
						</div>
                	</div>
                	<div class="row">
						<div class="form-group col-sm-12 col-md-12">
							<label for="categoria">Local</label>
							<select class="form-control " id="elocal" name="elocal" required>
								<?php
								foreach ($locals as $local):
									echo '<option value="' . $local['id'] . '">' . $local['local'] . '</option>';
								endforeach;
								?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-sm-12 col-md-6 edit-ordre-jerarquia"></div>
						<div class="form-group col-sm-12 col-md-6 edit-equip-jerarquia"></div>
					</div>
                	<div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Editar</button>
                	</div>
           		 </div>
        	</div>
    	</div>
    </div>
</form>

<form action="<?= site_url("configuracio/deleteTeam") ?>" method="post">
    <div class="modal fade" id="deletePlayer" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar Equip</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                	<div class="hidden-values"></div>
                    <div class="alert alert-danger">
                        <p>Estàs segur que desitges esborrar l'equip? </p> <p> Per a asegurar el comportament de l'aplicació, no es permetra esborrar equips que tinguin jugadors.</p>
                    </div>
                	<div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Eliminar</button>
                	</div>
           		 </div>
        	</div>
    	</div>
    </div>
</form>

<div class="modal fade" id="showOrdre" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Ordre Jeràrquic</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<h5> <b> Territorial </b> </h5>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Equip</th>
								<th>Nivell Jerarquic </th>
							</tr>
						</thead>
						<tbody>
						 <?php foreach ($equips as $equip): ?>
						 	<?php if($equip['id_categoria']==1): ?>
							<tr>
								<td> <?= $equip['equip']?> </td>
								<td> <?= $equip['jerarquia']?> </td>
							</tr>
							<?php endif; ?>
						 <?php endforeach;?>
						</tbody>
					</table>
				</div>
				<h5> <b> Nacional </b> </h5>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Equip</th>
								<th>Nivell Jerarquic</th>
							</tr>
						</thead>
						<tbody>
						 <?php foreach ($equips as $equip): ?>
						 	<?php if($equip['id_categoria']==2): ?>
							<tr>
								<td> <?= $equip['equip']?> </td>
								<td> <?= $equip['jerarquia']?> </td>
							</tr>
							<?php endif; ?>
						 <?php endforeach;?>
						</tbody>
					</table>
				</div>
				<h5> <b> Veterans </b> </h5>
				<div class="table-responsive">
					<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Equip</th>
								<th>Nivell Jerarquic</th>
							</tr>
						</thead>
						<tbody>
						 <?php foreach ($equips as $equip): ?>
						 	<?php if($equip['id_categoria']==3): ?>
							<tr>
								<td> <?= $equip['equip']?> </td>
								<td> <?= $equip['jerarquia']?> </td>
							</tr>
							<?php endif; ?>
						 <?php endforeach;?>
						</tbody>
					</table>
				</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Sortir</button>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url("assets/js/datatables.min.js"); ?>"></script>

<script>

function initJerarquia(idCategoria) {
	var idTemporada = $('#temporada').val();
	$.ajax({url: "<?= site_url("configuracio/getTeamsFromCategoria/") ?>"+idCategoria+"/"+idTemporada, success: function(result){

		$(".ordre-jerarquia").empty();
		$(".equip-jerarquia").empty();

		var data = JSON.parse(result);

		if(data.length > 0) {
			$(".ordre-jerarquia").append("<label for='jerarquia'>Ordre Jerarquic</label>");
			$(".ordre-jerarquia").append("<select class='form-control' id='jerarquia' name='jerarquia' required>");
			$("#jerarquia").append("<option value='-1'> Per sota de </option>");
			$("#jerarquia").append("<option value='0'> Al mateix nivell de </option>");
			$("#jerarquia").append("<option value='1'> Per sobre de </option>");

			$(".equip-jerarquia").append("<label for='equip-referencia'>Equip de Referencia</label>");
			$(".equip-jerarquia").append("<select class='form-control' id='equip-referencia' name='equip-referencia' required>");
			for(var i=0;i<data.length;i++) {
				$("#equip-referencia").append("<option value='"+data[i]['jerarquia']+"'>"+data[i]['equip']+"</option>");
			}
		}
	}})
}

function initEditJerarquia(idCategoria,idTemporada) {
	$.ajax({url: "<?= site_url("configuracio/getTeamsFromCategoria/") ?>"+idCategoria+"/"+idTemporada, success: function(result){

		$(".edit-ordre-jerarquia").empty();
		$(".edit-equip-jerarquia").empty();

		var data = JSON.parse(result);
		if(data.length > 0) {
			$(".edit-ordre-jerarquia").append("<label for='edit-jerarquia'>Ordre Jerarquic</label>");
			$(".edit-ordre-jerarquia").append("<select class='form-control' id='edit-jerarquia' name='edit-jerarquia' required>");
			$("#edit-jerarquia").append("<option value='-1'> Per sota de </option>");
			$("#edit-jerarquia").append("<option value='0'> Al mateix nivell de </option>");
			$("#edit-jerarquia").append("<option value='1'> Per sobre de </option>");

			$(".edit-equip-jerarquia").append("<label for='edit-equip-referencia'>Equip de Referencia</label>");
			$(".edit-equip-jerarquia").append("<select class='form-control' id='edit-equip-referencia' name='edit-equip-referencia' required>");
			for(var i=0;i<data.length;i++) {
				$("#edit-equip-referencia").append("<option value='"+data[i]['jerarquia']+"'>"+data[i]['equip']+"</option>");
			}
		}
	}})
}

function genEditModal(idEquip) {
		$.ajax({url: "<?= site_url("configuracio/getTeamToEdit/") ?>"+idEquip, success: function(result){

			var data = JSON.parse(result);

			initEditJerarquia(data.id_categoria,data.id_temporada);

			$(".hidden-values").empty();
			$(".hidden-values").append("<input id='idEquip' name='idEquip' value='"+idEquip+"' hidden />");

			$(".edit-equip").empty();
			$(".edit-equip").append("<label for='eequip'>Nom</label>");
			$(".edit-equip").append("<input type='text' class='form-control' id='eequip' name='eequip' value='"+data.equip+"' maxlength='20' required>");

			$(".setemporada").val(data.id_temporada).change();
            $(".secategoria").val(data.id_categoria).change();

		}})
	}

function genDeleteModal(idEquip) {
	$(".hidden-values").empty();
	$(".hidden-values").append("<input id='idEquip' name='idEquip' value='"+idEquip+"' hidden />");
}

$(document).ready(function () {	
        table = $('.datatable').DataTable({
            order: [[ 0, "desc" ]],
            language: {
                "sProcessing":     "Processant...",
                "sLengthMenu":     "Mostrar _MENU_ equips",
                "sZeroRecords":    "No s'han trobat equips",
                "sEmptyTable":     "Cap equip disponible",
                "sInfo":           "Mostrant de _START_ a _END_ de _TOTAL_ equips",
                "sInfoEmpty":      "Mostrant del 0 a 0 de 0 equips",
                "sInfoFiltered":   "(filtrades d'un total de _MAX_ equips)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Carregant...",
                "oPaginate": {
                    "sFirst":    "«",
                    "sLast":     "»",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "oAria": {
                    "sSortAscending":  ": Activar per a ordenar la columna de manera ascendent",
                    "sSortDescending": ": Activar per a ordenar la columna de manera descendent"
                },
			}
        })

        $('#categoria').change(function() {
        	initJerarquia(this.value);
        })

		$('#temporada').change(function() {
			initJerarquia($('#categoria').val());
		})
 });
</script>
</body>


</html>
