<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>

    <link href="<?= base_url("assets/css/datatables.min.css"); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/css/rpg-awesome.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/datepicker.min.css'); ?>" rel="stylesheet" />
	<script src="<?= base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/datepicker.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/i18n/datepicker.es.js'); ?>"></script>
</head>
<body>


<main>

    <div class="container">
		<div class="btn-group">
            <a href="<?= site_url('configuracio') ?>" class="btn btn-primary"><i class="fa fa-cogs"></i> Configuració </a>
			<a href="<?= site_url('alineacions/territorial') ?>" class="btn btn-outline-primary"><i class="fa fa-th"></i> Alineacions </a>
			<a href="<?= site_url('consultes') ?>" class="btn btn-outline-primary"><i class="fa fa-search"></i> Consultes </a>
        </div>
        <br><br>
        <div class="btn-group">
            <a href="<?= site_url('configuracio') ?>" class="btn btn-outline-primary"><i class="fa fa-hourglass"></i> Temporades </a>
            <a href="<?= site_url('configuracio/locals') ?>" class="btn btn-outline-primary"><i class="fa fa-building"></i> Locals </a>
            <a href="<?= site_url('configuracio/equips') ?>" class="btn btn-outline-primary"><i class="fa fa-users"></i> Equips </a>
			<a href="<?= site_url('configuracio/contrincants') ?>" class="btn btn-outline-primary"><i class='ra ra-crossed-swords'></i>Contrincants </a>
			<a href="<?= site_url('configuracio/jornades') ?>"class="btn btn-primary"><i class="fa fa-calendar"></i> Jornades </a>
            <a href="<?= site_url('configuracio/jugadors') ?>" class="btn btn-outline-primary"><i class="fa fa-address-card"></i> Jugadors </a>
        </div>
        <br><br>
        <div class="btn-group">
            <button class="btn btn-outline-primary" data-toggle="modal" data-target="#addJornades"><i class="fa fa-plus-circle"></i> Afegir Jornades </button>
        </div>
        <br><br>
		<?php if(isset($errorDelete)): error("Error al esborrar: La jornada te jugadors alineats");?>
		<?php endif;?>
        <div class="table-responsive">
            <table class="table table-striped table-bordered datatable">
                <thead>
                    <tr>
                        <th>Jornada</th>
                        <th>Equip</th>
                        <th>Data</th>
                        <th>Contrincant</th>
						<th>Casa</th>
						<th>Accions</th>
                    </tr>
                </thead>
                <tbody>
                	<?php foreach($jornades as $jornada):?>
                        <tr>
                        	<td> <?= 'Jornada '.$jornada['jornada']?> </td>
                        	<td> <?= $jornada['equip'].' '.$jornada['categoria'].' ('.$jornada['temporada'].')'?> </td>
                        	<td> <?= date('d-m-Y H:i',strtotime($jornada['data']))?> </td>
                        	<td> <?= $jornada['contrincant']?> </td>
                        	<td> <input type="checkbox" <?php if($jornada['casa']): echo "checked"; else: echo "unchecked"; endif;?> disabled> </td>
                        	<td>
								<i onclick="genEditModal('<?=$jornada['id']?>')" class='action-icon fa fa-pencil' data-toggle="modal" data-target="#editJornada" ></i>
								<i onclick="genDeleteModal('<?=$jornada['id']?>')" class='action-icon fa fa-trash' data-toggle="modal" data-target="#deleteJornada" ></i>
							</td>
                        </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <br>
    </div>
	<br>
</main>

<form action="<?= site_url("configuracio/addJornades") ?>" method="post">
    <div class="modal fade" id="addJornades" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Afegir Jornades</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-primary">
                        <p>Selecciona l'equip i el número de jornades a afegir</p>
                    </div>
					<div class="row">
						<div class="form-group col-sm-12 col-md-6">
							<label for="nomTemporada">Equip</label>
							<select class="form-control" id='equip' name='equip'>
								<?php
									foreach ($equips as $equip):	
										echo '<option value="'.$equip['id'].'">'.$equip['equip'].' '.$equip['categoria'].'</option>';
									endforeach;
								?>
                            </select>
						</div>
						<div class="form-group col-sm-12 col-md-6">
							<label for="numJugadors">Número de jornades</label>
							<input type="number" class="form-control" id="numJornades" name="numJornades" min=1 required>
						</div>
					</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success">Afegir</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form action="<?= site_url("configuracio/editJornada") ?>" method="post">
    <div class="modal fade" id="editJornada" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Editar Jornada</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                	<div class="hidden-values"></div>
                	<div class="row">
                		<div class="form-group col-sm-12 col-md-3 edit-jornada"></div>
						<div class="form-group col-sm-12 col-md-6 edit-contrincant">
							<label for="temporada">Contrincant</label>
							<select class="form-control secontrincant" id="econtrincant" name="econtrincant" required>
								<?php
									foreach ($contrincants as $contrincant):
										echo '<option value="' . $contrincant['id'] . '">' . $contrincant['contrincant'] . '</option>';
									endforeach;
								?>
							</select>
						</div>
						<div class="form-group col-sm-12 col-md-3 edit-casa"></div>
                	</div>
                	<div class="row">
                		<div class="form-group col-sm-12 col-md-12">
							<input data-timepicker="true" data-language='es' class='datepicker-here form-control' id='edatepicker' name='edatepicker'/>
						</div>
                	</div>
                	<div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Editar</button>
                	</div>
           		 </div>
        	</div>
    	</div>
    </div>
</form>

<form action="<?= site_url("configuracio/deleteJornada") ?>" method="post">
    <div class="modal fade" id="deleteJornada" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar Equip</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                	<div class="hidden-values"></div>
                    <div class="alert alert-danger">
                        <p>Estàs segur que desitges esborrar la jornada? </p> <p> Per a asegurar el comportament de l'aplicació, no es permetra esborrar jornades que tinguin jugadors alineats.</p>
                    </div>
                	<div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Eliminar</button>
                	</div>
           		 </div>
        	</div>
    	</div>
    </div>
</form>

<script src="<?php echo base_url("assets/js/datatables.min.js"); ?>"></script>

<script>

	function genEditModal(idJornada) {
		$.ajax({url: "<?= site_url("configuracio/getJornadaToEdit/") ?>"+idJornada, success: function(result){

			var data = JSON.parse(result);

			var casaCheck = "";
			if(data.casa == 1) casaCheck= "checked";

			$(".hidden-values").empty();
			$(".hidden-values").append("<input id='idJornada' name='idJornada' value='"+idJornada+"' hidden />");

			$("#edatepicker").val(data.data);

			$(".secontrincant").val(data.id_contrincant).change();

			$(".edit-jornada").empty();
			$(".edit-jornada").append("<label for='ejornada'>Jornada</label>");
			$(".edit-jornada").append("<input type='number' class='form-control' id='ejornada' name='ejornada' value='"+data.jornada+"' min='1' required>");

			$(".edit-casa").empty();
			$(".edit-casa").append("<label for='ecasa'>Casa</label>");
			$(".edit-casa").append("<input type='checkbox' class='form-control' id='ecasa' name='ecasa' "+casaCheck+">");
		}})
	}

	function genDeleteModal(idJornada) {
    	$(".hidden-values").empty();
    	$(".hidden-values").append("<input id='idJornada' name='idJornada' value='"+idJornada+"' hidden />");
    }

$(document).ready(function () {	
        table = $('.datatable').DataTable({
            order: [[ 0, "asc" ]],
            language: {
                "sProcessing":     "Processant...",
                "sLengthMenu":     "Mostrar _MENU_ jornades",
                "sZeroRecords":    "No s'han trobat jornades",
                "sEmptyTable":     "Cap jornada disponible",
                "sInfo":           "Mostrant de _START_ a _END_ de _TOTAL_ jornades",
                "sInfoEmpty":      "Mostrant del 0 a 0 de 0 jornades",
                "sInfoFiltered":   "(filtrades d'un total de _MAX_ jornades)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Carregant...",
                "oPaginate": {
                    "sFirst":    "«",
                    "sLast":     "»",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "oAria": {
                    "sSortAscending":  ": Activar per a ordenar la columna de manera ascendent",
                    "sSortDescending": ": Activar per a ordenar la columna de manera descendent"
                },
			}
        })

		$('#edatepicker').datepicker({
			language: 'es',
			timeFormat: 'hh:ii',
			dateFormat: 'yyyy-mm-dd',
		});
		$('#edatepicker').keydown(function() {
        		return false;
        });
 });
</script>
</body>


</html>
