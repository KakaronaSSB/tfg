<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>

    <link href="<?= base_url("assets/css/datatables.min.css"); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/css/rpg-awesome.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/datepicker.min.css'); ?>" rel="stylesheet" />
	<script src="<?= base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/datepicker.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/i18n/datepicker.es.js'); ?>"></script>
</head>
<body>


<main>

    <div class="container">
		<div class="btn-group">
            <a href="<?= site_url('configuracio') ?>" class="btn btn-primary"><i class="fa fa-cogs"></i> Configuració </a>
			<a href="<?= site_url('alineacions/territorial') ?>" class="btn btn-outline-primary"><i class="fa fa-th"></i> Alineacions </a>
			<a href="<?= site_url('consultes') ?>" class="btn btn-outline-primary"><i class="fa fa-search"></i> Consultes </a>
        </div>
        <br><br>
        <div class="btn-group">
            <a href="<?= site_url('configuracio') ?>" class="btn btn-outline-primary"><i class="fa fa-hourglass"></i> Temporades </a>
            <a href="<?= site_url('configuracio/locals') ?>" class="btn btn-outline-primary"><i class="fa fa-building"></i> Locals </a>
            <a href="<?= site_url('configuracio/equips') ?>" class="btn btn-outline-primary"><i class="fa fa-users"></i> Equips </a>
			<a href="<?= site_url('configuracio/contrincants') ?>" class="btn btn-outline-primary"><i class='ra ra-crossed-swords'></i>Contrincants </a>
			<a href="<?= site_url('configuracio/jornades') ?>"class="btn btn-outline-primary"><i class="fa fa-calendar"></i> Jornades </a>
            <a href="<?= site_url('configuracio/jugadors') ?>" class="btn btn-primary"><i class="fa fa-address-card"></i> Jugadors </a>
        </div>
        <br><br>
        <div class="btn-group">
            <button class="btn btn-outline-primary" data-toggle="modal" data-target="#addPlayers"><i class="fa fa-plus-circle"></i> Afegir Jugadors </button>
            <button class="btn btn-outline-primary" data-toggle="modal" data-target="#linkPlayers"><i class="fa fa-link"></i> Encadenar Jugadors </button>
        </div>
        <br><br>
        <?php if(isset($error)):?>
        	<?php if($error=="mateixJugador"): error("Error al encadenar: No es poden encadenar dos jugadors iguals");?>
        	<?php elseif($error=="teCadena"): error("Error al encadenar: Un dels dos jugadors ja està encadenat amb un altre");?>
        	<?php elseif($error=="diferentEquip"): error("Error al encadenar: Els jugadors competeixen en equips diferents");?>
        	<?php endif;?>
		<?php endif;?>
        <?php if(isset($errorDelete)):?>
   			<?php foreach($errorDelete as $partit):?>
   				<?php  error("Error al esborrar: El jugador està alineat el dia ".date('d-m-Y H:i',strtotime($partit['data']))." contra ".$partit['contrincant']); ?>
   			<?php endforeach; ?>
        <?php endif; ?>
        <div class="table-responsive">
            <table class="table table-striped table-bordered datatable">
                <thead>
                    <tr>
                        <th>Llicencia</th>
                        <th>Nom</th>
						<th>Fitxa</th>
						<th>Edat </th>
                        <th>Equip</th>
                        <th>Partits Esperats </th>
						<th>Transport</th>
						<th>Respon.</th>
						<th>Promoc.</th>
						<th>Accions</th>
                    </tr>
                </thead>
                <tbody>
					<?php foreach($jugadors as $jugador): ?>
						<tr <?php if(!$jugador['habilitat']): echo "style='background-color: #e86464;'"; endif;?>>
							<td> <?= $jugador['llicencia'] ?> </td>
							<td> <?= $jugador['nom'].' '.$jugador['cognom'] ?> </td>
							<td> <?= $jugador['fitxa'] ?> </td>
							<td> <?= $jugador['edat'] ?> </td>
							<td> <?= $jugador['equip'].' '.$jugador['categoria'].' ('.$jugador['temporada'] .')'?> </td>
							<td> Entre <?= $jugador['min_partits']?> i <?= $jugador['max_partits']?>  [<?=$jugador['partits'] ?>]</td>
							<td> <input type="checkbox" <?php if($jugador['transport']): echo "checked"; else: echo "unchecked"; endif;?> disabled> </td>
							<td> <input type="checkbox" <?php if($jugador['responsable']): echo "checked"; else: echo "unchecked"; endif;?> disabled> </td>
							<td> <input type="checkbox" <?php if($jugador['promocionable']): echo "checked"; else: echo "unchecked"; endif;?> disabled> </td>
							<td>
								<i onclick="genEditModal('<?=$jugador['llicencia']?>')" class='action-icon fa fa-pencil' data-toggle="modal" data-target="#editPlayer" ></i>
								<i onclick="genJornadaModal('<?=$jugador['llicencia']?>')" class='action-icon fa fa-calendar-times-o' data-toggle="modal" data-target="#disableJornada" ></i>
								<i onclick="genDisableModal('<?=$jugador['llicencia']?>')" class='fa action-icon <?php if($jugador['habilitat']): echo 'fa-ban'; else: echo 'fa-check';endif;?>' data-toggle="modal" data-target="#disablePlayer" ></i>
                    			<i onclick="genDeleteModal('<?=$jugador['llicencia']?>')" class='action-icon fa fa-trash' data-toggle="modal" data-target="#deletePlayer" ></i>
                    		</td>
						</tr>
					<?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <br>
    </div>
	<br>
</main>

<form action="<?= site_url("configuracio/addPlayers") ?>" method="post">
    <div class="modal fade" id="addPlayers" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Afegir Jugadors</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-primary">
                        <p>Selecciona el nombre de jugadors a afegir</p>
                    </div>
					<div class="row">
						<div class="form-group col-sm-12 col-md-12">
							<label for="numJugadors">Número de jugadors</label>
							<input type="number" class="form-control " id="numJugadors" name="numJugadors" min=1 max=100 required>
						</div>
					</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success">Afegir</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form action="<?= site_url("configuracio/linkPlayers") ?>" method="post">
    <div class="modal fade" id="linkPlayers" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Encadenar Jugadors</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <?php if(count($jugadors_nocadena)> 1):?>
                    <div class="alert alert-primary">
                        <p>Selecciona els dos jugadors a encadenar</p>
                    </div>
					<div class="row">
						<div class="form-group col-sm-12 col-md-6">
							<label for="jugador1">Primer Jugador</label>
							<select class="form-control" id='jugador1' name='jugador1'>
								<?php
									foreach ($jugadors_nocadena as $jugador):	
										echo '<option value="'.$jugador['llicencia'].'">'.$jugador['nom'].' '.$jugador['cognom'].'</option>';
									endforeach;
								?>
                            </select>
						</div>
						<div class="form-group col-sm-12 col-md-6">
							<label for="jugador2">Segon Jugador</label>
							<select class="form-control" id='jugador2' name='jugador2'>
								<?php
									foreach ($jugadors_nocadena as $jugador):	
										echo '<option value="'.$jugador['llicencia'].'">'.$jugador['nom'].' '.$jugador['cognom'].'</option>';
									endforeach;
								?>
                            </select>
						</div>
					</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success">Encadenar</button>
                </div>
                <?php else:?>
				 <div class="alert alert-primary">
                 	<p>No hi ha jugadors aptes per a ser encadenats</p>
                 </div>
				<?php endif;?>
            </div>
        </div>
    </div>
</form>

<form action="<?= site_url("configuracio/deletePlayer") ?>" method="post">
    <div class="modal fade" id="deletePlayer" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar Jugador</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                	<div class="hidden-values"></div>
                    <div class="alert alert-danger">
                        <p>Estàs segur que desitges esborrar al jugador? </p> <p> Per a asegurar el comportament de l'aplicació, no es permetra esborrar jugadors que formin part d'alguna alineació.</p>
                    </div>
                	<div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Eliminar</button>
                	</div>
           		 </div>
        	</div>
    	</div>
    </div>
</form>

<form action="<?= site_url("configuracio/editPlayer") ?>" method="post">
    <div class="modal fade" id="editPlayer" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Editar Jugador</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                	<div class="hidden-values"></div>
                	<div class="row">
                		<div class="form-group col-sm-12 col-md-4 edit-llicencia"></div>
                		<div class="form-group col-sm-12 col-md-4 edit-nom"></div>
                		<div class="form-group col-sm-12 col-md-4 edit-cognom"></div>
                	</div>
						<div class="row">
							<div class="form-group col-sm-12 col-md-4 edit-equip">
								<label for='eequip'>Equip</label>
								<select class="form-control seequip" id='eequip' name='eequip'>
									<?php
										foreach ($equips as $equip):
											echo '<option value="'.$equip['id'].'">'.$equip['equip'].' '.substr($equip['categoria'],0,3).'</option>';
										endforeach;
									?>
								</select>
							</div>
							<div class="form-group col-sm-12 col-md-4 edit-fitxa">
								<label for='efitxa'>Fitxa</label>
								<select class="form-control sefitxa" id='efitxa' name='efitxa'>
									<?php
										foreach ($fitxes as $fitxa):
											echo '<option value="'.$fitxa['id'].'">'.$fitxa['fitxa'].'</option>';
										endforeach;
									?>
								</select>
							</div>
							<div class="form-group col-sm-12 col-md-4 edit-edat">
								<label for='eedat'>Edat</label>
								<select class="form-control seedat" id='eedat' name='eedat'>
									<?php
										foreach ($edats as $edat):
											echo '<option value="'.$edat['id'].'">'.$edat['edat'].'</option>';
										endforeach;
									?>
								</select>
							</div>
						</div>
                	<div class="row">
						<div class="form-group col-sm-12 col-md-6 edit-minPartits"></div>
						<div class="form-group col-sm-12 col-md-6 edit-maxPartits"></div>
					</div>
					<div class ="row">
						<div class="form-group col-sm-12 col-md-12 edit-email"></div>
					</div>
					<div class="row">
						<div class="form-group col-sm-12 col-md-4 edit-transport"></div>
						<div class="form-group col-sm-12 col-md-4 edit-responsable"></div>
						<div class="form-group col-sm-12 col-md-4 edit-promocionable"></div>
					</div>
                	<div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Editar</button>
                	</div>
           		 </div>
        	</div>
    	</div>
    </div>
</form>

<form action="<?= site_url("configuracio/disablePlayer") ?>" method="post">
    <div class="modal fade" id="disablePlayer" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header disable-header"></div>
                <div class="modal-body">
                	<div class="hidden-values"></div>
                    <div class="alert alert-info disable-alert"></div>
                    <div class="modal-footer disable-footer"></div>
            	</div>
        	</div>
    	</div>
    </div>
</form>

<form action="<?= site_url("configuracio/disableJornada") ?>" method="post">
    <div class="modal fade" id="disableJornada" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                	<h5 class="modal-title">Deshabilitar Dates</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    	<span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                <div class="hidden-values"></div>
                    <div class="alert alert-info">
                    	<p> Selecciona les dates on el jugador no pot participar </p>
                    </div>
                    <div class="row">
						<div class="form-group col-sm-12 col-md-12">
							<input data-language='es' data-multiple-dates-separator=', ' class='datepicker-here form-control' id='datepicker' name='datepicker'/>
						</div>
					</div>
					<div class="row">
						<div class="form-group col-sm-12 col-md-12 motiu-vacances"></div>
					</div>
					<div class="row">
						<div class="form-group col-sm-12 col-md-12 jornades-disable"></div>
					</div>
                    <div class="modal-footer">
                    	<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                   		<button type="submit" class="btn btn-success">Deshabilitar</button>
                    </div>
            	</div>
        	</div>
    	</div>
    </div>
</form>


<script src="<?php echo base_url("assets/js/datatables.min.js"); ?>"></script>

<script>

	function genEditModal(idJugador) {
		$.ajax({url: "<?= site_url("configuracio/getPlayerToEdit/") ?>"+idJugador, success: function(result){

			var jugador = JSON.parse(result);
			
			var transportCheck = "";
			var responsableCheck = "";
			var promocionableCheck = "";
			if(jugador.transport == 1) transportCheck = "checked";
			if(jugador.responsable == 1) responsableCheck= "checked";
			if(jugador.promocionable == 1) promocionableCheck= "checked";

			$(".hidden-values").empty();
			$(".hidden-values").append("<input id='idJugador' name='idJugador' value='"+idJugador+"' hidden />");

			$(".edit-llicencia").empty();
			$(".edit-llicencia").append("<label for='ellicencia'>Llicencia</label>");
			$(".edit-llicencia").append("<input type='text' class='form-control' id='ellicencia' name='ellicencia' value='"+jugador.llicencia+"' maxlength='6' required>");

			$(".edit-nom").empty();
			$(".edit-nom").append("<label for='enom'>Nom</label>");
			$(".edit-nom").append("<input type='text' class='form-control' id='enom' name='enom' value='"+jugador.nom+"' maxlength='15' required>");

			$(".edit-cognom").empty();
			$(".edit-cognom").append("<label for='ecognom'>Cognom</label>");
			$(".edit-cognom").append("<input type='text' class='form-control' id='ecognom' name='ecognom' value='"+jugador.cognom+"' maxlength='15' required>");

			$(".seequip").val(jugador.id_equip).change();
			$(".sefitxa").val(jugador.id_fitxa).change();
			$(".seedat").val(jugador.id_edat).change();

			$(".edit-minPartits").empty();
			$(".edit-minPartits").append("<label for='eminPartits'>Min Partits</label>");
			$(".edit-minPartits").append("<input type='number' class='form-control' id='eminPartits' name='eminPartits' min='0' value='"+jugador.min_partits+"' required>");

			$(".edit-maxPartits").empty();
			$(".edit-maxPartits").append("<label for='emaxPartits'>Max Partits</label>");
			$(".edit-maxPartits").append("<input type='number' class='form-control' id='emaxPartits' name='emaxPartits' value='"+jugador.max_partits+"'required>");

			$(".edit-email").empty();
			$(".edit-email").append("<label for='eemail'>E-Mail</label>");
			$(".edit-email").append("<input type='email' class='form-control' id='eemail' name='eemail' value='"+jugador.email+"' maxlength='50' required>");

			$(".edit-transport").empty();
			$(".edit-transport").append("<label for='etransport'>Transport</label>");
			$(".edit-transport").append("<input type='checkbox' class='form-control' id='etransport' name='etransport' "+transportCheck+">");

			$(".edit-responsable").empty();
			$(".edit-responsable").append("<label for='eresponsable'>Responsable</label>");
			$(".edit-responsable").append("<input type='checkbox' class='form-control' id='eresponsable' name='eresponsable' "+responsableCheck+">");

			$(".edit-promocionable").empty();
			$(".edit-promocionable").append("<label for='epromocionable'>Promocionable</label>");
			$(".edit-promocionable").append("<input type='checkbox' class='form-control' id='epromocionable' name='epromocionable' "+promocionableCheck+">");
		}})
	}

	function genJornadaModal(idJugador) {
		$(".hidden-values").empty();
		$('.motiu-vacances').empty();
		$('.jornades-disable').empty();
		$(".hidden-values").append("<input id='idJugador' name='idJugador' value='"+idJugador+"' hidden />");
		$("#datepicker").val("");
	}

	function genDeleteModal(idJugador) {
		$(".hidden-values").empty();
		$(".hidden-values").append("<input id='idJugador' name='idJugador' value='"+idJugador+"' hidden />");
	}

	function genDisableModal(idJugador) {
		$.ajax({url: "<?= site_url("configuracio/getPlayerToEdit/") ?>"+idJugador, success: function(result){

			var data = JSON.parse(result);

			var text = "Habilitar";
			var textAlert = "<p> Voleu tornar a habilitar aquest jugador? Podrà tornar a formar part d'alineacions </p>";

			if(data.habilitat == 1) {
				var text = "Deshabilitar";
				var textAlert = "<p>Estàs segur que desitges deshabilitar al jugador? </p> <p> No podrà formar part de futures alineacions i no pot estar present en cap alineació bloquejada.</p>";
			}
			
			$(".hidden-values").empty();
			$(".hidden-values").append("<input id='idJugador' name='idJugador' value='"+idJugador+"' hidden />");
			$(".hidden-values").append("<input id='habilitat' name='habilitat' value='"+data.habilitat+"' hidden />");

			$(".disable-alert").empty();
			$(".disable-alert").append(textAlert);
			
			$(".disable-header").empty();
			$(".disable-header").append("<h5 class='modal-title'>"+text+" Jugador</h5>");
			$(".disable-header").append("<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>");
			
			$(".disable-footer").empty();
			$(".disable-footer").append("<button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancelar</button>");
			$(".disable-footer").append("<button type='submit' class='btn btn-success'>"+text+"</button>");
		}})
	}

$(document).ready(function () {	
        table = $('.datatable').DataTable({
            order: [[ 0, "desc" ]],
            language: {
                "sProcessing":     "Processant...",
                "sLengthMenu":     "Mostrar _MENU_ jugadors",
                "sZeroRecords":    "No s'han trobat jugadors",
                "sEmptyTable":     "Cap jugador disponible",
                "sInfo":           "Mostrant de _START_ a _END_ de _TOTAL_ jugadors",
                "sInfoEmpty":      "Mostrant del 0 a 0 de 0 jugadors",
                "sInfoFiltered":   "(filtrades d'un total de _MAX_ jugadors)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Carregant...",
                "oPaginate": {
                    "sFirst":    "«",
                    "sLast":     "»",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "oAria": {
                    "sSortAscending":  ": Activar per a ordenar la columna de manera ascendent",
                    "sSortDescending": ": Activar per a ordenar la columna de manera descendent"
                },
			}
        })

        $('#datepicker').datepicker({
	 		language: 'es',
            dateFormat: 'yyyy-mm-dd',
            multipleDates: true,
 		});

		$('#datepicker').datepicker({
            onSelect: function(dateText) {
            	$('.motiu-vacances').empty();
            	$('.jornades-disable').empty();
                if(dateText != "" && dateText.indexOf(',') == -1) {
                	//Poden existir tots dos motius. Cancela previ avis i cancela un cop convocat
                	$('.motiu-vacances').append("<select class='form-control select-motiu' name='select-motiu'></select>");
                	$('.select-motiu').append("<option value='1'> Cancelació sota avís previ </option>");
                	$('.select-motiu').append("<option value='2'> Cancelació de la convocatoria </option>");
                }
                else if(dateText != "" && dateText.indexOf(',') != -1) {
                	//Això només hauria de ser cancelar sota previ avis
                	$('.motiu-vacances').append("<select class='form-control select-motiu' name='select-motiu' required></select>");
                	$('.select-motiu').append("<option value='1'> Cancelació sota avís previ </option>");
                }
            }
        });

		$('.motiu-vacances').change(function() {
			$('.jornades-disable').empty();
			var option = $('.select-motiu').val();
			var data = $('#datepicker').val();
			if(option == 2) {
				$.ajax({url: "<?=site_url("configuracio/getJornadesFromData/") ?>"+data, success: function(result){
					var jornades = JSON.parse(result);
					var numJornades = jornades.length;
					if(numJornades > 0) {
						$('.jornades-disable').append("<select class='form-control select-jornades' name='select-jornades' required></select>");
						for(var i = 0; i < numJornades; ++i) {
							$('.select-jornades').append("<option value='"+jornades[i].id+"'>"+jornades[i].categoria+" - "+jornades[i].equip+" contra "+jornades[i].contrincant+"</option>");
						}
					}
				}})
			}
		});
		$('#datepicker').keydown(function() {
			return false;
        });
 });
</script>
</body>


</html>
