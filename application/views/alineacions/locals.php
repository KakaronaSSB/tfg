<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>

    <link href="<?= base_url("assets/css/datatables.min.css"); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/css/rpg-awesome.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet" />
	<script src="<?= base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
</head>
<body>


<main>

    <div class="container">
		<div class="btn-group">
            <a href="<?= site_url('configuracio') ?>" class="btn btn-primary"><i class="fa fa-cogs"></i> Configuració </a>
			<a href="<?= site_url('alineacions/territorial') ?>" class="btn btn-outline-primary"><i class="fa fa-th"></i> Alineacions </a>
			<a href="<?= site_url('consultes') ?>" class="btn btn-outline-primary"><i class="fa fa-search"></i> Consultes </a>
        </div>
        <br><br>
        <div class="btn-group">
            <a href="<?= site_url('configuracio') ?>" class="btn btn-outline-primary"><i class="fa fa-hourglass"></i> Temporades </a>
            <a href="<?= site_url('configuracio/locals') ?>" class="btn btn-primary"><i class="fa fa-building"></i> Locals </a>
            <a href="<?= site_url('configuracio/equips') ?>" class="btn btn-outline-primary"><i class="fa fa-users"></i> Equips </a>
			<a href="<?= site_url('configuracio/contrincants') ?>" class="btn btn-outline-primary"><i class='ra ra-crossed-swords'></i>Contrincants </a>
			<a href="<?= site_url('configuracio/jornades') ?>"class="btn btn-outline-primary"><i class="fa fa-calendar"></i> Jornades </a>
            <a href="<?= site_url('configuracio/jugadors') ?>" class="btn btn-outline-primary"><i class="fa fa-address-card"></i> Jugadors </a>
        </div>
		<br><br>
        <div class="btn-group">
            <button class="btn btn-outline-primary" data-toggle="modal" data-target="#addLocal"><i class="fa fa-plus-circle"></i> Afegir Local </button>
        </div>
        <br><br>
		<?php if(isset($errorDelete)):?>
			<?php foreach($errorDelete as $equip):?>
				<?php  error("Error al esborrar: El local està associat al equip de ".$equip['equip']." - ".$equip['categoria']." (".$equip['temporada'].")"); ?>
			<?php endforeach; ?>
		<?php endif; ?>
        <div class="table-responsive">
            <table class="table table-striped table-bordered datatable">
                <thead>
                    <tr>
                        <th>Ciutat </th>
                        <th>Local </th>
                        <th>Carrer</th>
                        <th>Pilota</th>
                        <th>Taula</th>
                        <th>Equipació</th>
                        <th>Accions</th>
                    </tr>
                </thead>
                <tbody>
                 <?php foreach ($locals as $local): ?>
                    <tr>
                    	<td> <?= $local['ciutat']?> (<?=$local['codi_postal']?>) </td>
                    	<td> <?= $local['local']?></td>
                    	<td> <?= $local['carrer']?> </td>
                    	<td> <?= $local['pilota']?> </td>
                    	<td> <?= $local['taula']?> </td>
                    	<td> <?= $local['equipacio']?> </td>
                    	<td>
							<i onclick="genEditModal('<?=$local['id']?>')" class='action-icon fa fa-pencil' data-toggle="modal" data-target="#editLocal" ></i>
							<i onclick="genDeleteModal('<?=$local['id']?>')" class='action-icon fa fa-trash' data-toggle="modal" data-target="#deleteLocal" ></i>
						</td>
                    </tr>
                 <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <br>
    </div>
	<br>
</main>
    </div>
	<br>
</main>

<form action="<?= site_url("configuracio/addLocal") ?>" method="post">
    <div class="modal fade" id="addLocal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Afegir Local</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                	<div class="hidden-values"></div>
                	<div class="row">
                		<div class="form-group col-sm-12 col-md-8">
							<label for='ciutat'>Ciutat</label>
							<input type='text' class='form-control' id='ciutat' name='ciutat' maxlength='35' required>
						</div>
                		<div class="form-group col-sm-12 col-md-4">
                			<label for='codiPostal'>Codi Postal</label>
                        	<input type='text' class='form-control' id='codiPostal' name='codiPostal' maxlength='5' required>
                		</div>
                	</div>
						<div class="row">
							<div class="form-group col-sm-12 col-md-6">
								<label for='local'>Local</label>
                            	<input type='text' class='form-control' id='local' name='local' maxlength='40' required>
							</div>
							<div class="form-group col-sm-12 col-md-6">
								<label for='local'>Carrer</label>
                            	<input type='text' class='form-control' id='carrer' name='carrer' maxlength='30' required>
							</div>
						</div>
                	<div class="row">
						<div class="form-group col-sm-12 col-md-4">
							<label for='local'>Pilota</label>
						    <input type='text' class='form-control' id='pilota' name='pilota' maxlength='15' required>
						</div>
						<div class="form-group col-sm-12 col-md-4">
							<label for='local'>Taula</label>
                      		<input type='text' class='form-control' id='taula' name='taula' maxlength='15' >
						</div>
						<div class="form-group col-sm-12 col-md-4">
							<label for='eequipacio'>Equipació</label>
							<input type='text' class='form-control' id='equipacio' name='equipacio' maxlength='10' >
						</div>
					</div>
                	<div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Afegir</button>
                	</div>
           		 </div>
        	</div>
    	</div>
    </div>
</form>

<form action="<?= site_url("configuracio/editLocal") ?>" method="post">
    <div class="modal fade" id="editLocal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Editar Dades del Local</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                	<div class="hidden-values"></div>
                	<div class="row">
                		<div class="form-group col-sm-12 col-md-8 edit-ciutat"></div>
                		<div class="form-group col-sm-12 col-md-4 edit-cp"></div>
                	</div>
						<div class="row">
							<div class="form-group col-sm-12 col-md-6 edit-local"></div>
							<div class="form-group col-sm-12 col-md-6 edit-carrer"></div>
						</div>
                	<div class="row">
						<div class="form-group col-sm-12 col-md-4 edit-pilota"></div>
						<div class="form-group col-sm-12 col-md-4 edit-taula"></div>
						<div class="form-group col-sm-12 col-md-4 edit-equipacio"></div>
					</div>
                	<div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Editar</button>
                	</div>
           		 </div>
        	</div>
    	</div>
    </div>
</form>

<form action="<?= site_url("configuracio/deleteLocal") ?>" method="post">
    <div class="modal fade" id="deleteLocal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar Local</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                	<div class="hidden-values"></div>
                    <div class="alert alert-danger">
                        <p>Estàs segur que desitges esborrar el local? </p> <p> Per a asegurar el comportament de l'aplicació, no es permetra esborrar locals asignats a algún equip</p>
                    </div>
                	<div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Eliminar</button>
                	</div>
           		 </div>
        	</div>
    	</div>
    </div>
</form>

<script src="<?php echo base_url("assets/js/datatables.min.js"); ?>"></script>

<script>

function genEditModal(idLocal) {
	$.ajax({url: "<?= site_url("configuracio/getLocalToEdit/") ?>"+idLocal, success: function(result){

		var data = JSON.parse(result);

		var taula = "";
		var equipacio = "";

		if(data.taula !== null) taula = data.taula;
		if(data.equipacio !== null) equipacio = data.equipacio;

		$(".hidden-values").empty();
		$(".hidden-values").append("<input id='idLocal' name='idLocal' value='"+idLocal+"' hidden />");

		$(".edit-ciutat").empty();
		$(".edit-ciutat").append("<label for='eciutat'>Ciutat</label>");
		$(".edit-ciutat").append("<input type='text' class='form-control' id='eciutat' name='eciutat' value='"+data.ciutat+"' maxlength='35' required>");

		$(".edit-cp").empty();
		$(".edit-cp").append("<label for='ecp'>Codi Postal</label>");
		$(".edit-cp").append("<input type='text' class='form-control' id='ecp' name='ecp' value='"+data.codi_postal+"' maxlength='5' required>");

		$(".edit-local").empty();
		$(".edit-local").append("<label for='elocal'>Local</label>");
		$(".edit-local").append("<input type='text' class='form-control' id='elocal' name='elocal' value='"+data.local+"' maxlength='40' required>");

		$(".edit-carrer").empty();
		$(".edit-carrer").append("<label for='ecarrer'>Carrer</label>");
		$(".edit-carrer").append("<input type='text' class='form-control' id='ecarrer' name='ecarrer' value='"+data.carrer+"' maxlength='30' required>");

		$(".edit-pilota").empty();
		$(".edit-pilota").append("<label for='epilota'>Pilota</label>");
		$(".edit-pilota").append("<input type='text' class='form-control' id='epilota' name='epilota' value='"+data.pilota+"' maxlength='15' required>");

		$(".edit-taula").empty();
		$(".edit-taula").append("<label for='etaula'>Taula</label>");
		$(".edit-taula").append("<input type='text' class='form-control' id='etaula' name='etaula' value='"+taula+"' maxlength='15' >");

		$(".edit-equipacio").empty();
		$(".edit-equipacio").append("<label for='eequipacio'>Equipació</label>");
		$(".edit-equipacio").append("<input type='text' class='form-control' id='eequipacio' name='eequipacio' value='"+equipacio+"' maxlength='10' >");

	}})
}

function genDeleteModal(idLocal) {
	$(".hidden-values").empty();
	$(".hidden-values").append("<input id='idLocal' name='idLocal' value='"+idLocal+"' hidden />");
}


$(document).ready(function () {
    table = $('.datatable').DataTable({
        order: [[ 0, "desc" ]],
        language: {
            "sProcessing":     "Processant...",
            "sLengthMenu":     "Mostrar _MENU_ contrincants",
            "sZeroRecords":    "No s'han trobat contrincants",
            "sEmptyTable":     "Cap contrincant disponible",
            "sInfo":           "Mostrant de _START_ a _END_ de _TOTAL_ contrincants",
            "sInfoEmpty":      "Mostrant del 0 a 0 de 0 contrincants",
            "sInfoFiltered":   "(filtrades d'un total de _MAX_ contrincants)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Carregant...",
            "oPaginate": {
                "sFirst":    "«",
                "sLast":     "»",
                "sNext":     ">",
                "sPrevious": "<"
            },
            "oAria": {
                "sSortAscending":  ": Activar per a ordenar la columna de manera ascendent",
                "sSortDescending": ": Activar per a ordenar la columna de manera descendent"
            },
		}
    })
 });

</script>
</body>


</html>
