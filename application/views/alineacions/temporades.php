<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="es">
<head>

    <link href="<?= base_url("assets/css/datatables.min.css"); ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/font-awesome.min.css') ?>" rel="stylesheet" />
    <link href="<?= base_url('assets/css/rpg-awesome.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" />
	<link href="<?= base_url('assets/css/style.css') ?>" rel="stylesheet" />
	<script src="<?= base_url('assets/js/jquery-3.4.1.min.js'); ?>"></script>
	<script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
	
</head>
<body>


<main>

    <div class="container">
		<div class="btn-group">
            <a href="<?= site_url('configuracio') ?>" class="btn btn-primary"><i class="fa fa-cogs"></i> Configuració </a>
			<a href="<?= site_url('alineacions/territorial') ?>" class="btn btn-outline-primary"><i class="fa fa-th"></i> Alineacions </a>
			<a href="<?= site_url('consultes') ?>" class="btn btn-outline-primary"><i class="fa fa-search"></i> Consultes </a>
        </div>
        <br><br>
        <div class="btn-group">
            <a href="<?= site_url('configuracio') ?>" class="btn btn-primary"><i class="fa fa-hourglass"></i> Temporades </a>
            <a href="<?= site_url('configuracio/locals') ?>" class="btn btn-outline-primary"><i class="fa fa-building"></i> Locals </a>
            <a href="<?= site_url('configuracio/equips') ?>" class="btn btn-outline-primary"><i class="fa fa-users"></i> Equips </a>
            <a href="<?= site_url('configuracio/contrincants') ?>" class="btn btn-outline-primary"><i class='ra ra-crossed-swords'></i>Contrincants </a>
            <a href="<?= site_url('configuracio/jornades') ?>"class="btn btn-outline-primary"><i class="fa fa-calendar"></i> Jornades </a>
            <a href="<?= site_url('configuracio/jugadors') ?>" class="btn btn-outline-primary"><i class="fa fa-address-card"></i> Jugadors </a>
        </div>
        <br><br>
        <div class="btn-group">
            <button class="btn btn-outline-primary" data-toggle="modal" data-target="#addSeason"><i class="fa fa-plus-circle"></i> Afegir Temporada </button>
        </div>
        <br><br>
        <?php if(isset($errorDelete)): error("Error al esborrar: No es pot esborrar la temporada activa");?>
        <?php endif;?>
        <div class="table-responsive">
            <table class="table table-striped table-bordered datatable">
                <thead>
                    <tr>
                        <th>Temporada</th>
                        <th>Nombre Equips</th>
                        <th>Activa</th>
                        <th>Accions</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($temporades as $temporada): ?>
                    <tr>
                    	<td> <?php echo $temporada['temporada']?> </td>
                    	<td> <?php echo $temporada['numEquips']?> </td>
                    	<td> <input type="checkbox" <?php if($temporada['activa']): echo "checked"; else: echo "unchecked"; endif;?> disabled> </td>
                    	<td> 
                    		<i onclick='genEditModal(<?=$temporada['id']?>)' class='action-icon fa fa-pencil' data-toggle="modal" data-target="#editSeason" ></i>
                    		<i onclick='genDeleteModal(<?=$temporada['id']?>)'class='action-icon fa fa-trash' data-toggle="modal" data-target="#deleteSeason" ></i>
                    	</td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <br>
    </div>
	<br>
</main>

<form action="<?= site_url("configuracio/addSeason") ?>" method="post">
    <div class="modal fade" id="addSeason" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Afegir Temporada</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-primary">
                        <p>Escriu el nom de de la temporada i marca <i>Activa</i> si la temporada es l'actual</p>
                    </div>
					<div class="row">
						<div class="form-group col-sm-12 col-md-8">
							<label for="nomTemporada">Nom Temporada</label>
							<input type="text" class="form-control " id="nomTemporada" name="nomTemporada" maxlength="9" required>
						</div>
						<div class="form-group col-sm-12 col-md-4" style="text-align: center;">
							<label for="activa">Activa</label>
							<input type="checkbox" class="form-control" id="activa" name="activa" />
						</div>
					</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success">Afegir</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form action="<?= site_url("configuracio/editSeason") ?>" method="post">
    <div class="modal fade" id="editSeason" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Editar Temporada</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                	<div class='hidden-values'> </div>
					<div class="row">
						<div id="edit-nom" class="form-group col-sm-12 col-md-8"></div>
						<div id="edit-activa" class="form-group col-sm-12 col-md-4" style="text-align: center;"></div>
					</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-success">Editar</button>
                </div>
            </div>
        </div>
    </div>
</form>


<form action="<?= site_url("configuracio/deleteSeason") ?>" method="post">
    <div class="modal fade" id="deleteSeason" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar Temporada</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                	<div class='hidden-values'> </div>
                    <div class="alert alert-danger">
                        <p>Estàs segur que desitges esborrar la temporada? S'eliminaràn els equips i jornades associats a aquesta temporada. </p> <p> Per a asegurar el comportament de l'aplicació, no es permetra esborrar dades que formin part de la temporada activa.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        <button type="submit" class="btn btn-success">Eliminar</button>
                    </div>
            	</div>
        	</div>
    	</div>
    </div>
</form>

<script src="<?php echo base_url("assets/js/datatables.min.js"); ?>"></script>

<script>

	function genEditModal(idTemporada) {
		$.ajax({url: "<?= site_url("configuracio/getSeasonToEdit/") ?>"+idTemporada, success: function(result){

			var data = JSON.parse(result);

			var checked = "";
			if(data.activa == 1) checked ="checked";

			$(".hidden-values").empty();
			$(".hidden-values").append("<input id='idTemporada' name='idTemporada' value='"+idTemporada+"' hidden />");
			
			$("#edit-nom").empty();
			$("#edit-nom").append("<label for='nomTemporada'>Nom Temporada</label>");
			$("#edit-nom").append("<input type='text' class='form-control' id='nomTemporada' name='nomTemporada' value='"+data.temporada+"' maxlength='9' required>");

       		$("#edit-activa").empty();
       		$("#edit-activa").append("<label for='activa'>Activa</label>");
			$("#edit-activa").append("<input type='checkbox' class='form-control' id='activa' name='activa'"+checked+" />");
		}})
	}

	function genDeleteModal(idTemporada) {
		$(".hidden-values").empty();
        $(".hidden-values").append("<input id='idTemporada' name='idTemporada' value='"+idTemporada+"' hidden />");
	}

$(document).ready(function () {	
        table = $('.datatable').DataTable({
            order: [[ 0, "desc" ]],
            language: {
                "sProcessing":     "Processant...",
                "sLengthMenu":     "Mostrar _MENU_ temporades",
                "sZeroRecords":    "No s'han trobat temporades",
                "sEmptyTable":     "Cap temporada disponible",
                "sInfo":           "Mostrant de _START_ a _END_ de _TOTAL_ temporades",
                "sInfoEmpty":      "Mostrant del 0 a 0 de 0 temporades",
                "sInfoFiltered":   "(filtrades d'un total de _MAX_ temporades)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Carregant...",
                "oPaginate": {
                    "sFirst":    "«",
                    "sLast":     "»",
                    "sNext":     ">",
                    "sPrevious": "<"
                },
                "oAria": {
                    "sSortAscending":  ": Activar per a ordenar la columna de manera ascendent",
                    "sSortDescending": ": Activar per a ordenar la columna de manera descendent"
                },
			}
        })
 });
</script>
</body>


</html>
