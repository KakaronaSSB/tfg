<p>
	<span style="font-size:14pt" lang="ES">
		Esteu convocats per l’encontre de <?=$equip?> <?=strtoupper($categoria)?> que es disputarà <?=$diaSetmana?> a <font style="background-color:rgb(255,255,0)"><?=$ciutat?> </font>
	</span>
</p>
<table>
	<tbody>
		<tr style="height:15.75pt" height="21">
			<td style="width:66pt;height:15.75pt" width="88" height="21"><?=$data?></td>
			<td style="width:44pt;border-left-color:currentColor;border-left-width:medium;border-left-style:none" width="59"><?=$hora?></td>
			<td style="width:35pt;border-left-color:currentColor;border-left-width:medium;border-left-style:none" width="47"><?=$categoriaEquip?></td>
			<td style="width:119pt;border-left-color:currentColor;border-left-width:medium;border-left-style:none" width="158"><?=$contrincant?></td>
			<td style="width:119pt;border-left-color:currentColor;border-left-width:medium;border-left-style:none" width="158"><?=CLUB?></td>
			<td style="width:106pt;border-left-color:currentColor;border-left-width:medium;border-left-style:none" width="141"></td>
			<td style="width:203pt;border-left-color:currentColor;border-left-width:medium;border-left-style:none" width="271">
				<div><font style="background-color:rgb(0,255,0)"><?=$jugador1?></font></div>
				<div><font style="background-color:rgb(0,255,0)"><?=$jugador2?></font></div>
				<div><font style="background-color:rgb(0,255,0)"><?=$jugador3?></font></div>
			</td>
		</tr>
	</tbody>
</table>
<p>
	<span style="font-size:14pt" lang="ES">
		Confirmar disponibilitat “via mail” el més aviat possible
	</span>
</p>
<p>
	<span style="font-size:14pt" lang="ES">
		<font style="background-color:rgb(255,255,0)">
			<?=$direccio?>
		</font>
	</span>
</p>
<p>
	<span style="font-size:14pt" lang="ES">
		<font style="background-color:rgb(255,255,0)">
			<?=$local?>
		</font>
	</span>
</p>

<p>
	<span style="font-size:14pt" lang="ES">
		<font style="background-color:rgb(255,255,0)">
			<?=$cp." ".$ciutat?>
		</font>
	</span>
</p>

<p>
	<span style="font-size:14pt" lang="ES">
		Salutacions
	</span>
</p>
