-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 30, 2019 at 03:20 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tfg`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `categoria` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `categoria`) VALUES
(1, 'Territorial'),
(2, 'Nacional'),
(3, 'Veterans');

-- --------------------------------------------------------

--
-- Table structure for table `contrincants`
--

CREATE TABLE `contrincants` (
  `id` int(11) NOT NULL,
  `contrincant` varchar(25) NOT NULL,
  `ciutat` varchar(35) NOT NULL,
  `codi_postal` varchar(5) NOT NULL,
  `local` varchar(40) NOT NULL,
  `carrer` varchar(30) NOT NULL,
  `pilota` varchar(15) NOT NULL,
  `taula` varchar(15) DEFAULT NULL,
  `equipacio` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contrincants`
--

INSERT INTO `contrincants` (`id`, `contrincant`, `ciutat`, `codi_postal`, `local`, `carrer`, `pilota`, `taula`, `equipacio`) VALUES
(7, 'CTT MONTBUI', 'STA MARGARIDA DE MONTBUI', '08710', 'Poliesportiu Can Passanals', 'C/ CAN PASSANALS  S/N', 'XUSHAOFA', '', ''),
(8, 'CTT SALLENT', 'SALLENT', '08650', 'Pavellò municipal de Sallent', ' C/ VERGE DEL PILAR S/N', 'BUTTERFLY G40+', '', ''),
(9, 'CTT CALELLA', 'CALELLA', '08370', 'Fabrica Llobet i Guiri', 'C/ SANT JAUME 339 ', 'BUTTERFLY G40+', '', ''),
(10, 'CTT ELS AMICS DE TERRASSA', 'TERRASSA', '08226', 'Institut Blanxart', 'C/ CADIS S/N', 'BUTTERFLY G40+', '', ''),
(11, 'CTT BADALONA', 'BADALONA', '08911', '-', 'C/ SANT ANASTASI, 14', 'BUTTERFLY G40+', '', ''),
(12, 'CTT LA TORRE DE CLARAMUNT', 'LA TORRE DE CLARAMUNT', '08789', 'Gimnàs CEIP La Torre de Claramunt', 'C/ MURANTA S/N', 'BUTTERFLY G40+', '', ''),
(13, 'LLUÏSOS DE GRÀCIA', 'BARCELONA', '08024', '-', 'PLAÇA DEL NORD, 7-10', 'BUTTERFLY G40+', '', ''),
(14, 'CTT ATC MOLINS DE REI', 'MOLINS DE REI', '08750', 'Gimnàs Antic Inst. Lluís Requesens', 'PASSATGE DEL MAS RUBI S/N', 'BUTTERFLY G40+', '', ''),
(15, 'TT PARETS', 'PARETS DEL VALLES', '08150', 'Escola Municipal Pau Vila,', 'C/ PARE J. M. DE IMBERT 39', 'NITTAKU', '', ''),
(16, 'UE SANT CUGAT', 'SANT CUGAT DEL VALLES', '08172', 'A prop de l\'Ajuntament', 'RAMBLA DEL CELLER S/N', 'BUTTERFLY G40+', '', ''),
(17, 'VIC TT', 'VIC', '08500', 'Pavelló municipal d\'esports', 'PLAÇA PARE MILLAN S/N', 'BUTTERFLY G40+', '', ''),
(18, 'CTT CASTELLGALÍ', 'CASTELLGALÍ', '08297', 'Fàbrica Rètro', 'C/ AFORES 11', 'NITTAKU', '', ''),
(19, 'CTT BARCELONA', 'BARCELONA', '08034', '-', 'AVINGUDA JOSEP V. FOIX 85', 'BUTTERFLY G40+', '', ''),
(20, 'CTT SENTMENAT', 'SENTMENAT', '08181', 'Complex esportiu textil besos', 'C/ POCA FARINA S/N', 'XUSHAOFA', '', ''),
(21, 'CTT SANT FELIU DE CODINES', 'SANT FELIU DE CODINES', '08182', 'Centre Civic la Fonteta', 'PLAÇA JOSEP UMBERT ROSAS S/N', 'NITTAKU PREMIUM', '', ''),
(22, 'TT TORELLÓ', 'TORELLO', '08570', 'Antig Col·legi FP', 'C/ ARTESANS 5', 'BUTTERFLY G40+', '', ''),
(23, 'CTT VALLS DE TORROELLA', 'VALLS DE TORROELLA', '08269', '-', 'CARRETERA CARDONA S/N', 'BUTTERFLY G40+', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `edats`
--

CREATE TABLE `edats` (
  `id` int(11) NOT NULL,
  `edat` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `edats`
--

INSERT INTO `edats` (`id`, `edat`) VALUES
(1, 'PREB'),
(2, 'BEN'),
(3, 'ALE'),
(4, 'INF'),
(5, 'JUV'),
(6, 'SUB23'),
(7, 'SEN'),
(8, 'V40'),
(9, 'V50'),
(10, 'V60');

-- --------------------------------------------------------

--
-- Table structure for table `equips`
--

CREATE TABLE `equips` (
  `id` int(11) NOT NULL,
  `equip` varchar(20) NOT NULL,
  `id_local` int(11) NOT NULL,
  `id_temporada` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `jerarquia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `equips`
--

INSERT INTO `equips` (`id`, `equip`, `id_local`, `id_temporada`, `id_categoria`, `jerarquia`) VALUES
(4, '3ªA', 1, 6, 1, 0),
(7, '3ªB \"A\"', 1, 6, 1, -3),
(8, '3ªB \"B\"', 1, 6, 1, -3),
(9, '2ªA', 1, 6, 1, 1),
(10, '1ª \"A\"', 1, 6, 1, 2),
(11, '1ª \"B\"', 1, 6, 1, 2),
(12, 'TDM', 1, 6, 2, 0),
(13, 'SDM', 1, 6, 2, 1),
(23, '3ªA', 1, 6, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fitxes`
--

CREATE TABLE `fitxes` (
  `id` int(11) NOT NULL,
  `fitxa` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fitxes`
--

INSERT INTO `fitxes` (`id`, `fitxa`) VALUES
(1, 'C'),
(2, 'B'),
(3, 'A1'),
(4, 'A2');

-- --------------------------------------------------------

--
-- Table structure for table `jornades`
--

CREATE TABLE `jornades` (
  `id` int(11) NOT NULL,
  `jornada` int(11) NOT NULL,
  `id_equip` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `casa` tinyint(1) NOT NULL,
  `id_contrincant` int(11) NOT NULL,
  `resultat_equip` int(11) NOT NULL,
  `resultat_contrincant` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jornades`
--

INSERT INTO `jornades` (`id`, `jornada`, `id_equip`, `data`, `casa`, `id_contrincant`, `resultat_equip`, `resultat_contrincant`) VALUES
(20, 1, 9, '2019-09-29 11:00:00', 1, 8, 5, 1),
(21, 2, 9, '2019-10-06 11:00:00', 0, 7, 5, 1),
(22, 3, 9, '2019-10-13 11:00:00', 1, 9, 1, 5),
(23, 4, 9, '2019-10-20 11:00:00', 0, 15, 0, 0),
(24, 5, 9, '2019-10-27 11:00:00', 1, 16, 4, 2),
(25, 6, 9, '2019-11-10 11:00:00', 0, 11, 0, 0),
(26, 7, 9, '2019-11-17 11:00:00', 1, 14, 0, 0),
(27, 8, 9, '2019-11-24 10:00:00', 0, 10, 0, 0),
(28, 9, 9, '2019-12-01 11:00:00', 1, 17, 4, 2),
(29, 10, 9, '2019-12-15 11:00:00', 0, 12, 0, 0),
(30, 11, 9, '2019-12-22 11:00:00', 1, 13, 0, 0),
(31, 12, 9, '2020-01-12 11:00:00', 0, 8, 0, 0),
(32, 13, 9, '2020-01-19 11:00:00', 1, 7, 0, 0),
(33, 14, 9, '2020-01-26 11:00:00', 0, 9, 0, 0),
(34, 15, 9, '2020-02-02 11:00:00', 1, 15, 0, 0),
(35, 16, 9, '2020-02-09 11:00:00', 0, 16, 0, 0),
(36, 17, 9, '2020-02-16 11:00:00', 1, 11, 0, 0),
(37, 18, 9, '2020-03-01 11:00:00', 0, 14, 0, 0),
(38, 19, 9, '2020-03-08 11:00:00', 1, 10, 0, 0),
(39, 20, 9, '2020-03-15 11:00:00', 0, 17, 0, 0),
(40, 21, 9, '2020-03-22 11:00:00', 1, 12, 0, 0),
(41, 22, 9, '2020-03-29 11:00:00', 0, 13, 0, 0),
(42, 1, 8, '2019-10-05 17:00:00', 1, 22, 0, 0),
(43, 2, 8, '2019-10-13 11:00:00', 0, 21, 0, 0),
(44, 3, 8, '2019-10-19 17:00:00', 1, 8, 0, 0),
(45, 4, 8, '2019-11-16 17:00:00', 1, 18, 0, 0),
(46, 5, 8, '2019-11-24 11:00:00', 0, 23, 0, 0),
(47, 6, 8, '2019-11-30 17:00:00', 1, 19, 0, 0),
(48, 7, 8, '2019-12-15 11:00:00', 0, 20, 0, 0),
(49, 8, 8, '2019-12-22 17:00:00', 1, 10, 0, 0),
(50, 9, 8, '2020-01-19 11:00:00', 0, 22, 0, 0),
(51, 10, 8, '2020-01-25 17:00:00', 1, 21, 0, 0),
(52, 11, 8, '2020-02-02 11:00:00', 0, 8, 0, 0),
(53, 12, 8, '2020-02-29 18:00:00', 0, 18, 0, 0),
(54, 13, 8, '2020-03-07 17:00:00', 1, 23, 0, 0),
(55, 14, 8, '2020-03-14 11:00:00', 0, 19, 0, 0),
(56, 15, 8, '2020-03-21 17:00:00', 1, 20, 0, 0),
(57, 16, 8, '2020-03-29 10:00:00', 0, 10, 0, 0),
(58, 1, 10, '2019-11-03 17:09:00', 1, 7, 0, 0),
(59, 2, 10, '2019-11-10 17:09:00', 0, 15, 0, 0),
(60, 4, 10, '2019-11-17 17:09:00', 1, 13, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jugadors`
--

CREATE TABLE `jugadors` (
  `llicencia` varchar(6) NOT NULL,
  `nom` varchar(15) NOT NULL,
  `cognom` varchar(15) NOT NULL,
  `id_fitxa` int(11) NOT NULL,
  `id_edat` int(11) NOT NULL,
  `id_equip` int(11) NOT NULL,
  `transport` tinyint(1) NOT NULL,
  `responsable` tinyint(1) NOT NULL,
  `promocionable` tinyint(1) NOT NULL,
  `min_partits` int(11) NOT NULL,
  `max_partits` int(11) NOT NULL,
  `llic_encadenat` varchar(6) DEFAULT NULL,
  `habilitat` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jugadors`
--

INSERT INTO `jugadors` (`llicencia`, `nom`, `cognom`, `id_fitxa`, `id_edat`, `id_equip`, `transport`, `responsable`, `promocionable`, `min_partits`, `max_partits`, `llic_encadenat`, `habilitat`) VALUES
('N07012', 'Marc', 'Lopez', 4, 5, 4, 0, 0, 1, 1, 2, '', 1),
('P11209', 'Alex', 'Vélez', 1, 2, 8, 0, 0, 0, 2, 5, '', 1),
('P11397', 'Matías', 'de Castro', 2, 5, 8, 0, 0, 0, 2, 6, '', 1),
('P11790', 'Èric', 'López', 2, 7, 9, 1, 1, 0, 12, 18, 'P8328', 1),
('P11904', 'Alex', 'Alvarez', 2, 5, 8, 0, 0, 0, 3, 7, '', 1),
('P12121', 'David', 'Pascual', 2, 9, 8, 1, 1, 0, 2, 5, '', 1),
('P1223', 'Jordi', 'Expósito', 2, 3, 8, 0, 0, 0, 3, 8, '', 1),
('P12255', 'Nil', 'Forga', 2, 5, 8, 0, 1, 0, 3, 7, '', 1),
('P12294', 'Christian', 'Arévalo', 2, 5, 8, 0, 1, 0, 3, 8, '', 1),
('p12345', 'Sergi', 'Lopez', 2, 6, 10, 1, 1, 0, 2, 8, '', 1),
('P12741', 'Jan', 'Teixidó', 2, 2, 8, 0, 0, 0, 2, 4, '', 1),
('P12947', 'Marc', 'Aniceto', 2, 4, 8, 0, 0, 0, 3, 5, '', 1),
('P13136', 'Eric', 'Domínguez', 2, 3, 8, 0, 0, 0, 3, 7, '', 1),
('P13140', 'Arnau', 'Marquilles', 2, 6, 8, 0, 1, 0, 2, 6, '', 1),
('P13217', 'Francisco', 'Martínez', 2, 9, 8, 1, 1, 0, 2, 6, '', 1),
('P1724', 'Victor', 'Vinuesa', 2, 9, 9, 1, 1, 0, 2, 4, '', 1),
('P1725', 'Alex', 'Bonavila', 2, 9, 8, 1, 0, 0, 2, 4, '', 1),
('P1985', 'Francisco', 'López', 2, 9, 4, 1, 1, 1, 4, 7, '', 1),
('P3413', 'Alberto', 'Vega', 2, 8, 9, 0, 1, 0, 8, 12, '', 1),
('P431', 'Xavier', 'Giró', 2, 9, 9, 1, 1, 0, 2, 4, '', 1),
('p48571', 'Marc', 'Pascual', 2, 5, 10, 1, 1, 0, 7, 9, '', 1),
('p49573', 'Roger', 'Pons', 2, 6, 10, 1, 1, 0, 8, 10, '', 1),
('P8328', 'Angel', 'Giménez N', 2, 7, 9, 1, 1, 0, 12, 18, 'P11790', 1),
('P8429', 'Eva', 'Martínez', 2, 7, 9, 1, 1, 0, 12, 18, '', 1),
('p87461', 'Ivan', 'Velez', 2, 5, 10, 0, 1, 0, 10, 14, '', 1),
('P8909', 'Joel', 'Domínguez', 2, 5, 8, 0, 1, 0, 4, 7, '', 1),
('P9036', 'Pol', 'Bonavila', 2, 4, 8, 0, 0, 0, 4, 8, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jugadors_alineats`
--

CREATE TABLE `jugadors_alineats` (
  `id` int(11) NOT NULL,
  `llic_jugador` varchar(6) NOT NULL,
  `id_jornada` int(11) NOT NULL,
  `slot` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `promociona` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jugadors_alineats`
--

INSERT INTO `jugadors_alineats` (`id`, `llic_jugador`, `id_jornada`, `slot`, `status`, `promociona`) VALUES
(19, 'P11790', 21, 1, 3, 0),
(20, 'P8328', 21, 2, 3, 0),
(21, 'P8429', 21, 3, 3, 0),
(22, 'P11790', 20, 1, 3, 0),
(23, 'P3413', 20, 2, 3, 0),
(24, 'P8328', 20, 3, 3, 0),
(25, 'P8429', 22, 1, 3, 0),
(26, 'P8328', 22, 2, 3, 0),
(27, 'P3413', 22, 3, 3, 0),
(1283, 'P11790', 23, 1, 2, 0),
(1383, 'P11790', 24, 1, 2, 0),
(1384, 'P3413', 24, 2, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jugadors_alineats_backup`
--

CREATE TABLE `jugadors_alineats_backup` (
  `id` int(11) NOT NULL,
  `llic_jugador` varchar(6) NOT NULL,
  `id_jornada` int(11) NOT NULL,
  `slot` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `promociona` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jugadors_alineats_backup`
--

INSERT INTO `jugadors_alineats_backup` (`id`, `llic_jugador`, `id_jornada`, `slot`, `status`, `promociona`) VALUES
(19, 'P11790', 21, 1, 3, 0),
(20, 'P8328', 21, 2, 3, 0),
(21, 'P8429', 21, 3, 3, 0),
(22, 'P11790', 20, 1, 3, 0),
(23, 'P3413', 20, 2, 3, 0),
(24, 'P8328', 20, 3, 3, 0),
(25, 'P8429', 22, 1, 3, 0),
(26, 'P8328', 22, 2, 3, 0),
(27, 'P3413', 22, 3, 3, 0),
(1283, 'P11790', 23, 1, 2, 0),
(1383, 'P11790', 24, 1, 2, 0),
(1384, 'P3413', 24, 2, 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `locals`
--

CREATE TABLE `locals` (
  `id` int(11) NOT NULL,
  `ciutat` varchar(35) NOT NULL,
  `codi_postal` varchar(5) NOT NULL,
  `local` varchar(40) NOT NULL,
  `carrer` varchar(30) NOT NULL,
  `pilota` varchar(15) NOT NULL,
  `taula` varchar(15) DEFAULT NULL,
  `equipacio` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `locals`
--

INSERT INTO `locals` (`id`, `ciutat`, `codi_postal`, `local`, `carrer`, `pilota`, `taula`, `equipacio`) VALUES
(1, 'Sant Joan Despí', '08970', 'Gimnàs Ateneu Instructiu', 'Passeig del Canal s/n', 'BUTTERFLY S40+', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `temporades`
--

CREATE TABLE `temporades` (
  `id` int(11) NOT NULL,
  `temporada` varchar(10) NOT NULL,
  `activa` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `temporades`
--

INSERT INTO `temporades` (`id`, `temporada`, `activa`) VALUES
(6, '2019-2020', 1);

-- --------------------------------------------------------

--
-- Table structure for table `vacances`
--

CREATE TABLE `vacances` (
  `id` int(11) NOT NULL,
  `llic_jugador` varchar(6) NOT NULL,
  `data` date NOT NULL,
  `motiu` int(11) NOT NULL,
  `id_jornada` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vacances`
--

INSERT INTO `vacances` (`id`, `llic_jugador`, `data`, `motiu`, `id_jornada`) VALUES
(1, 'P11790', '2019-10-16', 0, -1),
(2, 'P11790', '2019-10-17', 0, -1),
(4, 'P11790', '2019-11-17', 1, -1),
(5, 'P11790', '2019-12-22', 1, -1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contrincants`
--
ALTER TABLE `contrincants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `edats`
--
ALTER TABLE `edats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equips`
--
ALTER TABLE `equips`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_categoria` (`id_categoria`),
  ADD KEY `id_local` (`id_local`),
  ADD KEY `id_temporada` (`id_temporada`);

--
-- Indexes for table `fitxes`
--
ALTER TABLE `fitxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jornades`
--
ALTER TABLE `jornades`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uniqueEquipJornada` (`id_equip`,`jornada`),
  ADD KEY `id_contrincant` (`id_contrincant`);

--
-- Indexes for table `jugadors`
--
ALTER TABLE `jugadors`
  ADD PRIMARY KEY (`llicencia`),
  ADD KEY `id_edat` (`id_edat`),
  ADD KEY `id_equip` (`id_equip`),
  ADD KEY `id_fitxa` (`id_fitxa`);

--
-- Indexes for table `jugadors_alineats`
--
ALTER TABLE `jugadors_alineats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_jornada` (`id_jornada`),
  ADD KEY `llic_jugador` (`llic_jugador`);

--
-- Indexes for table `jugadors_alineats_backup`
--
ALTER TABLE `jugadors_alineats_backup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_jornada` (`id_jornada`),
  ADD KEY `llic_jugador` (`llic_jugador`);

--
-- Indexes for table `locals`
--
ALTER TABLE `locals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temporades`
--
ALTER TABLE `temporades`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vacances`
--
ALTER TABLE `vacances`
  ADD PRIMARY KEY (`id`),
  ADD KEY `llic_jugador` (`llic_jugador`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `contrincants`
--
ALTER TABLE `contrincants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `edats`
--
ALTER TABLE `edats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `equips`
--
ALTER TABLE `equips`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `fitxes`
--
ALTER TABLE `fitxes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jornades`
--
ALTER TABLE `jornades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT for table `jugadors_alineats`
--
ALTER TABLE `jugadors_alineats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9131;

--
-- AUTO_INCREMENT for table `jugadors_alineats_backup`
--
ALTER TABLE `jugadors_alineats_backup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9131;

--
-- AUTO_INCREMENT for table `locals`
--
ALTER TABLE `locals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `temporades`
--
ALTER TABLE `temporades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `vacances`
--
ALTER TABLE `vacances`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `equips`
--
ALTER TABLE `equips`
  ADD CONSTRAINT `equips_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `equips_ibfk_2` FOREIGN KEY (`id_local`) REFERENCES `locals` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `equips_ibfk_3` FOREIGN KEY (`id_temporada`) REFERENCES `temporades` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `jornades`
--
ALTER TABLE `jornades`
  ADD CONSTRAINT `jornades_ibfk_1` FOREIGN KEY (`id_contrincant`) REFERENCES `contrincants` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `jornades_ibfk_2` FOREIGN KEY (`id_equip`) REFERENCES `equips` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `jugadors`
--
ALTER TABLE `jugadors`
  ADD CONSTRAINT `jugadors_ibfk_1` FOREIGN KEY (`id_edat`) REFERENCES `edats` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `jugadors_ibfk_2` FOREIGN KEY (`id_equip`) REFERENCES `equips` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `jugadors_ibfk_3` FOREIGN KEY (`id_fitxa`) REFERENCES `fitxes` (`id`) ON UPDATE NO ACTION;

--
-- Constraints for table `jugadors_alineats`
--
ALTER TABLE `jugadors_alineats`
  ADD CONSTRAINT `jugadors_alineats_ibfk_1` FOREIGN KEY (`id_jornada`) REFERENCES `jornades` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jugadors_alineats_ibfk_2` FOREIGN KEY (`llic_jugador`) REFERENCES `jugadors` (`llicencia`);

--
-- Constraints for table `jugadors_alineats_backup`
--
ALTER TABLE `jugadors_alineats_backup`
  ADD CONSTRAINT `jugadors_alineats_backup_ibfk_1` FOREIGN KEY (`id_jornada`) REFERENCES `jornades` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `jugadors_alineats_backup_ibfk_2` FOREIGN KEY (`llic_jugador`) REFERENCES `jugadors` (`llicencia`);

--
-- Constraints for table `vacances`
--
ALTER TABLE `vacances`
  ADD CONSTRAINT `vacances_ibfk_1` FOREIGN KEY (`llic_jugador`) REFERENCES `jugadors` (`llicencia`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
