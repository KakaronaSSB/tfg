# Procediment per a desplegar el sistema

#### Descarregar i instalar XAMPP 

[Enllaç de descarrega](www.apachefriends.org)

#### Desplegar el codi al servidor mitjançant git
```
git clone https://gitlab.com/KakaronaSSB/tfg.git
```
#### Renombrar la carpeta del projecte amb el nom del client (tfg per defecte)
Si el client fos el club CTT Universitat la carpeta es podria dir universitat. Això afectara a la URL ja que serà domini/nom
#### Modificar la constant *TENANT*
Aquesta constant es troba definida al fitxer application/config/constants.php, per defecte el seu valor es:
```
define('TENANT', 'tfg');
```
S'ha de canviar per el nom de la carpeta, en el cas de l'exemple tindríem:
```
define('TENANT', 'universitat');
```
#### Configurar la ruta del projecte al apache
#### Exportar la base de dades
#### Instalar idioma per a poder traduir dates correctament

    Check which locales are installed: locale -a
    if your locale is not there, install it: sudo locale-gen ca_ES
    Update the locales in your server: sudo update-locale
    Restart apache for the changes to take effect

### GLPK

Compilarlo estaticament si falla
