\contentsline {section}{\numberline {1}Introducci\IeC {\'o} als problemes d'optimitzaci\IeC {\'o}}{3}
\contentsline {subsection}{\numberline {1.1}Problema del viatjant de comer\IeC {\c c}}{3}
\contentsline {section}{\numberline {2}Context}{3}
\contentsline {subsection}{\numberline {2.1}Centralitzaci\IeC {\'o} del projecte}{4}
\contentsline {subsection}{\numberline {2.2}Tennis Taula en nombres}{4}
\contentsline {subsection}{\numberline {2.3}Termes i conceptes relacionats amb l\IeC {\textquoteright }esport}{4}
\contentsline {paragraph}{Temporada}{4}
\contentsline {paragraph}{Equip}{5}
\contentsline {paragraph}{Jerarquia d\IeC {\textquoteright }equips}{5}
\contentsline {paragraph}{Categories}{5}
\contentsline {paragraph}{Contrincants}{5}
\contentsline {paragraph}{Jugadors}{5}
\contentsline {paragraph}{Jugadors Encadenats}{5}
\contentsline {paragraph}{Estat d\IeC {\textquoteright }un jugador}{6}
\contentsline {paragraph}{Tipus de Fitxa}{6}
\contentsline {paragraph}{Jornada}{6}
\contentsline {subsection}{\numberline {2.4}Altres termes i conceptes}{6}
\contentsline {paragraph}{Constraint Programming}{6}
\contentsline {paragraph}{Model}{6}
\contentsline {paragraph}{Interpretaci\IeC {\'o}}{7}
\contentsline {paragraph}{Solver}{7}
\contentsline {subsection}{\numberline {2.5}Problema a resoldre}{7}
\contentsline {paragraph}{Restriccions imposades per la federaci\IeC {\'o} i l\IeC {\textquoteright }esport}{7}
\contentsline {paragraph}{Possibles restriccions imposades pels clubs}{7}
\contentsline {subsection}{\numberline {2.6}Actors Implicats}{8}
\contentsline {paragraph}{Desenvolupador i dissenyador}{8}
\contentsline {paragraph}{Test de l\IeC {\textquoteright }aplicaci\IeC {\'o}}{8}
\contentsline {paragraph}{Director del projecte}{8}
\contentsline {paragraph}{Club de Tennis Taula}{8}
\contentsline {subsection}{\numberline {2.7}Justificaci\IeC {\'o}}{8}
\contentsline {section}{\numberline {3}Abast}{9}
\contentsline {subsection}{\numberline {3.1}Objectius del projecte}{9}
\contentsline {subsection}{\numberline {3.2}Sub-objectius del projecte}{9}
\contentsline {paragraph}{Configuraci\IeC {\'o} d\IeC {\textquoteright }elements que formen part d\IeC {\textquoteright }una alineaci\IeC {\'o}}{9}
\contentsline {paragraph}{Gesti\IeC {\'o} d\IeC {\textquoteright }alineacions}{10}
\contentsline {paragraph}{Generaci\IeC {\'o} autom\IeC {\`a}tica d\IeC {\textquoteright }alineacions}{10}
\contentsline {paragraph}{Consultes}{10}
\contentsline {paragraph}{Altres requeriments}{10}
\contentsline {subsection}{\numberline {3.3}Problemes i riscos}{11}
\contentsline {paragraph}{Calendari marcat per la facultat}{11}
\contentsline {paragraph}{Problemes relacionats amb el codi}{11}
\contentsline {paragraph}{Integraci\IeC {\'o} del projecte en l'entorn real}{11}
\contentsline {paragraph}{Desconfian\IeC {\c c}a del client}{11}
\contentsline {subsection}{\numberline {3.4}Metodologia i rigor}{11}
\contentsline {paragraph}{Trello}{12}
\contentsline {paragraph}{Gitlab}{12}
\contentsline {section}{\numberline {4}Programa}{12}
\contentsline {subsection}{\numberline {4.1}Tasques del projecte}{12}
\contentsline {paragraph}{Planificaci\IeC {\'o} i gesti\IeC {\'o} del projecte}{12}
\contentsline {paragraph}{An\IeC {\`a}lisis, disseny i desenvolupament i proves de la interf\IeC {\'\i }cie web}{13}
\contentsline {paragraph}{Disseny, desenvolupament i proves del algorisme que genera alineacions}{13}
\contentsline {subsection}{\numberline {4.2}Taula Resum}{14}
\contentsline {section}{\numberline {5}Recursos humans i materials}{14}
\contentsline {paragraph}{Recursos humans}{14}
\contentsline {paragraph}{Hardware}{15}
\contentsline {paragraph}{Software}{15}
\contentsline {section}{\numberline {6}Gesti\IeC {\'o} del risc. Plans alternatius i obstacles}{15}
\contentsline {paragraph}{Mancan\IeC {\c c}a de temps degudes les dates imposades per la facultat}{15}
\contentsline {paragraph}{Problemes relacionats amb el codi}{16}
\contentsline {paragraph}{Implementaci\IeC {\'o} del projecte en l'entorn real}{16}
\contentsline {section}{\numberline {7}Pressupost}{16}
\contentsline {subsection}{\numberline {7.1}C\IeC {\`a}lcul de costos}{16}
\contentsline {paragraph}{Cost de recursos humans}{16}
\contentsline {paragraph}{Costos del hardware}{16}
\contentsline {paragraph}{Costos del software}{17}
\contentsline {paragraph}{Costos indirectes}{17}
\contentsline {subsection}{\numberline {7.2}Pressupost Total}{18}
\contentsline {subsection}{\numberline {7.3}Conting\IeC {\`e}ncies}{18}
\contentsline {subsection}{\numberline {7.4}Imprevistos}{18}
\contentsline {section}{\numberline {8}Informe de Sostenibilitat}{18}
\contentsline {subsection}{\numberline {8.1}Autoavaluaci\IeC {\'o}}{19}
\contentsline {subsection}{\numberline {8.2}\IeC {\`A}mbit Economic}{19}
\contentsline {subsection}{\numberline {8.3}\IeC {\`A}mbit Ambiental}{19}
\contentsline {subsection}{\numberline {8.4}\IeC {\`A}mbit Social}{19}
