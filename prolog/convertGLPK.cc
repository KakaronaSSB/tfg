#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;

int main() {
  string line;
  stringstream ss;
  bool startReadingValues = false;
  while (getline(cin,line)){
    stringstream ss(line);
    if (startReadingValues and line.length() == 0) break;
    if (startReadingValues) {
      int num, value;
      string varName, ast;
      ss >> num >> varName;
      if (ss >> ast) { // single line
	ss >> value;
      }
      else {
	getline(cin,line);
	stringstream s1(line);
	s1 >> ast >> value;
      }
      cout << varName << " = " << value << "." << endl;
    }
    else {
      string aux1, aux2, aux3;
      if (ss >> aux1 >> aux2 >> aux3 and
	  aux1 == "No." and aux2 == "Column" and aux3 == "name") {
	getline(cin,line);
	startReadingValues = true;
      }
    }
  }
}
