:-include(entrada).
%:-include(entradaTest).
:-dynamic(encadenats/2).
:-dynamic(count/1).
:-dynamic(noDisponibilitat/2).
:-dynamic(jugadorJornada/2).

maxComputationTime(60).



%%%%%%%%%%%%%%%%%%%%% Percentatges importancia constraints softs
perc(minJornades,40).
perc(encadenats,20).
perc(espaiat,10).
perc(localVisitant,10).
perc(responsable,5).
perc(transport,5).
perc(maxJornades,5).
perc(noPromociona,5).


coeff(encadenats,C):- numCadenes(N1), numJornades(N2), N is N1*N2, N > 0, !, perc(encadenats,P), C is P/N.
coeff(encadenats,0).
coeff(transport,C):-numJornadesVisitants(N), N > 0, !, perc(transport,P), C is P/N.
coeff(transport,0).
coeff(noPromociona,C):-numJugadorsPromocionables(N1), numJornades(N2), N is N1*N2, N > 0, !, perc(noPromociona,P), C is P/N.
coeff(noPromociona,0).

coeff(espaiat,C):- numJugadors(N), perc(espaiat,P), C is P/N.
coeff(localVisitant,C):- numJugadors(N), perc(localVisitant,P), C is P/N.
coeff(responsable,C):-numJornades(N), perc(responsable,P), C is P/N.
coeff(minJornades,C):- numJugadors(N), perc(minJornades,P), C is P/N.
coeff(maxJornades,C):- numJugadors(N), perc(maxJornades,P), C is P/N.


numCadenes(N):- findall(P1-P2,encadenats(P1,P2),L), length(L,N).
numJugadors(N):- findall(P,jugador(P),L), length(L,N).
numJugadorsPromocionables(N):- findall(P,jugadorPromocionable(P),L), length(L,N).
numJornades(N):- findall(P,jornada(P),L), length(L,N).
numJornadesVisitants(N):- findall(P,jornadaVisitant(P),L), length(L,N).
checkPercentatges:- findall(P,perc(_,P),L), sum_list(L,100).
%%%%%%%%%%%%%%%%%%%%%%



solver(cplex).
%solver(intsat).
%solver(pbsat).
%solver(glpk).

diffLocalVisitant(2).


%                            jugador(id,MinPartits,MaxPartits,Equip,Transport,Responsable,Promocionable,Edat,Fitxa).
jugador(P):-                 jugador(P ,_         ,_         ,_    ,_        ,_          ,_            ,_   ,_    ).
jugadorEquip(P,E):-          jugador(P ,_         ,_         ,E    ,_        ,_          ,_   		   ,_   ,_    ).
jugadorMinMax(P,Min,Max):-   jugador(P ,Min       ,Max       ,_    ,_        ,_          ,_            ,_   ,_    ).
jugadorResponsable(P):-      jugador(P ,_         ,_         ,_    ,_        ,1          ,_            ,_   ,_    ).
jugadorTransport(P):-        jugador(P ,_         ,_         ,_    ,1        ,_          ,_            ,_   ,_    ).
jugadorPromocionable(P):-    jugador(P ,_         ,_         ,_    ,_        ,_          ,1            ,_   ,_    ).
jugadorEdat(P,A):-			 jugador(P ,_         ,_         ,_    ,_        ,_          ,_            ,A   ,_    ).

%                            jornada(id,idEquip,numJornada,data,local/visitant).
jornada(J):-                 jornada(J ,_      ,_         ,_   ,_             ).
jornadaEquip(J,E):-          jornada(J ,E      ,_         ,_   ,_             ).
jornadaVisitant(J):-         jornada(J ,_      ,_         ,_   ,0             ).
jornadaLocal(J):-            jornada(J ,_      ,_         ,_   ,1             ).
jornadaData(J,D):-			 jornada(J ,_      ,_         ,D   ,_             ).

%							 equip(id,idCategoria,jerarquia).
equip(E):-					 equip(E ,_			 ,_		   ).
equipCategoria(E,C):-		 equip(E ,C			 ,_		   ).
equipJerarquia(E,O):-		 equip(E ,_			 ,O		   ).

%% === useful predicates: =====================================================================
numJornadesEquip(E,N):-
	findall(_,jornadaEquip(_,E),List),
	length(List,N).

maxEspaiat(P,T):-
	jugadorEquip(P,E),
	jugadorMinMax(P,Min,_),
	numJornadesEquip(E,N),
	T is round(N/Min) - 1.

jugadorPotJugarJornada(P,J):-
	jugadorEquip(P,E),
	jornadaEquip(J,E).
jugadorPotJugarJornada(P,J):-
	jugadorPromocionable(P),
	jugadorEquip(P,E1),
	jornadaEquip(J,E2),
	equipCategoria(E1,C),
	equipCategoria(E2,C),
	equipJerarquia(E1,O1),
	equipJerarquia(E2,O2),
	O2 is O1 + 1.

jugadorCategoria(P,C):-
	jugadorEquip(P,E),
	equipCategoria(E,C).

%% === maximesPromocions(edat,idCategoria,M): =================================================
	maximesPromocions(preb,1,5).
	maximesPromocions(ben,1,5).
	maximesPromocions(ale,1,5).
	maximesPromocions(inf,1,5).
	maximesPromocions(juv,1,5).

	maximesPromocions(sub23,1,1).
	maximesPromocions(sen,1,1).
	maximesPromocions(v40,1,1).
	maximesPromocions(v50,1,1).
	maximesPromocions(v60,1,1).

	maximesPromocions(v40,3,2).
	maximesPromocions(v50,3,2).
	maximesPromocions(v60,3,2).
%% === boolean vars: ==========================================================================

makeVars:- jugador(P), jornada(J), 				assertz(boolVar(jugJor(P,J))), 			 fail.  % P juga la jornada Jor
makeVars:- jugador(P), equip(E), 				assertz(boolVar(jugAliEquip(P,E))), 	 fail.  % P ha jugat amb equip E
makeVars:- jugador(P), jornada(J),				assertz(boolVar(jugPromo(P,J))), 		 fail.  % P promociona en jornada J

makeVars:- jugador(P),             				assertz(boolVar(min(P))),   			 fail.  % P compleix el [Min,Max] associat
makeVars:- jugador(P),             				assertz(boolVar(max(P))),   			 fail.  % P compleix el [Min,Max] associat
makeVars:- encadenats(P1,P2), jornada(J),       assertz(boolVar(cadena(P1,P2,J))), 		 fail.  % P1-P2 compleix la seva cadena en la jornada J
makeVars:- jugador(P),							assertz(boolVar(espaiat(P))), 			 fail.  % P compleix el seu espaiat
makeVars:- jornada(J),							assertz(boolVar(jornadaResponsable(J))), fail.  % J te un responsable
makeVars:- jornada(J),							assertz(boolVar(jornadaTransport(J))), 	 fail.    % J te transport
makeVars:- jugador(P),                      	assertz(boolVar(igualLocalVisit(P))), 	 fail. % P jugador aprox igual a casa que a fora
makeVars.

%% === objective function: ====================================================================================

% Minimize objective function
writeObjectiveFunction:-
    jugador(P),
    coeff(maxJornades,C1),    writeObjective( [0,P], C1*max(P) ),
    coeff(minJornades,C2),    writeObjective( [1,P], C2*min(P) ),
    coeff(espaiat,C3),        writeObjective( [6,P], C3*espaiat(P)),
    coeff(localVisitant,C4),  writeObjective( [4,P], C4*igualLocalVisit(P)),
    fail.
writeObjectiveFunction:-
    jornada(J),
    coeff(responsable,C1), writeObjective( [2,J], C1*jornadaResponsable(J)),
    coeff(transport,C2),   writeObjective( [3,J], C2*jornadaTransport(J)),
    fail.
writeObjectiveFunction:-
    jornada(J), jugador(P),
    coeff(noPromociona,C), writeObjective( [7,P,J], C*jugPromo(P,J)),
    fail.
writeObjectiveFunction:-
    encadenats(P1,P2), jornada(J),    
    coeff(encadenats,C), writeObjective( [5,P1,P2,J], C*cadena(P1,P2,J)),
    fail.

writeObjectiveFunction.

%% === constraints: =========================================================================================

symbolicOutput(0).

writeConstraints:-
    checkPercentatges, %% no escriu cap constraint
    cadaJornada3Jugadors, %HARD
    cadaJugadorMinMaxJornades, %SOFT [0,P] [1,P]
    cadaJornadaUnResponsable, %SOFT [2,J]
    cadaJornadaUnTransport, %SOFT [3,J]
    cadaJugadorAproxIgualLocalVisitant, %SOFT [4,P]
    jugadorsEncadenats, %SOFT [5,[P1,P2,J]]
    noJugarEnDatesNoDisponibles, %HARD
    respectarSolucioParcial, %HARD
    noExcedirPromocions, %HARD
    noPromocionsHoritzontals, %HARD
    respectarEspaiat, %SOFT [6,P]
    relateVars,
    true.

cadaJornada3Jugadors:-
    jornada(J), findall(1*jugJor(P,J),jugadorPotJugarJornada(P,J),M),
    wConstraint( M = 3 ),
    fail.
cadaJornada3Jugadors.


%wConstraint( ubForLHSminusK(M), BoolVar --> Sum =< K ):-  % b--> S =< K becomes S =< K+M(1-b)

cadaJugadorMinMaxJornades:-
    jugadorMinMax(P,Min,Max),
    findall(1*jugJor(P,J),jugadorPotJugarJornada(P,J),M),
    wConstraint( \+max(P) --> M =< Max),
    wConstraint( \+min(P) --> M >= Min ),
    fail.
cadaJugadorMinMaxJornades.

cadaJornadaUnResponsable:- % Es podria separar en local/visitant per donar diferents pesos
    jornada(J),
    findall(1*jugJor(P,J),(jugadorPotJugarJornada(P,J),jugadorResponsable(P)),M),
    wConstraint( \+jornadaResponsable(J) --> M >= 1 ),
    fail.
cadaJornadaUnResponsable.

cadaJornadaUnTransport:- %%%%% pendent soft !!!!!
    jornadaVisitant(J),
    findall(1*jugJor(P,J),(jugadorPotJugarJornada(P,J),jugadorTransport(P)),M),
    wConstraint( \+jornadaTransport(J) --> M >= 1 ),
    fail.
cadaJornadaUnTransport.

jugadorsEncadenats:-
    encadenats(P1,P2), % Sabem que seran del mateix equip
    jugadorEquip(P1,E),
    jornadaEquip(J,E),
    wConstraint(\+cadena(P1,P2,J) --> [1*jugJor(P1,J), (-1)*jugJor(P2,J)] = 0),
    fail.
jugadorsEncadenats.

cadaJugadorAproxIgualLocalVisitant:-
    diffLocalVisitant(K),
    jugador(P),
    findall(1*jugJor(P,J),(jugadorPotJugarJornada(P,J),jornadaLocal(J)),SumaLocals),
    findall((-1)*jugJor(P,J),(jugadorPotJugarJornada(P,J),jornadaVisitant(J)),SumaVisitants),
    append(SumaLocals,SumaVisitants,Suma),
    wConstraint(\+igualLocalVisit(P) --> Suma >= -K),
    wConstraint(\+igualLocalVisit(P) --> Suma =< K),
    fail.
cadaJugadorAproxIgualLocalVisitant.

noJugarEnDatesNoDisponibles:-
	noDisponibilitat(P,D),
	findall(1*jugJor(P,J),jornadaData(J,D),List),
	wConstraint(List = 0),
	fail.
noJugarEnDatesNoDisponibles.

respectarSolucioParcial:-
	findall(1*jugJor(P,J),jugadorJornada(P,J),SolParcial),
	length(SolParcial,M),
	wConstraint(SolParcial = M),
	fail.
respectarSolucioParcial.

respectarEspaiat:-
	jugadorEquip(P,E),
	maxEspaiat(P,T),
	K is T + 1,
	jornadaEquip(JIni,E),
	JFin is JIni + K - 1, jornadaEquip(JFin,E),
	findall(1*jugJor(P,J),between(JIni,JFin,J),List),
	wConstraint( \+espaiat(P) --> List >= 1),
	fail.
respectarEspaiat.

noExcedirPromocions:-
	jugadorCategoria(P,C),
	jugadorEdat(P,E),
	maximesPromocions(E,C,K),
	findall(1*jugPromo(P,J),jornada(J),M),
	wConstraint(M =< K),
	fail.
noExcedirPromocions.

noPromocionsHoritzontals:-
	equip(E1,C,O),
	equip(E2,C,O),
	E1 < E2,
	jugador(P),
	wConstraint([1*jugAliEquip(P,E1), 1*jugAliEquip(P,E2)] =< 1),
	fail.
noPromocionsHoritzontals.

relateVars:-
	jugador(P), jornadaEquip(J,E),
	wConstraint([1*jugJor(P,J), (-1)*jugAliEquip(P,E)] =< 0),
	fail.
relateVars:-
	jugadorEquip(P,E1), jornadaEquip(J,E2),
	E1 =\= E2,
	wConstraint([1*jugJor(P,J), (-1)*jugPromo(P,J)] =< 0),
	fail.
relateVars.

% === display sol: =======================================================================================

escriu_jornada(J):-
    jornadaLocal(J),!,
    write("Jornada local "),write(J).
escriu_jornada(J):-
    write("Jornada visitant "),write(J).

escriu_output(J,P):-
	 write(J), write(","), write(P),nl.

%displaySol(M):- write(M), nl, fail.
displaySol(M):- member(X=V,M),  myround(V,V1),  keep(X,V1), storez(sol(X,V1)), fail.

displaySol(_):- jornada(J), escriu_jornada(J), write(": "),
		findall(P,  sol(jugJor(P,J),   _),L), write(L),
		findall(P,  ( sol(jugJor(P,J),_), jugadorResponsable(P)),LR),
		write(" resp: "), write(LR),
		findall(P,  ( sol(jugJor(P,J),_), jugadorTransport(P)),LT),
		write(" transp: "), write(LT),
		nl,fail.
displaySol(_):-nl,nl,fail.
displaySol(_):- jugadorMinMax(P,Min,Max), write("Jugador "), write(P), write(": "),
		findall(J,  sol(jugJor(P,J),   _),L), sort(L,L1), write(L1),
		write(" "),write([Min,Max]),
		write(" "),jugadorEquip(P,E), numJornadesEquip(E,N), write("Num Jornades: "), write(N),
		nl,fail.

displaySol(_):-
   objective(_,C*V), sol(V,1),
   assertz(count(C)), fail.

displaySol(_):-
   objective(Label,C*V), sol(V,1), write([Label,C*V]),nl,
   fail.


displaySol(_):- nl,nl,nl,findall(C,count(C),L), sum_list(L,Sum), write('Total objective: '), write(Sum), nl,nl,fail.

displaySol(_):-tell('solution'),
	sol(jugJor(P,J),_),
	escriu_output(J,P),
	fail.
displaySol(_):-told,tell('validation'),
	objective([C,X],_*V), sol(V,1),
	write(C), write(","), write(X), nl,
	fail.
displaySol(_):-
	objective([C,P,J],_*V), sol(V,1),
	write(C), write(","), write(P), write(","), write(J), nl,
	fail.
displaySol(_):-
	objective([C,P1,P2,J],_*V), sol(V,1),
	write(C), write(","), write(P1), write(","), write(P2), write(","), write(J), nl,
	fail.
displaySol(_):-told.


myround(V,0):- V > -0.00000001, V < 0.00000001, !.
myround(V,1):- V >  0.99999999, V < 1.00000001, !.
myround(V,V):- !.

keep(_,1):-!.
keep(attended(_),_):-!.


writeList(L):- member(X,L), write(X), write(' '), fail.
writeList(_).

%=======================================================================================================

%storez(X):- write(X), nl,assertz(X),!.
storez(X):- assertz(X),!.

% ========== No need to change the following: =====================================

main:-
       retractall( boolVar(_) ),
       makeVars, 
       fail.

main:- current_prolog_flag(argv,[_,InFile|_]), consult(InFile), fail.

main:- symbolicOutput(1),!, writeConstraints, nl, halt.
main:-
    unix('rm -f solCplex.sol intsatSolution.pl intsatSolution.lp fileForCplex salCplex c.lp cplex.log'),
    write('generating constraints...'),nl,
    tell('c.lp'),
    write('Minimize'   ), nl, 	writeObjectiveFunction,
    write('Subject To' ), nl, 	writeConstraints,
    write('Bounds'     ), nl,   writeBounds,
    write('Generals'   ), nl,  	
    write('Binary'     ), nl,   writeBooleanVars,
    write('End'        ), nl,   told,
    write('constraints generated'),nl,nl,nl,nl,

    tell(fileForCplex), maxComputationTime(T),
    write('read c.lp'), nl,
    write('set timelimit '), write(T), write(' s'), nl,
    write('set mip tolerance mipgap 0.0000001.  '), nl,
    write('set mip limits treememory 1000  '), nl,
    write('opt'), nl, write('write solCplex.sol'), nl, write('quit'), nl, told,
    callSolver,
    checkIfSolution, nl,nl,
    halt.
main:- told, write('constraints generation failed'),nl, halt.

callSolver:- solver(cplex),  !, unix("./cplex < fileForCplex "), !.
callSolver:- solver(intsat), !, maxComputationTime(T), atom_number(Tatom,T),
	     atom_concat('intsat -decision polsTmpIntSat.txt -strategy stratIntSat.txt -tlimit ',Tatom,At1), atom_concat(At1,' c.lp',Call), unix(Call),!.
callSolver:- solver(glpk), !, maxComputationTime(T), atom_number(TAtom,T),
	     atom_concat('glpsol --clique --mipgap 0.01 --lp c.lp -o intsatSolution.lp --tmlim ', TAtom,Call), unix(Call),!.
callSolver:- solver(pbsat), !, maxComputationTime(T), atom_number(Tatom,T),
	     atom_concat('pbsat -seed 142313 -conflict 2 -decision polsTmpPBSat.txt -strategy stratPBSat.txt -tlimit ',Tatom,At1), atom_concat(At1,' c.lp',Call), unix(Call),!.




glpkHasSolution:-exists_file('intsatSolution.lp'), shell('grep "INTEGER EMPTY" intsatSolution.lp > salgrep', 1). %grep returns 1

checkIfSolution:- solver(glpk),nl,glpkHasSolution, !, unix("./convertGLPK < intsatSolution.lp > sol.pl"), see('sol.pl'), readModel([], M), seen, nl,nl,write('Solution found.'), nl,nl, displaySol(M),!.


checkIfSolution:- solver(cplex),  exists_file('solCplex.sol'), !, unix("./xml2simple solCplex.sol > sol.pl"),
    see('sol.pl'), readModel([], M), seen, nl,nl,write('Solution found.'), nl,nl, displaySol(M),!.
checkIfSolution:- solver(intsat), exists_file('intsatSolution.lp'), !,  see('intsatSolution.lp'),
		  readModel([], M), seen, nl,nl,write('Solution found.'), nl,nl, displaySol(M),!.
checkIfSolution:- solver(pbsat), exists_file('intsatSolution.lp'), !,  see('intsatSolution.lp'),
    readModel([], M), seen, nl,nl,write('Solution found.'), nl,nl, displaySol(M),!.
checkIfSolution:- solver(cplex), shell('grep "Integer infeasible" cplex.log > salgrep', 0), nl,nl, %grep returns 0
    write('Solver: No solution exists'),!.
checkIfSolution:- maxComputationTime(T), nl,nl,
    write('Solver: No solution found under the given time limit of '), write(T), write(' s.'),!.

checkIfSolution:-write('Hola'),nl.

unix(Command):-shell(Command),!.
unix(_).

writeObjective(E, M):- assertz(objective(E, M)), writeMon( M ), nl, !.

wConstraint(_, _ --> C ):- C =.. [Op,_,_],  \+member(Op,[>=,=<,=]),      write('error1 in constraint'-->C), nl, halt.
wConstraint(_, _ --> C ):- C =.. [_,Sum,_], \+is_list(Sum),              write('error2 in constraint'-->C), nl, halt.

wConstraint(ubsForKminusLHSandLHSminusK(M1,M2), BoolVar1  /\ BoolVar2 --> Sum = K ):-
    wConstraint(ubForKminusLHS(M1), BoolVar1 /\ BoolVar2 --> Sum >= K ),
    wConstraint(ubForLHSminusK(M2), BoolVar1 /\ BoolVar2 --> Sum =< K ),!.

wConstraint( ubForKminusLHS(M), BoolVar1 /\ BoolVar2 --> Sum >= K ):- K1 is K-2*M,
						   wConstraint( [-M*BoolVar1 , -M*BoolVar2 | Sum ] >= K1 ),!.
wConstraint( ubForLHSminusK(M), BoolVar1 /\ BoolVar2 --> Sum =< K ):- K1 is K+2*M,
						   wConstraint( [M*BoolVar1 , M*BoolVar2 | Sum ] =< K1 ),!.

maxSum([],0):-!.
maxSum(L,S):-
    findall(Coeff,(member(C*_,L), Coeff is C, Coeff > 0),List),
    sum_list(List,S).

minSum([],0):-!.
minSum(L,S):-
    findall(Coeff,(member(C*_,L), Coeff is C, Coeff < 0),List),
    sum_list(List,S).

wConstraint( \+BoolVar --> Sum =< K ):-  
    maxSum(Sum,Max), M is Max - K, 
    wConstraint( [-M*BoolVar | Sum ] =< K ),!.            

wConstraint( \+BoolVar --> Sum >= K ):-  
    minSum(Sum,Min), M is K - Min,
    wConstraint( [M*BoolVar | Sum ] >= K ),!.

wConstraint( BoolVar --> Sum =< K ):-  
    maxSum(Sum,Max), M is Max - K, K1 is K+M,                                            
    wConstraint( [M*BoolVar | Sum ] =< K1 ),!.            

wConstraint( BoolVar --> Sum >= K ):-  
    minSum(Sum,Min), M is K - Min,  K1 is K - M,
    wConstraint( [-M*BoolVar | Sum ] >= K1 ),!.		  

wConstraint( Literal --> Sum = K ):-
    wConstraint(Literal --> Sum >= K ),
    wConstraint(Literal --> Sum =< K ),!.

wConstraint( V --> C ):- write('error3 in constraint'-V-->C), nl, halt.

wConstraint(C):- C =.. [=,Sum,K], length(Sum,0), K is 0, !.
wConstraint(C):- C =.. [Op,Sum,K], writeSum(Sum), write('  '), writeOp(Op), write('  '), write(K), nl,!.
wConstraint(C):- write('error4 in constraint'(C)), nl, halt.

writeSum([]):-  !.
writeSum(S+Mon):-  !, writeMon( Mon), nl,  writeSum(S), !.
writeSum(S-Mon):-  !, writeMon(-Mon), nl,  writeSum(S), !.
writeSum([Mon|L]):-   writeMon( Mon), nl,  writeSum(L), !.
writeSum(Mon):-   writeMon( Mon), nl,  !.


writeMon( -(A) ):- number(A), !, AB is -A,     writeMon(AB),!.
writeMon( A ):- number(A), A is 0, !.
writeMon( A ):- number(A), A>=0, !, write(' + '), write(A ), !.
writeMon( A ):- number(A), A<0, !, write(' - '), AB is -A, write(AB), !.
writeMon(-(A)*X):- number(A), !, AB is -A,     writeMon(AB*X),!.
writeMon(A*_):- number(A), A is 0, !.
writeMon(A*X):- number(A), A>0, !,            write(' + '), write(A ), write(' '), write(X), !.
writeMon(A*X):- number(A), A<0, !,  AB is -A,  write(' - '), write(AB), write(' '), write(X), !.
writeMon(-X):- !,                   write(' - '), write(1 ), write(' '), write(X), !.
writeMon( X):- !,                   write(' + '), write(1 ), write(' '), write(X), !.
writeMon(X):- write('error in writeMon'(X)), nl, halt.

writeClause(Lits):- linearize(Lits,Sum,N), K is 1-N, wConstraint( Sum >= K ), !.

linearize( [], [], 0).
linearize( [\+X | A], [(-1) * X | B], N) :- !, linearize(A,B,M), N is M+1.
linearize( [ -X | A], [(-1) * X | B], N) :- !, linearize(A,B,M), N is M+1.
linearize( [  X | A], [       X | B], N) :-    linearize(A,B,N).

readModel(L1,L2):- read(XV), addIfNeeded(XV,L1,L2),!.
addIfNeeded(end_of_file,L,L):-!.
addIfNeeded(XV,L1,L2):- readModel([XV|L1],L2),!.

writeOp(=<):-write('<='),!.
writeOp(Op):-write(Op),!.

writeSlot(S):- slot(S,H1,H2), numPickups(H1-H2,_), write2(H1), write(':00-'), write2(H2), write(':00'),!.
writeDate(X-Y-Z):- write2(X),write('-'),write2(Y),write('-'),write(Z),!.
write2(X):- integer(X), X<10,!, write('0'), write(X),!.
write2(X):- write(X),!.

wl([]).
wl([X|L]):- write(X), write('  '), wl(L),!.


% Express that Var is equivalent to the disjunction of Lits:
expressOr( Var, Lits ):- member(Lit,Lits), negateLit(Lit,NLit), writeClause([ NLit, Var ]), fail.
expressOr( Var, Lits ):- negateLit(Var,NVar), writeClause([ NVar | Lits ]),!.

% Express that Var is equivalent to the conjunction of Lits:
expressAnd( Var, Lits ):- negateLit(Var,NVar), member(Lit,Lits),  writeClause([ NVar, Lit ]), fail.
expressAnd( Var, Lits ):- negateAll(Lits,NLits), writeClause([ Var | NLits ]),!.

negateLit(\+L,   L):-!.
negateLit(  L, \+L):-!.

negateAll( [], [] ).
negateAll( [X|L],[NX|NL] ):- negateLit(X,NX), negateAll(L,NL),!.

writeBooleanVars:- boolVar(V), write(V), nl, fail.
writeBooleanVars:-nl.

writeIntegerVars:- intVar(V,_,_), write(V), nl, fail.
writeIntegerVars:-nl.

writeBounds:- boolVar(V),       write(0),  write(' <= '), write(V), write(' <= '), write(1),  nl, fail.
writeBounds.

%=============================================================================
